#.rst:
#
# autocmake.yml configuration::
#
#   docopt:
#     - "--dec Compile the divide-expand-consolidate module [default: False]."
#   define: "'-DENABLE_DEC={0}'.format(arguments['--dec'])"

option(ENABLE_DEC "Enable Divide-Expand-Consolidate module" OFF)
option(FORCE_DEC "Force Enable Divide-Expand-Consolidate module" OFF)

option(ENABLE_TENSORS "Enable parallel distributed tensors" OFF)
option(TENSORS_ENABLE_REQUEST_MPI3 "Enable request based one-sided MPI3 routines" OFF)
option(TENSORS_ENABLE_BRUTE_FORCE_FLUSH "Brute force flushing of MPI windows" OFF)

if(ENABLE_DEC)
  set(ENABLE_TENSORS ON CACHE BOOL "Enable parallel distributed tensors" FORCE)
  if(ENABLE_MPI)
    if(NOT HAS_MPI3_FEATURES)
      set(ENABLE_DEC OFF CACHE BOOL "Enable Divide-Expand-Consolidate module" FORCE)
      message(STATUS "DEC deactivated due to missing MPI3 features")
    endif()
  endif()
  if(NOT HAS_PTR_RESHAPE)
    set(ENABLE_DEC OFF CACHE BOOL "Enable Divide-Expand-Consolidate module" FORCE)
    message(STATUS "DEC deactivated due to missing POINTER RESHAPE features")
  endif()
endif()

if(FORCE_DEC)
  set(ENABLE_DEC ON CACHE BOOL "Enable Divide-Expand-Consolidate module" FORCE)
  message(STATUS "DEC enforced due to FORCE_DEC")
endif()

if(TENSORS_ENABLE_REQUEST_MPI3 AND TENSORS_ENABLE_BRUTE_FORCE_FLUSH)
  message(FATAL_ERROR "Only one of the options TENSORS_ENABLE_REQUEST_MPI3, TENSORS_ENABLE_BRUTE_FORCE_FLUSH allowed ")
endif()

if(ENABLE_TENSORS)
  message(STATUS "Fetching and building ScaTeLib")
  set(_scatelib_sha256 "bdf4186c31c2f0633d94853a868412197e1fa62c")
  include(FetchContent)
  FetchContent_Declare(scatelib_sources
    QUIET
    URL
      https://gitlab.com/pett/ScaTeLib/-/archive/${_scatelib_sha256}/ScaTeLib-${_scatelib_sha256}.tar.gz
    PATCH_COMMAND
      patch -p1 < ${CMAKE_CURRENT_LIST_DIR}/ScaTeLib.patch
    )

  FetchContent_GetProperties(scatelib_sources)

  set(ENABLE_MPI ${ENABLE_MPI} CACHE BOOL "")
  set(ENABLE_OPENMP ${ENABLE_OMP} CACHE BOOL "")
  set(ENABLE_GPU ${ENABLE_GPU} CACHE BOOL "")
  set(ENABLE_64BIT_INTEGERS ${ENABLE_64BIT_INTEGERS} CACHE BOOL "")
  set(ENABLE_REAL_SP ${ENABLE_REAL_SP} CACHE BOOL "")
  set(ENABLE_CRAY_WRAPPERS ${ENABLE_CRAY_WRAPPERS} CACHE BOOL "")
  set(USE_MPIF_H ${USE_MPIF_H} CACHE BOOL "")
  set(ENABLE_TENSOR_CLASS ON CACHE BOOL "")
  set(ENABLE_REQUEST_BASED_MPI3 ${TENSORS_ENABLE_REQUEST_MPI3} CACHE BOOL "")
  set(ENABLE_BRUTE_FORCE_FLUSH ${TENSORS_ENABLE_BRUTE_FORCE_FLUSH} CACHE BOOL "")
  set(BUILT_AS_SUBMODULE ON CACHE BOOL "")
  set(CMAKE_POSITION_INDEPENDENT_CODE ON CACHE BOOL "")

  if(NOT ScaTeLib_sources_POPULATED)
    FetchContent_Populate(scatelib_sources)

    add_subdirectory(
      ${scatelib_sources_SOURCE_DIR}
      ${scatelib_sources_BINARY_DIR}
      )
  endif()
endif()

