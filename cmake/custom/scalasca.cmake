option(ENABLE_SCALASCA "Enable scalasca profiler mode" OFF)

if(ENABLE_SCALASCA)
  set(SCALASCA_INSTRUMENT ${CMAKE_Fortran_COMPILER})
  configure_script(
    ${PROJECT_SOURCE_DIR}/src/scalasca.in
    ${PROJECT_BINARY_DIR}/scalascaf90.sh
    )
  set(SCALASCA_INSTRUMENT ${CMAKE_C_COMPILER})
  configure_script(
    ${PROJECT_SOURCE_DIR}/src/scalasca.in
    ${PROJECT_BINARY_DIR}/scalascaCC.sh
    )
  set(SCALASCA_INSTRUMENT ${CMAKE_CXX_COMPILER})
  configure_script(
    ${PROJECT_SOURCE_DIR}/src/scalasca.in
    ${PROJECT_BINARY_DIR}/scalascaCXX.sh
    )
  unset(SCALASCA_INSTRUMENT)
  SET(CMAKE_Fortran_COMPILER "${PROJECT_BINARY_DIR}/scalascaf90.sh")
  SET(CMAKE_C_COMPILER "${PROJECT_BINARY_DIR}/scalascaCC.sh")
  SET(CMAKE_CXX_COMPILER "${PROJECT_BINARY_DIR}/scalascaCXX.sh")
endif()
