option(ENABLE_MPI "Enable MPI parallelization" OFF)
option(USE_MPIF_H "Use mpif.h for MPI binding" OFF)
option(ENABLE_CRAY_WRAPPERS "Enable Cray wrappers for BLAS/LAPACK and MPI" OFF)
option(FORCE_32BIT_MPI_INTERFACE "Force use of 32-bit integer MPI interface" OFF)

set(MPI_FOUND FALSE)

if(ENABLE_MPI)
  if(ENABLE_CRAY_WRAPPERS)
    message(STATUS "Use CRAY wrappers; this disables MPI detection")
    set(MPI_FOUND TRUE)
  else()
    find_package(MPI REQUIRED COMPONENTS Fortran)
  endif()
endif()

if(MPI_FOUND)
  if(NOT USE_MPIF_H)
    if(MPI_Fortran_HAVE_F90_MODULE)
      message(STATUS "Using Fortran 90 mpi module")
    else()
      message(WARNING "Fortran 90 mpi module NOT found. Falling back to mpif.h")
    endif()
  endif()

  # test integer size available for MPI module, will fail if using mpif.h
  foreach(_integer_size I4 I8)
    if(NOT DEFINED MPI_F90_${_integer_size})
      set(WORK_DIR "${CMAKE_CURRENT_BINARY_DIR}/feature_tests/MPI_tests")
      set(SRC_DIR  "${CMAKE_CURRENT_LIST_DIR}/feature_tests")
      configure_file("${SRC_DIR}/test-MPI-f90-mod-${_integer_size}.f90" "${WORK_DIR}/test-MPI-f90-mod-${_integer_size}.f90" COPYONLY)
      set(SOURCE_FILE "${WORK_DIR}/test-MPI-f90-mod-${_integer_size}.f90")
      try_compile(MPI_F90_${_integer_size}
        "${CMAKE_CURRENT_BINARY_DIR}" SOURCES "${SOURCE_FILE}"
        CMAKE_FLAGS
          -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
          -DINCLUDE_DIRECTORIES=${MPI_Fortran_INCLUDE_DIRS}
          -DCMAKE_Fortran_FLAGS=${MPI_Fortran_COMPILE_FLAGS}
        LINK_LIBRARIES
          ${MPI_Fortran_LINK_FLAGS}
          ${MPI_Fortran_LIBRARIES}
      )
      unset(WORK_DIR)
      unset(SRC_DIR)
      unset(SOURCE_FILE)
    endif()
  endforeach()

  if(MPI_F90_I8)
    message(STATUS "Found 64-bit integer mpi module")
  elseif(MPI_F90_I4)
    if(ENABLE_64BIT_INTEGERS)
      message(STATUS "Found 32-bit integer mpi module")
      set(USE_32BIT_MPI_INTERFACE TRUE CACHE BOOL "Using 32-bit integer size in MPI interface")
    else()
      message(STATUS "Found 32-bit integer mpi module")
    endif()
  else()
    if(NOT FORCE_32BIT_MPI_INTERFACE)
      if(ENABLE_64BIT_INTEGERS)
        message(WARNING "Integer check not successful, assuming a 64-bit mpif.h instead (if this is not true, please specify -DFORCE_32BIT_MPI_INTERFACE=ON) ")
      else()
        message(WARNING "Integer check not successful, assuming a 32bit mpif.h")
      endif()
    endif()
  endif()

  if(ENABLE_64BIT_INTEGERS AND FORCE_32BIT_MPI_INTERFACE)
    message(STATIC "32-bit integer MPI interface activated by the user")
    set(USE_32BIT_MPI_INTERFACE TRUE CACHE BOOL "Using 32-bit integer size in MPI interface")
  endif()

  # Test MPI-3 features
  set(WORK_DIR "${CMAKE_CURRENT_BINARY_DIR}/feature_tests/MPI_tests")
  set(SRC_DIR  "${CMAKE_CURRENT_LIST_DIR}/feature_tests")
  configure_file("${SRC_DIR}/test-MPI-3-features-simple.F90" "${WORK_DIR}/test-MPI-3-features-simple.F90" COPYONLY)
  set(SOURCE_FILE "${WORK_DIR}/test-MPI-3-features-simple.F90")
  try_compile(MPI3_FEATURES_WORK
    "${CMAKE_CURRENT_BINARY_DIR}" SOURCES "${SOURCE_FILE}"
    CMAKE_FLAGS
      -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
      -DINCLUDE_DIRECTORIES=${MPI_Fortran_INCLUDE_PATH}
      -DCMAKE_Fortran_FLAGS=${MPI_Fortran_COMPILE_FLAGS}
    LINK_LIBRARIES
      ${MPI_Fortran_LINK_FLAGS}
      ${MPI_Fortran_LIBRARIES}
    )
  unset(WORK_DIR)
  unset(SRC_DIR)
  unset(SOURCE_FILE)

  if(MPI3_FEATURES_WORK)
    message(STATUS "Found MPI-3 compatible MPI library")
  endif()
  set(HAS_MPI3_FEATURES ${MPI3_FEATURES_WORK} CACHE BOOL "MPI-3 implementation is available" FORCE)
endif()
