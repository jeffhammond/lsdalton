#.rst:
#
# autocmake.yml configuration::
#
#   docopt:
#     - "--pcm Enable PCMSolver library [default: False]."
#   define: "'-DENABLE_PCMSOLVER={0}'.format(arguments['--pcm'])"

option(ENABLE_PCMSOLVER "Enable PCMSolver library" OFF)

# Check for non-PCMSolver friendly compilers
if(CMAKE_CXX_COMPILER MATCHES Cray OR CMAKE_CXX_COMPILER MATCHES PGI OR CMAKE_CXX_COMPILER MATCHES XL)
  set(ENABLE_PCMSOLVER OFF CACHE BOOL "Enable PCMSolver library" FORCE)
  message(STATUS "PCMSolver DISABLED: not supported by the ${CMAKE_CXX_COMPILER} compiler")
endif()

if(ENABLE_PCMSOLVER)
  find_package(ZLIB QUIET)
  if(NOT TARGET ZLIB::ZLIB)
    set(ENABLE_PCMSOLVER OFF CACHE BOOL "Enable PCMSolver library" FORCE)
    message(STATUS "Polarizable Continuum Model via PCMSolver DISABLED")
    message(STATUS " PCMSolver dependencies NOT satisfied:")
    if(ENABLE_STATIC_LINKING)
      message(STATUS "  - install static Zlib development libraries")
    else()
       message(STATUS "  - install dynamic Zlib development libraries")
    endif()
  else()
    message(STATUS "Polarizable Continuum Model via PCMSolver ENABLED")
  endif()
else()
  message(STATUS "Polarizable Continuum Model via PCMSolver DISABLED")
endif()
