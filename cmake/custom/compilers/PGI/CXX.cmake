list(APPEND LSDALTON_CXX_FLAGS
  "-Mpreprocess"
  "--diag_suppress 236"
  )

list(APPEND LSDALTON_CXX_FLAGS_DEBUG
  "-g"
  "-O0"
  )

list(APPEND LSDALTON_CXX_FLAGS_RELEASE
  "-O3"
  "-fast"
  "-Munroll"
  "-Mvect=idiom"
  "-DRESTRICT=restrict"
  )