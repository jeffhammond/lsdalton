# Check if -mcmodel=medium is available
set_compiler_flag(_mcmodel Fortran "-mcmodel=medium")

list(APPEND LSDALTON_Fortran_FLAGS
  "${_mcmodel}"
  "-pgcpplibs"
  )

if(ENABLE_STATIC_LINKING)
  list(APPEND LSDALTON_Fortran_FLAGS
    "-Bstatic"
    )
endif()

list(APPEND LSDALTON_Fortran_FLAGS_DEBUG
  "-g"
  "-O0"
  "-Mframe"
  )

list(APPEND LSDALTON_Fortran_FLAGS_RELEASE
  "-O3"
  "-Mipa=fast"
  )
