add_library(xcfun_host
  OBJECT
    ${CMAKE_CURRENT_LIST_DIR}/xcfun_host.F90
  )

target_compile_options(xcfun_host
  PRIVATE
    "${LSDALTON_Fortran_FLAGS};${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )

target_link_libraries(xcfun_host
  PUBLIC
    lsutil
  )

target_link_libraries(lsdalton
  PUBLIC
    xcfun_host
  )

if(ENABLE_XCFUN)
  find_package(XCFun CONFIG QUIET)
  if(TARGET XCFun::xcfun)
    get_property(_loc TARGET XCFun::xcfun PROPERTY LOCATION)
    message(STATUS "Found XCFun: ${_loc} (found version ${XCFun_VERSION})")
  else()
    message(STATUS "Suitable XCFun could not be located. Fetching and building!")
    include(FetchContent)
    FetchContent_Declare(xcfun_sources
      QUIET
      URL
        https://github.com/dftlibs/xcfun/archive/v2.0.2.tar.gz
      )

    FetchContent_GetProperties(xcfun_sources)

    set(ENABLE_TESTALL FALSE CACHE BOOL "")
    set(BUILD_SHARED_LIBS FALSE CACHE BOOL "")
    set(XCFUN_MAX_ORDER 6)  # TODO Maybe as a user-facing option?
    set(XCFUN_PYTHON_INTERFACE FALSE CACHE BOOL "")

    if(NOT xcfun_sources_POPULATED)
      FetchContent_Populate(xcfun_sources)

      add_subdirectory(
        ${xcfun_sources_SOURCE_DIR}
        ${xcfun_sources_BINARY_DIR}
        )
    endif()
  endif()

  target_sources(xcfun_host
    PRIVATE
      ${XCFun_Fortran_SOURCES}
    )

  target_link_libraries(xcfun_host
    PUBLIC
      XCFun::xcfun
    )
endif()
