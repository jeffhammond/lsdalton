add_library(rsp_properties
  OBJECT
    ${CMAKE_CURRENT_LIST_DIR}/response_prop_noOpenRSP.F90
    ${CMAKE_CURRENT_LIST_DIR}/molecular_hessian.F90
    ${CMAKE_CURRENT_LIST_DIR}/test_molHessian.F90
    ${CMAKE_CURRENT_LIST_DIR}/nuclei_selected_shielding.F90
  )

target_compile_options(rsp_properties
  PRIVATE
    "${LSDALTON_Fortran_FLAGS};${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )

target_link_libraries(rsp_properties
  PUBLIC
    linears
    responsesolver
  )

target_link_libraries(lsdalton
  PUBLIC
    rsp_properties
  )
