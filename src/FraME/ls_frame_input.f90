MODULE ls_frame_input_module

TYPE ls_frame_input
  !> \param Use density-fitting for the potential: (C|\rho) \approx (C|\tilde\rho)
  LOGICAL :: df_potential
  !> \param Use density-fitting for the KS matrix contribution: (ab|EP) \approx (\tilde{ab}|EP)
  LOGICAL :: df_matrix
END TYPE ls_frame_input

public :: ls_frame_input, ls_frame_configure, ls_frame_print_input, ls_frame_copy_input
public :: ls_frame_read

CONTAINS

!> \brief Initialize the ls_frame_input
!> \param frame_input the ls_frame_input object
subroutine ls_frame_configure(frame_input)
implicit none
TYPE(ls_frame_input),intent(INOUT) :: frame_input
frame_input%df_potential = .FALSE.
frame_input%df_matrix    = .FALSE.
end subroutine ls_frame_configure

!> \brief Reads the **FRAME section of the LSDALTON.INP
!> \param frame_input the ls_frame_input object
!> \param LUCMD the dalton input file (LSDALTON.INP)
!> \param LUPRI the default print unit (LSDALTON.OUT)
!> \param word on exit, the next input line
subroutine ls_frame_read(frame_input,LUCMD,LUPRI,word)
implicit none
TYPE(ls_frame_input),intent(INOUT) :: frame_input
INTEGER,intent(IN)                 :: LUCMD !Logical unit number for the daltoninput
character(len=80),intent(inout)    :: WORD
INTEGER,intent(IN)                 :: LUPRI
!
character(len=1) :: PROMPT

DO
  READ (LUCMD, '(A40)') WORD
  PROMPT = WORD(1:1)
  IF (PROMPT(1:1) .EQ. '!' .OR. PROMPT .EQ. '#') CYCLE
  IF (PROMPT .EQ. '.') THEN
    SELECT CASE(WORD) 
      CASE ('.DENSFIT'); !This is the option to use for EP density fitting
        frame_input%df_potential = .TRUE.
        frame_input%df_matrix    = .TRUE.
      CASE ('.DF-POT');  !EXPERIMENTAL, fitting of the potential only
        frame_input%df_potential = .TRUE.
      CASE ('.DF-MAT');  !EXPERIMENTAL, fitting of the matrix only
        frame_input%df_matrix    = .TRUE.
      CASE DEFAULT
        WRITE (LUPRI,'(/,3A,/)') ' Keyword "',WORD,&
              & '" not recognized in FraME input.'
        CALL lsQUIT('Illegal keyword in FraME input.',lupri)
    END SELECT
  ENDIF
  IF (PROMPT .EQ. '*') EXIT
ENDDO
end subroutine ls_frame_read

!> \brief Prints the **FRAME section of the LSDALTON.INP
!> \param frame_input the ls_frame_input object
!> \param LUPRI the default print unit (LSDALTON.OUT)
subroutine ls_frame_print_input(frame_input,LUPRI)
implicit none
TYPE(ls_frame_input),intent(IN) :: frame_input
INTEGER,intent(IN)              :: LUPRI
WRITE(LUPRI,'(2X,A35,F16.8)') 'df_potential',frame_input%df_potential
WRITE(LUPRI,'(2X,A35,F16.8)') 'df_matrix',   frame_input%df_matrix
end subroutine ls_frame_print_input

!> \brief Copies the **FRAME section of the LSDALTON.INP
!> \param frame_copy on return the copy of the input object
!> \param frame_input the ls_frame_input object
subroutine ls_frame_copy_input(frame_copy,frame_input)
implicit none
TYPE(ls_frame_input),intent(INOUT) :: frame_copy
TYPE(ls_frame_input),intent(IN)    :: frame_input
frame_copy%df_potential = frame_input%df_potential
frame_copy%df_matrix    = frame_input%df_matrix
end subroutine ls_frame_copy_input

END MODULE ls_frame_input_module
