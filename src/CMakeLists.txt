file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/generated)
add_custom_command(
  OUTPUT
    ${PROJECT_BINARY_DIR}/generated/print_info.c
  COMMAND
    ${CMAKE_COMMAND} -DINPUT_DIR=${CMAKE_CURRENT_LIST_DIR}
                     -DTARGET_DIR=${PROJECT_BINARY_DIR}
                     -DCMAKE_SYSTEM=${CMAKE_SYSTEM}
                     -DCMAKE_SYSTEM_PROCESSOR=${CMAKE_SYSTEM_PROCESSOR}
                     -DCMAKE_GENERATOR=${CMAKE_GENERATOR}
                     -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
                     -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
                     -DCMAKE_Fortran_COMPILER_VERSION=${CMAKE_Fortran_COMPILER_VERSION}
                     -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
                     -DCMAKE_C_COMPILER_VERSION=${CMAKE_C_COMPILER_VERSION}
                     -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
                     -DCMAKE_CXX_COMPILER_VERSION=${CMAKE_CXX_COMPILER_VERSION}
                     -DVERSION_FILE=${PROJECT_SOURCE_DIR}/VERSION
                     -DINSTALL_BASDIR=${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_DATADIR}/lsdalton/basis
                     -P ${CMAKE_CURRENT_LIST_DIR}/binary-info.cmake
  MAIN_DEPENDENCY
    ${CMAKE_CURRENT_LIST_DIR}/print_info.c.in
  )

# rebuild print_info.c every time
add_custom_target(
  binary-info
  ALL
  COMMAND
    ${CMAKE_COMMAND} -E touch_nocreate ${CMAKE_CURRENT_LIST_DIR}/print_info.c.in
  DEPENDS
    ${PROJECT_BINARY_DIR}/generated/print_info.c
  )
# See here for the reason why: https://gitlab.kitware.com/cmake/cmake/issues/18399
set_source_files_properties(${PROJECT_BINARY_DIR}/generated/print_info.c
  PROPERTIES
    COMPILE_DEFINITIONS "_XOPEN_SOURCE=500"
    GENERATED 1
  )

include(${CMAKE_CURRENT_LIST_DIR}/definitions.cmake)

# define LSDalton library
add_library(lsdalton
  STATIC
    ${PROJECT_BINARY_DIR}/generated/print_info.c
  )

set_target_properties(lsdalton
  PROPERTIES
    LINKER_LANGUAGE Fortran
  )

target_link_libraries(lsdalton
  PUBLIC
    $<$<TARGET_EXISTS:OpenMP::OpenMP_Fortran>:OpenMP::OpenMP_Fortran>
    $<$<TARGET_EXISTS:MPI::MPI_Fortran>:MPI::MPI_Fortran>
    ${EXTERNAL_LIBS}
  )

add_dependencies(lsdalton binary-info)

install(
  TARGETS
    lsdalton
  DESTINATION
    ${CMAKE_INSTALL_LIBDIR}
  PERMISSIONS
    OWNER_READ OWNER_WRITE OWNER_EXECUTE
    GROUP_READ GROUP_EXECUTE
    WORLD_READ WORLD_EXECUTE
  )

foreach(_dir
  LSint
  SolverUtilities
  cuda
  ddynam
  dft
  geomopt
  linears
  lsdaltonsrc
  lsutil
  mm
  pbc2
  pdpack
  responsesolver
  rsp_properties
  wannier
  xcfun_host
  )
  add_subdirectory(${_dir})
endforeach()

if(ENABLE_DEC)
  add_subdirectory(deccc)
endif()

if(ENABLE_QCMATRIX)
  add_subdirectory(qcmatrix)
endif()

if(ENABLE_OPENRSP)
  add_subdirectory(openrsp)
endif()

if(ENABLE_FRAME)
  add_subdirectory(FraME)
endif()

if(ENABLE_PCMSOLVER)
  add_subdirectory(pcm)
endif()

if(ENABLE_PYTHON_INTERFACE)
  if(NOT TARGET Python::Interpreter)
    set(ENABLE_PYTHON_INTERFACE OFF CACHE BOOL "Enable Python interface through CFFI" FORCE)
    message(STATUS "Python interface DISABLED")
  else()
    # TODO verify that CFFI is actually available
    message(STATUS "Python interface via CFFI ENABLED")
    add_subdirectory(pylsdalton)
  endif()
endif()

if(NOT ENABLE_CHEMSHELL)
  add_executable(lsdalton.x
      ${CMAKE_CURRENT_LIST_DIR}/lsdaltonsrc/lsdalton_wrapper.f90
    )
  target_compile_options(lsdalton.x
    PRIVATE
      "${LSDALTON_Fortran_FLAGS};${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
    )
  set_target_properties(lsdalton.x PROPERTIES LINKER_LANGUAGE Fortran)
  target_link_libraries(lsdalton.x
    PRIVATE
      lsdalton
    )
  install(
    TARGETS
      lsdalton.x
    DESTINATION
      ${CMAKE_INSTALL_BINDIR}
    PERMISSIONS
      OWNER_READ OWNER_WRITE OWNER_EXECUTE
      GROUP_READ GROUP_EXECUTE
      WORLD_READ WORLD_EXECUTE
    )
endif()

add_executable(lslib_tester.x
    ${CMAKE_CURRENT_LIST_DIR}/lsdaltonsrc/LSlib_tester.F90
    ${CMAKE_CURRENT_LIST_DIR}/lsdaltonsrc/LSlib_tester_module.F90
  )
target_compile_options(lslib_tester.x
  PRIVATE
    "${LSDALTON_Fortran_FLAGS};${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )
set_target_properties(lslib_tester.x PROPERTIES LINKER_LANGUAGE Fortran)
target_link_libraries(lslib_tester.x
  PRIVATE
    lsdalton
  )

install(
  TARGETS
    lslib_tester.x
  DESTINATION
    ${CMAKE_INSTALL_BINDIR}
  PERMISSIONS
    OWNER_READ OWNER_WRITE OWNER_EXECUTE
    GROUP_READ GROUP_EXECUTE
    WORLD_READ WORLD_EXECUTE
  )
