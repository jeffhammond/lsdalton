module lsmpi_SendRecv
  use lsmpi_param
  use precision
  use lsmpi_typeParam
  use,intrinsic :: iso_c_binding,only:c_ptr,c_f_pointer,c_associated,&
       & c_null_ptr,c_loc
  use LSparameters
  use memory_handling, only: mem_alloc,mem_dealloc, max_mem_used_global,&
       & longintbuffersize, print_maxmem, stats_mem, copy_from_mem_stats,&
       & init_globalmemvar, stats_mpi_mem, copy_to_mem_stats, &
       & MemModParamPrintMemory, Print_Memory_info, &
       & MemModParamPrintMemorylupri, mem_allocated_global
#ifdef VAR_MPI
  use infpar_module
  use lsmpi_module
#endif

  INTERFACE lsmpi_send
     MODULE PROCEDURE lsmpi_send_realkV_4,lsmpi_send_realkV_8
  END INTERFACE lsmpi_send

  INTERFACE lsmpi_recv
     MODULE PROCEDURE lsmpi_recv_realkV_4,lsmpi_recv_realkV_8,&
          & lsmpi_recv_intV4,lsmpi_recv_realk
  END INTERFACE lsmpi_recv

  INTERFACE lsmpi_isend
     MODULE PROCEDURE lsmpi_isend_intV4,lsmpi_isend_realk,lsmpi_isend_realkV
  end INTERFACE lsmpi_isend

  INTERFACE lsmpi_irecv
     MODULE PROCEDURE lsmpi_irecv_realkV
  end INTERFACE lsmpi_irecv

  INTERFACE ls_mpisendrecv
     MODULE PROCEDURE ls_mpisendrecv_integer,& 
          &           ls_mpisendrecv_integerV,ls_mpisendrecv_integerV_wrapper8,&
          &           ls_mpisendrecv_longV,ls_mpisendrecv_longV_wrapper8, &
          &           ls_mpisendrecv_realk, &
          &           ls_mpisendrecv_realkV,ls_mpisendrecv_realkV_wrapper8,&
          &           ls_mpisendrecv_realkM, ls_mpisendrecv_realkT,&
          &           ls_mpisendrecv_realkQ,&
          &           ls_mpisendrecv_logical4,ls_mpisendrecv_logical8, &
          &           ls_mpisendrecv_logical4V,ls_mpisendrecv_logical4V_wrapper8,&
          &           ls_mpisendrecv_logical8V,ls_mpisendrecv_logical8V_wrapper8,&
          &           ls_mpisendrecv_logical4M,ls_mpisendrecv_logical8M,&
          &           ls_mpisendrecv_short, &
          &           ls_mpisendrecv_charac, &
          &           ls_mpisendrecv_characV, ls_mpisendrecv_characV_wrapper8,&
          &           ls_mpisendrecv_characV2,ls_mpisendrecv_characV2_wrapper8, &
          &           ls_mpisendrecv_shortV, ls_mpisendrecv_shortV_wrapper8,&
          &           ls_mpisendrecv_long
  END INTERFACE ls_mpisendrecv

  public :: lsmpi_send,lsmpi_recv,lsmpi_isend,ls_mpisendrecv,lsmpi_irecv

  private

contains
  ! ########################################################################
  !                          MPI SEND
  ! ########################################################################
  subroutine lsmpi_send_realkV_8(buffer,nbuf,comm,receiver)
    implicit none
    real(realk) :: buffer(:)
    integer(kind=8) :: nbuf
    integer(kind=ls_mpik) :: comm
    integer(kind=ls_mpik) :: receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: tag,dtype,ierr,nMPI
    integer(kind=8) :: k,i
    tag   = 124_ls_mpik
    ierr  = 0_ls_mpik
    dtype = MPI_DOUBLE_PRECISION
    k=SPLIT_MPI_MSG
    do i=1,nbuf,k
       nMPI=k
       if(((nbuf-i)<k).and.(mod(nbuf-i+1,k)/=0))nMPI=mod(nbuf,k)
       call MPI_SEND(buffer(i:i+nMPI-1),nMPI,dtype,receiver,tag,comm,ierr)
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif 
  end subroutine lsmpi_send_realkV_8

  subroutine lsmpi_send_realkV_4(buffer,nbuf,comm,receiver)
    implicit none
    real(realk) :: buffer(:)
    integer(kind=4) :: nbuf
    integer(kind=ls_mpik) :: comm
    integer(kind=ls_mpik) :: receiver
    integer(kind=8) :: n
    n=nbuf
    call lsmpi_send_realkV_8(buffer,n,comm,receiver)
  end subroutine lsmpi_send_realkV_4

  ! ########################################################################
  !                          MPI RECV
  ! ########################################################################

  subroutine lsmpi_recv_realkV_8(buffer,nbuf,comm,sender)
    implicit none
    real(realk) :: buffer(:)
    integer(kind=8) :: nbuf
    integer(kind=ls_mpik) :: comm
    integer(kind=ls_mpik) :: sender
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: tag,dtype,ierr,nMPI
    integer(kind=8) :: k,i
    tag   = 124_ls_mpik
    ierr  = 0_ls_mpik
    dtype = MPI_DOUBLE_PRECISION
    k=SPLIT_MPI_MSG
    do i=1,nbuf,k
       nMPI=k
       if(((nbuf-i)<k).and.(mod(nbuf-i+1,k)/=0))nMPI=mod(nbuf,k)
       call MPI_RECV(buffer(i:i+nMPI-1),nMPI,dtype,sender,tag,comm,lsmpi_status,ierr)
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif 
  end subroutine lsmpi_recv_realkV_8

  subroutine lsmpi_recv_realkV_4(buffer,nbuf,comm,sender)
    implicit none
    real(realk) :: buffer(:)
    integer(kind=4) :: nbuf
    integer(kind=ls_mpik) :: comm
    integer(kind=ls_mpik) :: sender
    integer(kind=8) :: n
    n=nbuf
    call lsmpi_recv_realkV_8(buffer,n,comm,sender)
  end subroutine lsmpi_recv_realkV_4

  subroutine lsmpi_recv_intV4(buf,nbuf,sender,TAG1,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: nbuf,sender,TAG1,comm
    integer(kind=4) :: buf(nbuf)
    integer(kind=ls_mpik) :: IERR
#ifdef VAR_MPI
    call MPI_RECV(buf,nbuf,MPI_INTEGER4,sender,TAG1,comm,lsmpi_status,ierr)
#endif
  end subroutine lsmpi_recv_intV4

  subroutine lsmpi_recv_realk(buf,sender,TAG1,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: sender,TAG1,comm
    real(realk),intent(inout) :: buf
    integer(kind=ls_mpik) :: IERR
    integer(kind=ls_mpik) :: nbuf
    nbuf = 1
#ifdef VAR_MPI
    call MPI_RECV(buf,nbuf,MPI_DOUBLE_PRECISION,sender,TAG1,comm,lsmpi_status,ierr)
#endif
  end subroutine lsmpi_recv_realk

  !####################################################################
  !             SENDRECV
  !####################################################################

  subroutine ls_mpisendrecv_integer(buffer,comm,sender,receiver)
    implicit none
    integer(kind=4) :: buffer
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,thesize,datatype,mynum,tag

    IERR=0
    tag=0
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)
    DATATYPE = MPI_INTEGER4
    THESIZE = 1      

    if(mynum.EQ.sender) then ! send stuff to receiver
       call MPI_SEND(buffer,thesize,DATATYPE,receiver,tag,comm,ierr)
    else if(mynum.EQ.receiver) then  ! receive stuff from sender
       call MPI_RECV(buffer,thesize,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
    else ! Error: Node should be either sender or receiver
       print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
       call lsquit('ls_mpisendrecv_integer: &
            & Rank is neither sender nor receiver',-1)
    end if
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpisendrecv_integer

  subroutine ls_mpisendrecv_long(buffer,comm,sender,receiver)
    implicit none
    integer(kind=8) :: buffer
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,thesize,datatype
    integer(kind=ls_mpik) :: mynum,tag,dummystat
    IERR=0
    tag=1
    dummystat=0
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)
    DATATYPE = MPI_INTEGER8
    THESIZE = 1

    if(mynum.EQ.sender) then ! send stuff to receiver
       call MPI_SEND(buffer,thesize,DATATYPE,receiver,tag,comm,ierr)
    else if(mynum.EQ.receiver) then  ! receive stuff from sender
       call MPI_RECV(buffer,thesize,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
    else ! Error: Node should be either sender or receiver
       print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
       call lsquit('ls_mpisendrecv_long: &
            & Rank is neither sender nor receiver',-1)
    end if
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpisendrecv_long

  subroutine ls_mpisendrecv_short(buffer,comm,sender,receiver)
    implicit none
    integer(kind=short) :: buffer
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=4) :: intbuffer
    !Convert from short integer to 32 bit integer
    intbuffer = buffer
    call ls_mpisendrecv_integer(intbuffer,comm,sender,receiver)
    !Convert back
    buffer = INT(intbuffer,kind=short)
#endif
  end subroutine ls_mpisendrecv_short

  subroutine ls_mpisendrecv_shortV_wrapper8(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=short)   :: buffer(:)
    integer(kind=8)       :: nbuf
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
    !
#ifdef VAR_MPI
    integer(kind=4),pointer :: intbuffer(:)
    integer(kind=8) :: I
    call mem_alloc(intbuffer,nbuf)
    DO I=1,nbuf
       INTBUFFER(I) = BUFFER(I)
    ENDDO
    call ls_mpisendrecv_integerV_wrapper8(intbuffer,nbuf,comm,sender,receiver)
    DO I=1,nbuf
       BUFFER(I) = INT(INTBUFFER(I),kind=short)
    ENDDO
    call mem_dealloc(intbuffer)
#endif
  end subroutine ls_mpisendrecv_shortV_wrapper8

  subroutine ls_mpisendrecv_shortV(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=short)   :: buffer(:)
    integer(kind=4)       :: nbuf
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI      
    integer(kind=8) :: n
    n = nbuf
    call ls_mpisendrecv_shortV_wrapper8(buffer,n,comm,sender,receiver)
#endif
  end subroutine ls_mpisendrecv_shortV

  subroutine ls_mpisendrecv_longV_wrapper8(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=8) :: nbuf
    integer(kind=8) :: buffer(:)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,thesize,datatype,mynum,tag,nMPI
    integer(kind=8) :: i,k
    IERR=0
    DATATYPE = MPI_INTEGER8
    k=SPLIT_MPI_MSG
    tag=4
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)
    do i=1,nbuf,k
       nMPI=k
       !if((nbuf-i)<k)nMPI=mod(nbuf,k)
       if(((nbuf-i)<k).and.(mod(nbuf-i+1,k)/=0))nMPI=mod(nbuf,k)
       !         call ls_mpisendrecv_longV(buffer(i:i+nMPI-1),nMPI,comm,sender,receiver)
       if(mynum.EQ.sender) then ! send stuff to receiver
          call MPI_SEND(buffer(i:i+nMPI-1),nMPI,DATATYPE,receiver,tag,comm,ierr)
       else if(mynum.EQ.receiver) then  ! receive stuff from sender
          call MPI_RECV(buffer(i:i+nMPI-1),nMPI,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
       else ! Error: Node should be either sender or receiver
          print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
          call lsquit('ls_mpisendrecv_longV: &
               & Rank is neither sender nor receiver',-1)
       end if
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpisendrecv_longV_wrapper8

  subroutine ls_mpisendrecv_longV(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=4) :: nbuf
    integer(kind=8) :: buffer(:)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=8) :: n
    n = nbuf
    call ls_mpisendrecv_longV_wrapper8(buffer,n,comm,sender,receiver)
#endif
  end subroutine ls_mpisendrecv_longV

  subroutine ls_mpisendrecv_integerV_wrapper8(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=8) :: nbuf
    integer(kind=4) :: buffer(:)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,thesize,datatype,mynum,tag,nMPI
    integer(kind=8) :: i,k
    IERR=0
    DATATYPE = MPI_INTEGER4
    k=SPLIT_MPI_MSG
    tag=4
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)
    do i=1,nbuf,k
       nMPI=k
       !if((nbuf-i)<k)nMPI=mod(nbuf,k)
       if(((nbuf-i)<k).and.(mod(nbuf-i+1,k)/=0))nMPI=mod(nbuf,k)
       !         call ls_mpisendrecv_longV(buffer(i:i+nMPI-1),nMPI,comm,sender,receiver)
       if(mynum.EQ.sender) then ! send stuff to receiver
          call MPI_SEND(buffer(i:i+nMPI-1),nMPI,DATATYPE,receiver,tag,comm,ierr)
       else if(mynum.EQ.receiver) then  ! receive stuff from sender
          call MPI_RECV(buffer(i:i+nMPI-1),nMPI,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
       else ! Error: Node should be either sender or receiver
          print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
          call lsquit('ls_mpisendrecv_longV: &
               & Rank is neither sender nor receiver',-1)
       end if
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpisendrecv_integerV_wrapper8

  subroutine ls_mpisendrecv_integerV(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=4) :: nbuf
    integer(kind=4) :: buffer(:)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=8) :: n
    n = nbuf
    call ls_mpisendrecv_integerV_wrapper8(buffer,n,comm,sender,receiver)
#endif
  end subroutine ls_mpisendrecv_integerV

  subroutine ls_mpisendrecv_realk(buffer,comm,sender,receiver)
    implicit none
    real(realk) :: buffer
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,datatype,thesize
    integer(kind=ls_mpik) :: mynum,tag
    IERR=0
    tag=6
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)
    DATATYPE = MPI_DOUBLE_PRECISION
    THESIZE = 1
    if(mynum.EQ.sender) then ! send stuff to receiver
       call MPI_SEND(buffer,thesize,DATATYPE,receiver,tag,comm,ierr)
    else if(mynum.EQ.receiver) then  ! receive stuff from sender
       call MPI_RECV(buffer,thesize,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
    else ! Error: Node should be either sender or receiver
       print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
       call lsquit('ls_mpisendrecv_realk: &
            & Rank is neither sender nor receiver',-1)
    end if
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpisendrecv_realk

  subroutine ls_mpisendrecv_realkV_wrapper8(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=8) :: nbuf
    real(realk) :: buffer(:)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,datatype,mynum,tag,nMPI
    integer(kind=8) :: i,k
    IERR=0
    k=SPLIT_MPI_MSG
    tag=7
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)
    DATATYPE = MPI_DOUBLE_PRECISION
    do i=1,nbuf,k
       nMPI=k
       if(((nbuf-i)<k).and.(mod(nbuf-i+1,k)/=0))nMPI=mod(nbuf,k)
       if(mynum.EQ.sender) then ! send stuff to receiver
          call MPI_SEND(buffer(i:i+nMPI-1),nMPI,DATATYPE,receiver,tag,comm,ierr)
       else if(mynum.EQ.receiver) then  ! receive stuff from sender
          call MPI_RECV(buffer(i:i+nMPI-1),nMPI,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
       else ! Error: Node should be either sender or receiver
          print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
          call lsquit('ls_mpisendrecv_realkV: &
               & Rank is neither sender nor receiver',-1)
       end if
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpisendrecv_realkV_wrapper8

  subroutine ls_mpisendrecv_realkV(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=4) :: nbuf
    real(realk) :: buffer(:)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=8) :: n
    n = nbuf
    call ls_mpisendrecv_realkV_wrapper8(buffer,n,comm,sender,receiver)
#endif
  end subroutine ls_mpisendrecv_realkV

  subroutine ls_mpisendrecv_realkM(buffer,n1,n2,comm,sender,receiver)
    implicit none
    integer :: n1,n2
    real(realk),target :: buffer(n1,n2)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    real(realk),pointer :: buf(:)
    integer(kind=8)::n
    n = (i8*n1)*n2
#ifdef VAR_PTR_RESHAPE
    buf(1:n) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buf,[n])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpisendrecv_realkV_wrapper8(buf,n,comm,sender,receiver)
    nullify(buf)
#endif
  end subroutine ls_mpisendrecv_realkM

  subroutine ls_mpisendrecv_realkT(buffer,n1,n2,n3,comm,sender,receiver)
    implicit none
    integer :: n1,n2,n3
    real(realk),target :: buffer(n1,n2,n3)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    real(realk),pointer :: buf(:)
    integer(kind=8)::n
    n = (i8*n1)*n2*n3
#ifdef VAR_PTR_RESHAPE
    buf(1:n) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1,1)),buf,[n])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpisendrecv_realkV_wrapper8(buf,n,comm,sender,receiver)
    nullify(buf)
#endif
  end subroutine ls_mpisendrecv_realkT

  subroutine ls_mpisendrecv_realkQ(buffer,n1,n2,n3,n4,comm,sender,receiver)
    implicit none
    integer :: n1,n2,n3,n4
    real(realk),target :: buffer(n1,n2,n3,n4)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    real(realk),pointer :: buf(:)
    integer(kind=8)::n
    n = (i8*n1)*n2*n3*n4
#ifdef VAR_PTR_RESHAPE
    buf(1:n) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1,1,1)),buf,[n])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpisendrecv_realkV_wrapper8(buf,n,comm,sender,receiver)
    nullify(buf)
#endif
  end subroutine ls_mpisendrecv_realkQ

  subroutine ls_mpisendrecv_logical8(buffer,comm,sender,receiver)
    implicit none
    logical(kind=8) :: buffer
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,thesize,datatype
    integer(kind=ls_mpik) :: mynum,tag
    integer(kind=MPI_ADDRESS_KIND) :: mpi_logical_extent,lb
    logical(kind=4) :: buffer4
    IERR=0
    tag=11
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)

    DATATYPE = MPI_LOGICAL
    THESIZE = 1
    !     Send/receive 
    call MPI_TYPE_GET_EXTENT(MPI_LOGICAL,lb,mpi_logical_extent,ierr)
    if(mynum.EQ.sender) then ! send stuff to receiver
       IF(mpi_logical_extent.EQ.4)THEN
          !32 bit mpi logical
          BUFFER4 = BUFFER
          call MPI_SEND(buffer4,thesize,DATATYPE,receiver,tag,comm,ierr)
       ELSE
          !64 bit mpi logical
          call MPI_SEND(buffer,thesize,DATATYPE,receiver,tag,comm,ierr)
       ENDIF
    else if(mynum.EQ.receiver) then  ! receive stuff from sender
       IF(mpi_logical_extent.EQ.4)THEN
          !32 bit mpi logical
          call MPI_RECV(buffer4,thesize,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
          BUFFER = BUFFER4
       ELSE
          !64 bit mpi logical
          call MPI_RECV(buffer,thesize,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
       ENDIF
    else ! Error: Node should be either sender or receiver
       print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
       call lsquit('ls_mpisendrecv_logical: &
            & Rank is neither sender nor receiver',-1)
    end if
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpisendrecv_logical8
  subroutine ls_mpisendrecv_logical4(buffer,comm,sender,receiver)
    implicit none
    logical(kind=4) :: buffer
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,thesize,datatype
    integer(kind=ls_mpik) :: mynum,tag
    logical(kind=8) :: buffer8
    integer(kind=MPI_ADDRESS_KIND) :: mpi_logical_extent,lb
    IERR=0
    tag=11
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)

    DATATYPE = MPI_LOGICAL
    THESIZE = 1

    !     Send/receive 
    call MPI_TYPE_GET_EXTENT(MPI_LOGICAL,lb,mpi_logical_extent,ierr)
    if(mynum.EQ.sender) then ! send stuff to receiver
       IF(mpi_logical_extent.EQ.4)THEN
          !32 bit mpi logical
          call MPI_SEND(buffer,thesize,DATATYPE,receiver,tag,comm,ierr)
       ELSE
          BUFFER8 = BUFFER
          call MPI_SEND(buffer8,thesize,DATATYPE,receiver,tag,comm,ierr)
       ENDIF
    else if(mynum.EQ.receiver) then  ! receive stuff from sender
       IF(mpi_logical_extent.EQ.4)THEN
          !32 bit mpi logical
          call MPI_RECV(buffer,thesize,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
       ELSE
          call MPI_RECV(buffer8,thesize,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
          BUFFER = BUFFER8
       ENDIF
    else ! Error: Node should be either sender or receiver
       print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
       call lsquit('ls_mpisendrecv_logical: &
            & Rank is neither sender nor receiver',-1)
    end if
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpisendrecv_logical4

  subroutine ls_mpisendrecv_logical8V_wrapper8(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=8) :: nbuf
    logical(kind=8) :: buffer(:)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,datatype,mynum,tag,nMPI
    integer(kind=8) :: i,k,j
    logical(kind=4),pointer :: buffer4(:)
    integer(kind=MPI_ADDRESS_KIND) :: mpi_logical_extent,lb
    IERR=0
    k=SPLIT_MPI_MSG
    tag=12
    DATATYPE = MPI_LOGICAL
    call MPI_TYPE_GET_EXTENT(MPI_LOGICAL,lb,mpi_logical_extent,ierr)
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)
    do i=1,nbuf,k
       nMPI=k
       if(((nbuf-i)<k).and.(mod(nbuf-i+1,k)/=0))nMPI=mod(nbuf,k)
       if(mynum.EQ.sender) then ! send stuff to receiver
          IF(mpi_logical_extent.EQ.4)THEN
             !32 bit mpi logical
             call mem_alloc(buffer4,nMPI)
             do J = 1,nMPI
                buffer4(J) = buffer(i+J-1)
             enddo
             call MPI_SEND(buffer4,nMPI,DATATYPE,receiver,tag,comm,ierr)
             call mem_dealloc(buffer4)
          ELSE
             !64 bit mpi logical
             call MPI_SEND(buffer(i:i+nMPI-1),nMPI,DATATYPE,receiver,tag,comm,ierr)
          ENDIF
       else if(mynum.EQ.receiver) then  ! receive stuff from sender
          IF(mpi_logical_extent.EQ.4)THEN
             !32 bit mpi logical
             call mem_alloc(buffer4,nMPI)
             call MPI_RECV(buffer4,nMPI,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
             do J = 1,nMPI
                buffer(i+J-1) = buffer4(J)
             enddo
             call mem_dealloc(buffer4)
          ELSE
             !64 bit mpi logical
             call MPI_RECV(buffer(i:i+nMPI-1),nMPI,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
          ENDIF
       else ! Error: Node should be either sender or receiver
          print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
          call lsquit('ls_mpisendrecv_logical8V_wrapper8: &
               & Rank is neither sender nor receiver',-1)
       end if
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpisendrecv_logical8V_wrapper8

  subroutine ls_mpisendrecv_logical8V(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=4) :: nbuf
    logical(kind=8) :: buffer(:)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=8) :: n
    n = nbuf 
    call ls_mpisendrecv_logical8V_wrapper8(buffer,n,comm,sender,receiver)
#endif
  end subroutine ls_mpisendrecv_logical8V

  subroutine ls_mpisendrecv_logical4V_wrapper8(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=8) :: nbuf
    logical(kind=4) :: buffer(:)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,datatype,mynum,tag,nMPI
    integer(kind=8) :: i,k,j
    logical(kind=8),pointer :: buffer8(:)
    integer(kind=MPI_ADDRESS_KIND) :: mpi_logical_extent,lb
    IERR=0
    k=SPLIT_MPI_MSG
    tag=12
    DATATYPE = MPI_LOGICAL
    call MPI_TYPE_GET_EXTENT(MPI_LOGICAL,lb,mpi_logical_extent,ierr)
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)
    do i=1,nbuf,k
       nMPI=k
       if(((nbuf-i)<k).and.(mod(nbuf-i+1,k)/=0))nMPI=mod(nbuf,k)
       if(mynum.EQ.sender) then ! send stuff to receiver
          IF(mpi_logical_extent.EQ.4)THEN
             !32 bit mpi logical
             call MPI_SEND(buffer(i:i+nMPI-1),nMPI,DATATYPE,receiver,tag,comm,ierr)
          ELSE
             !64 bit mpi logical
             call mem_alloc(buffer8,nMPI)
             do J = 1,nMPI
                buffer8(J) = buffer(i+J-1)
             enddo
             call MPI_SEND(buffer8,nMPI,DATATYPE,receiver,tag,comm,ierr)
             call mem_dealloc(buffer8)
          ENDIF
       else if(mynum.EQ.receiver) then  ! receive stuff from sender
          IF(mpi_logical_extent.EQ.4)THEN
             !32 bit mpi logical
             call MPI_RECV(buffer(i:i+nMPI-1),nMPI,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
          ELSE
             !64 bit mpi logical
             call mem_alloc(buffer8,nMPI)
             call MPI_RECV(buffer8,nMPI,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
             do J = 1,nMPI
                buffer(i+J-1) = buffer8(J)
             enddo
             call mem_dealloc(buffer8)
          ENDIF
       else ! Error: Node should be either sender or receiver
          print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
          call lsquit('ls_mpisendrecv_logical8V_wrapper8: &
               & Rank is neither sender nor receiver',-1)
       end if
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpisendrecv_logical4V_wrapper8

  subroutine ls_mpisendrecv_logical4V(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=4) :: nbuf
    logical(kind=4) :: buffer(:)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=8) :: n
    n = nbuf
    call ls_mpisendrecv_logical4V_wrapper8(buffer,n,comm,sender,receiver)
#endif
  end subroutine ls_mpisendrecv_logical4V

  subroutine ls_mpisendrecv_logical4M(buffer,n1,n2,comm,sender,receiver)
    implicit none
    integer(kind=4) :: n1,n2
    logical(kind=4),target :: buffer(n1,n2)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    logical(kind=4),pointer :: buf(:)      
    integer(kind=8)::n
    n = (i8*n1)*n2
#ifdef VAR_PTR_RESHAPE
    buf(1:n) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buf,[n])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpisendrecv_logical4V_wrapper8(buf,n,comm,sender,receiver)
    nullify(buf)
#endif
  end subroutine ls_mpisendrecv_logical4M

  subroutine ls_mpisendrecv_logical4M_wrapper8(buffer,n1,n2,comm,sender,receiver)
    implicit none
    integer(kind=8) :: n1,n2
    logical(kind=4),target :: buffer(n1,n2)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    logical(kind=4),pointer :: buf(:)      
    integer(kind=8)::n
    n = (i8*n1)*n2
#ifdef VAR_PTR_RESHAPE
    buf(1:n) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buf,[n])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpisendrecv_logical4V_wrapper8(buf,n,comm,sender,receiver)
    nullify(buf)
#endif
  end subroutine ls_mpisendrecv_logical4M_wrapper8

  subroutine ls_mpisendrecv_logical8M(buffer,n1,n2,comm,sender,receiver)
    implicit none
    integer(kind=4) :: n1,n2
    logical(kind=8),target :: buffer(n1,n2)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    logical(kind=8),pointer :: buf(:)      
    integer(kind=8)::n
    n = (i8*n1)*n2
#ifdef VAR_PTR_RESHAPE
    buf(1:n) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buf,[n])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpisendrecv_logical8V_wrapper8(buf,n,comm,sender,receiver)
    nullify(buf)
#endif
  end subroutine ls_mpisendrecv_logical8M

  subroutine ls_mpisendrecv_logical8M_wrapper8(buffer,n1,n2,comm,sender,receiver)
    implicit none
    integer(kind=8) :: n1,n2
    logical(kind=8),target :: buffer(n1,n2)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    logical(kind=8),pointer :: buf(:)      
    integer(kind=8)::n
    n = (i8*n1)*n2
#ifdef VAR_PTR_RESHAPE
    buf(1:n) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buf,[n])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpisendrecv_logical8V_wrapper8(buf,n,comm,sender,receiver)
    nullify(buf)
#endif
  end subroutine ls_mpisendrecv_logical8M_wrapper8

  subroutine ls_mpisendrecv_charac(buffer,comm,sender,receiver)
    implicit none
    character :: buffer
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,thesize,datatype,mynum,tag
    IERR=0
    tag=14
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)

    DATATYPE = MPI_CHARACTER
    THESIZE = 1

    !     Send/receive 
    if(mynum.EQ.sender) then ! send stuff to receiver
       call MPI_SEND(buffer,thesize,DATATYPE,receiver,tag,comm,ierr)
    else if(mynum.EQ.receiver) then  ! receive stuff from sender
       call MPI_RECV(buffer,thesize,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
    else ! Error: Node should be either sender or receiver
       print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
       call lsquit('ls_mpisendrecv_charac: &
            & Rank is neither sender nor receiver',-1)
    end if
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpisendrecv_charac

  subroutine ls_mpisendrecv_characV_wrapper8(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=8) :: nbuf
    character*(*) :: buffer
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,thesize,datatype,mynum,tag,nMPI
    integer(kind=8) :: i,k
    IERR=0
    DATATYPE = MPI_CHARACTER
    k=SPLIT_MPI_MSG
    tag=15
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)

    do i=1,nbuf,k
       nMPI=k
       if(((nbuf-i)<k).and.(mod(nbuf-i+1,k)/=0))nMPI=mod(nbuf,k)
       if(mynum.EQ.sender) then ! send stuff to receiver
          call MPI_SEND(buffer(i:i+nMPI-1),nMPI,DATATYPE,receiver,tag,comm,ierr)
       else if(mynum.EQ.receiver) then  ! receive stuff from sender
          call MPI_RECV(buffer(i:i+nMPI-1),nMPI,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
       else ! Error: Node should be either sender or receiver
          print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
          call lsquit('ls_mpisendrecv_characV: &
               & Rank is neither sender nor receiver',-1)
       end if
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpisendrecv_characV_wrapper8

  subroutine ls_mpisendrecv_characV(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=4) :: nbuf
    character*(*) :: buffer
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=8) :: n
    n = nbuf
    call ls_mpisendrecv_characV_wrapper8(buffer,n,comm,sender,receiver)
#endif
  end subroutine ls_mpisendrecv_characV

  subroutine ls_mpisendrecv_characV2_wrapper8(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=8) :: nbuf
    character :: buffer(nbuf)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,thesize,datatype,mynum,tag,nMPI
    integer(kind=8) :: i,k
    IERR=0
    DATATYPE = MPI_CHARACTER
    k=SPLIT_MPI_MSG
    tag=15
    ! Get rank within specific communicator
    call get_rank_for_comm(comm,mynum)

    do i=1,nbuf,k
       nMPI=k
       if(((nbuf-i)<k).and.(mod(nbuf-i+1,k)/=0))nMPI=mod(nbuf,k)
       if(mynum.EQ.sender) then ! send stuff to receiver
          call MPI_SEND(buffer(i:i+nMPI-1),nMPI,DATATYPE,receiver,tag,comm,ierr)
       else if(mynum.EQ.receiver) then  ! receive stuff from sender
          call MPI_RECV(buffer(i:i+nMPI-1),nMPI,DATATYPE,sender,tag,comm,lsmpi_status,ierr)
       else ! Error: Node should be either sender or receiver
          print '(a,3i6)', 'Rank,sender,receiver',mynum,sender,receiver
          call lsquit('ls_mpisendrecv_characV: &
               & Rank is neither sender nor receiver',-1)
       end if
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpisendrecv_characV2_wrapper8

  subroutine ls_mpisendrecv_characV2(buffer,nbuf,comm,sender,receiver)
    implicit none
    integer(kind=4) :: nbuf
    character :: buffer(nbuf)
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=ls_mpik) :: sender,receiver
#ifdef VAR_MPI
    integer(kind=8) :: n
    n = nbuf
    call ls_mpisendrecv_characV2_wrapper8(buffer,n,comm,sender,receiver)
#endif
  end subroutine ls_mpisendrecv_characV2

  !####################################################################
  !             ISEND IRECV
  !####################################################################

  subroutine lsmpi_isend_intV4(buf,nbuf,receiver,TAG1,comm,request1)
    implicit none
    integer(kind=ls_mpik),intent(in) :: nbuf,receiver,TAG1,comm
    integer(kind=ls_mpik),intent(inout) :: request1
    integer(kind=4) :: buf(nbuf)
    integer(kind=ls_mpik) :: IERR
#ifdef VAR_MPI
    call MPI_ISEND(buf,nbuf,MPI_INTEGER4,receiver,TAG1,comm,request1,ierr)
    IF(ierr.NE.0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine lsmpi_isend_intV4

  subroutine lsmpi_isend_realk(buf,receiver,TAG1,comm,req1)
    implicit none
    integer(kind=ls_mpik),intent(in) :: receiver,TAG1,comm
    integer(kind=ls_mpik),intent(inout) :: req1
    real(realk) :: buf
    integer(kind=ls_mpik) :: IERR,nbuf
    nbuf = 1
#ifdef VAR_MPI
    call MPI_ISEND(buf,nbuf,MPI_DOUBLE_PRECISION,receiver,TAG1,comm,req1,ierr)
    IF(ierr.NE.0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine lsmpi_isend_realk

  subroutine lsmpi_isend_realkV(buf,nbuf,receiver,TAG1,comm,req1)
    implicit none
    integer(kind=ls_mpik),intent(in) :: receiver,TAG1,comm,nbuf
    integer(kind=ls_mpik),intent(inout) :: req1
    real(realk) :: buf(nbuf)
    integer(kind=ls_mpik) :: IERR
#ifdef VAR_MPI
    call MPI_ISEND(buf,nbuf,MPI_DOUBLE_PRECISION,receiver,TAG1,comm,req1,ierr)
    IF(ierr.NE.0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine lsmpi_isend_realkV

  subroutine lsmpi_irecv_realkV(buf,nbuf,sender,TAG1,comm,req1)
    implicit none
    integer(kind=ls_mpik),intent(in) :: sender,TAG1,comm,nbuf
    integer(kind=ls_mpik),intent(inout) :: req1
    real(realk) :: buf(nbuf)
    integer(kind=ls_mpik) :: IERR
#ifdef VAR_MPI
    call MPI_IRECV(buf,nbuf,MPI_DOUBLE_PRECISION,sender,TAG1,comm,req1,ierr)
    IF(ierr.NE.0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine lsmpi_irecv_realkV

end module lsmpi_SendRecv


