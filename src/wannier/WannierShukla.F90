module wannier_shukla

  
  use precision
  use timings_module
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use molecule_typetype, only: &
       moleculeinfo
  use typedeftype, only: &
       daltoninput, &
       lssetting
  use lattice_storage
  use wlattice_domains, only: &
       ltdom_init, &
       ltdom_free, &
       ltdom_getdomain, &
       lattice_domains_type
  use memory_handling, only: &
       mem_alloc, mem_dealloc
  use matrix_module, only: &
       matrix, matrixp
  use matrix_operations, only: &
       mat_init, &
       mat_zero, &
       mat_identity, &
       mat_free, &
       mat_mul, &
       mat_diag_f, &
       mat_print, &
       mat_abs_max_elm, &
       mat_sqnorm2, &
       mat_section, &
       mat_daxpy, &
       mat_insert_section, &
       mat_trans
  use wannier_fockMO, only: &
       w_calc_fock_mo, w_calc_mo_l, w_calc_AO2MO_shukla
  use wannier_complex_utils, only: &
       wannier_dggev, &
       sort_virt, &
       sort_eigenvalue
  use wannier_test_module, only: &
       check_unitary_U, &
       check_unitary_G, &
       wtest_orthonormality_wspace, &
       wtest_projocc, &
       wtest_density, &
       wtest_virtspace, &
       wtest_fullsol, &
       wtest_unitary_mat
  use wannier_startguess_module, only: &
       wannier_orthogonalisation
  use wannier_orthpot
  use wannier_dens
  
  implicit none
  private

  public :: scf_shuklaAO, scf_shuklaMO, projectout_occspace

contains
  
  !> @brief Shukla's algorithm for the SCF iteration
  !>
  !> The Fock matrix is tranformed to MO space
  !> the orthogonalising potential is added only to the OCCUPIED orbitals
  !> The virtual orbitals are obtained as the orthogonal complement of the
  !> occupied ones
  !>
  !> @author Elisa Rebolini
  !> @date September 2016
  !> @param fockmat Fock matrix AO basis in lattice storage
  !> @param mos Input/output MO coefficients in lattice storage
  !> @param wconf  Contains the Wannier configuration
  !> @param nocc Number of occupied orbitals
  !> @param lupri Default print-unit for output
  !> @param luerr Default print-unit for termination
  
  subroutine scf_shuklaMO(fockmat, mos, dens, smat, wconf, nocc, lupri, luerr)

    type(lmatrixp), intent(inout)       :: fockmat, mos, dens, smat
    type(wannier_config), intent(inout) :: wconf
    integer, intent(in)                 :: nocc, lupri, luerr

    integer                     :: norb, nbas, ncell, ndim, nvirt
    integer                     :: i, p, indx, l1, lref
    type(lattice_domains_type)  :: dom
    type(lmatrixp)              :: fockMO,occMO,SMO
    type(matrix)                :: fullfockMO,fullU,fullSMO,subU,orthpot
    type(matrix)                :: MOocc_new,MO_old
    real(realk), pointer        :: eival(:)
    real(realk)                 :: lambda

    norb = mos%ncol
    nvirt = norb - nocc 
    nbas = mos%nrow
    !Nb of cells
    ncell =1
    do i = 1, fockmat%periodicdims
       ncell = ncell * (1 + 2*mos%cutoffs(i))
    enddo
    ! Size of the full Fock matrix
    ndim = ncell*nvirt + nocc

    !Set up the domain d0
    call ltdom_init(dom, mos%periodicdims, maxval(mos%cutoffs))
    call ltdom_getdomain(dom, mos%cutoffs, incl_redundant=.true.)

    !Transform F_mu,nu^L to F_p,q^L with L in 2*ao cutoff
    !We need twice the AO cutoff in order to build the matrice in full storage
    call lts_init(fockMO, wconf%blat%dims, 2*mos%cutoffs, norb, norb, &
         & .true., .false.)
    call w_calc_AO2MO_shukla(fockMO,fockmat,mos,wconf,lupri,luerr)

    !Tranform to full storage
    call mat_init(fullfockMO,ndim,ndim)
    call mat_zero(fullfockMO)
    call lts_tofull_nolambda(fockMO,fullfockMO,mos%cutoffs,ncell,nvirt,norb,luerr)

    !Transform S_mu,nu^L to S_p,q^L with L in 2*ao cutoff
    !We need twice the AO cutoff in order to build the matrice in full storage
    !call lts_init(SMO, wconf%blat%dims, 2*mos%cutoffs, norb, norb, &
    !     & .true., .false.)
    !call w_calc_AO2MO_shukla(SMO,smat,mos,wconf,lupri,luerr)

    !Tranform to full storage
    call mat_init(fullSMO,ndim,ndim)
    call mat_identity(fullSMO)
    !call lts_tofull_nolambda(SMO,fullSMO,mos%cutoffs,ncell,nvirt,norb,luerr)
           
    ! Diagonalize the modified Fock matrix
    call mat_init(fullU,ndim,ndim)
    call mem_alloc(eival,ndim) 
    call mat_diag_f(fullFockMO,fullSMO,eival,fullU)
    !call wannier_dggev(FullfockMO,fullSMO,eival,fullU,lupri,luerr)
    
    ! print eigenvalues
    write (lupri,*) 'Eigenvalues from MO diagonalization:'
    do p=1,nocc
       write (lupri,'(A8,I4,X,A,X,F16.8)') 'Epsilon',p,'=',eival(p)
    enddo
    call mem_dealloc(eival) ! no longer needed

    !if (wconf%debug_level >= 0) then
    !   write(luerr,*) 'Test unitarity of the full U matrix'
    !   call wtest_unitary_mat(fullU,lupri,luerr)
    !endif
    
    !Update occupied MOs
    call mat_init(MO_old,ncell*norb,ncell*norb)
    call lts_tofull(mos,MO_old,mos%cutoffs)

    call mat_init(MOocc_new,ncell*norb,nocc)

    call mat_init(subU,ncell*norb,nocc)
    call extract_subU(subU,fullU,nocc,nvirt,ncell,lupri,luerr)

    if (wconf%debug_level >= 0) then
       write(luerr,*) 'Test unitarity of the sub U matrix'
       call wtest_unitary_mat(subU,lupri,luerr)
    endif
    
    call mat_mul(MO_old,subU,'N','N',1.0e0_realk,0.0e0_realk,MOocc_new)
   
    call lts_init(occMO, wconf%blat%dims, mos%cutoffs, nbas, nocc, &
         & .true., .false.)
    call lts_zero(occMO)
    call lts_fromfull_mos(MOocc_new,occMO,nocc,nbas)

    !Lowdin Orthogonalisation of the occupied MO coefficients
    call wannier_orthogonalisation(occMO,wconf,smat,nbas,lupri,luerr)
    if (wconf%debug_level >= 0) then
       call wtest_orthonormality_wspace(occMO,smat,nbas,wconf,lupri,luerr,1e-4_realk)
    endif
       
    !Update density matrix
    call w_calc_dens(occMO, wconf%blat, wconf%no_redundant, dens, nocc)
    if (wconf%debug_level >= 0) then
       call wtest_density(dens, smat, occMO, wconf, nocc, lupri, luerr)
    endif
       
    if (norb == nocc) then
       call lts_swap_pointers(mos, occMO)
    else
       !Build the new set of virtual orbitals as the orthonormal complement
       !of the occupied orbitals
       call projectout_occspace(occMO,mos,dens,smat,wconf,lupri,luerr)
    endif

    if (wconf%debug_level >= 1) then
       write (luerr,*) ''
       write (luerr,*) 'Updated MOs'
       call lts_print(mos,luerr)
    endif
       
    !Deallocation
    call mat_free(subU)
    call mat_free(MO_old)
    call mat_free(MOocc_new)
    call lts_free(occMO)
    !call lts_free(SMO)
    call mat_free(fullSMO)
    call mat_free(fullfockMO)
    call mat_free(fullU)
    call lts_free(fockMO)
    
    !Free domain d0
    call ltdom_free(dom)
    
  end subroutine scf_shuklaMO

!!$  subroutine w_calc_MOoverlap(SMO,smat,mos,wconf,lupri,luerr)
!!$
!!$    type(lmatrixp), intent(out)      :: SMO
!!$    type(lmatrixp), intent(in)       :: smat,mos
!!$    type(wannier_config), intent(in) :: wconf
!!$    integer, intent(in)              :: lupri,luerr
!!$
!!$    type(lattice_domains_type) :: dom
!!$    
!!$    !Initailization of the domain d0
!!$    call ltdom_init(dom, wconf%blat%dims, 2*maxval(mos%cutoffs))
!!$    call ltdom_getdomain(dom, 2*mos%cutoffs, incl_redundant=.true.)
!!$
!!$    call lts_zero(SMO)
!!$    !Transform F_mu,nu^L to F_p,q^L with L in ao cutoff
!!$    loopl: do l=1, dom%nelms
!!$       indx_l = dom%indcs(l)
!!$       call lts_mat_init(fockMO, indx_l)
!!$       FMO_0L_ptr => lts_get(fockMO, indx_l)
!!$       call w_calc_fock_mo_l(FMO_0L_ptr, indx_l, dom, fockmat, mos, nocc, wconf, lupri, luerr)
!!$    enddo loopl
!!$    
!!$    call ltdom_free(dom)
!!$    
!!$  end subroutine w_calc_MOoverlap

  
  !> @brief Build the Fock matrix in full storage with lambda to infinity
  !> The colums and row for the occupied outside the ref cell are removed
  !>
  !> @author Elisa Rebolini
  !> @date November 2016
  !> @param[in] ltype Fock matrix in lattice storage
  !> @param[out] fullmat Fock matrix in full storage
  !> @param[in] cutoffs MO cutoffs
  !> @param[in] ncell Total number of cell within the MO cutoffs
  !> @param[in] nvirt Number of virtual orbitals per cell
  !> @param[in] norb Number of orbitals per cell
  !> @param[in] luerr Default print-unit for termination
  subroutine lts_tofull_nolambda(ltype, fullmat, cutoffs, ncell, nvirt, norb, luerr)

    type(lmatrixp), intent(in) :: ltype
    type(matrix), intent(out)   :: fullmat
    integer, intent(in)           :: cutoffs(3), ncell, nvirt, norb, luerr

    integer :: ndim, nocc
    integer :: m, n, m0, n0, l1, l2, indx1, indx2, r1, c1
    type(lattice_domains_type) :: dom
    type(matrix), pointer :: ptr
    type(matrix) :: mattmp, matsec
    logical :: lstat

    ndim = fullmat%nrow
    nocc = norb - nvirt

    !if no virtual orbitals, the sull Fock matrix is just the Fock matrix in the ref cell
    if (nvirt == 0) then
       call lts_copy(ltype,0,lstat,fullmat)
    else
       call ltdom_init(dom, ltype%periodicdims, maxval(cutoffs))
       call ltdom_getdomain(dom, cutoffs, incl_redundant = .true.)
       
       call mat_init(mattmp,norb,norb)
       
       ! loop over all blocks in the matrix.
       r1=0
       loop1: do l1 = 1, dom%nelms; indx1 = dom%indcs(l1)
          c1 = 0
          loop2: do l2 = 1, dom%nelms; indx2 = dom%indcs(l2)
             
             if (indx1 == 0) then
                m0 = 1
                m = norb
             else
                m0 = nocc +1
                m = nvirt
             endif
             if (indx2 == 0) then
                n0 = 1
                n = norb
             else
                n0 = nocc + 1
                n = nvirt
             endif
             
             call mat_init(matsec,m,n)
             
             ! For symmetric matrices elements are only stored if the cell 
             ! index is positive.
             ! For a negative cell index the block is found by taking the 
             ! transpose of the elements corresponding to the positive index
             if ((.not. ltype%redundant) .and. (indx2 < indx1)) then
                ptr => lts_get(ltype, indx1-indx2)
                if (associated(ptr)) then
                   call mat_trans(ptr, mattmp)
                   call mat_section(mattmp,m0,norb,n0,norb,matsec)
                else
                   call mat_zero(matsec)
                endif
                ! For non symmetric matrices we just copy the correct block 
             else
                ptr => lts_get(ltype, indx2-indx1)
                if (associated(ptr)) then
                   call mat_section(ptr,m0,norb,n0,norb,matsec)
                   call mat_insert_section(matsec, r1+1, r1+m, c1+1, c1+n, fullmat)
                else
                   call mat_zero(matsec)
                endif
             endif
             call mat_insert_section(matsec, r1+1, r1+m, c1+1, c1+n, fullmat)
             
             call mat_free(matsec)
             c1 = c1 + n
             if (c1 >= ndim) cycle 
          enddo loop2
          r1 = r1 + m
          if (r1 >= ndim) cycle
       enddo loop1
       
       !if (.not. ltype%redundant) then 
       call mat_free(mattmp)
       !endif
       call ltdom_free(dom)
       
    endif

  end subroutine lts_tofull_nolambda

  !> @brief Extract the part of the unitary matrix U to transform the occupied MO coeff
  !> Fill the rows for the occupied outside the ref cell with zeros
  !> @author Elisa Rebolini
  !> @date Nov. 2016
  !>
  !> @param[in] fullU full unitary matrix U in full storage
  !> @param[out] subU subsection of U to transform the occupied MOs in the ref cell
  !> @param[in] nocc  Number of occupied orbitals per cell
  !> @param[in] nvirt Number of virtual orbitals per cell
  !> @param[in] ncell Total number os cells within the MO cutoffs
  !> @param lupri Default print-unit for output
  !> @param luerr Default print-unit for termination
  subroutine extract_subU(subU,fullU,nocc,nvirt,ncell,lupri,luerr)
    type(matrix), intent(out) :: subU
    type(matrix), intent(in)  :: fullU
    integer, intent(in)       :: nocc, nvirt, ncell, lupri, luerr

    integer      :: norb
    integer      :: fullr, subr
    integer      :: l, l0
    type(matrix) :: tmp

    if (nvirt == 0) then
       subU%elms(:) = fullU%elms(:)
    else
    
       norb = nocc + nvirt
       l0 = (ncell - 1)/2 + 1
       call mat_zero(subU)
       
       fullr = 0
       subr = 0
       do l = 1, ncell
          if (l==l0) then
             call mat_init(tmp, norb, nocc)
             call mat_section(fullU,fullr+1,fullr+norb,1,nocc,tmp)
             call mat_insert_section(tmp,subr+1,subr+norb,1,nocc,subU)
             call mat_free(tmp)
             fullr = fullr + norb
             subr = subr + norb
          else
             call mat_init(tmp, nvirt, nocc)
             call mat_section(fullU,fullr+1,fullr+nvirt,1,nocc,tmp)
             call mat_insert_section(tmp,subr+nocc+1,subr+norb,1,nocc,subU)
             call mat_free(tmp)
             fullr = fullr + nvirt
             subr = subr + norb
          endif
       enddo
    endif
    
  end subroutine extract_subU
  
  
  !> @brief Build the new set of virtual orbitals as the orthonormal complement
  !> @author Elisa Rebolini
  !> @date September 2016
  !>
  !> @param occMO Occupied molecular orbital coefficients nbas*nocc lmatrixp
  !> @param mos Full MO coeffs to be updated
  !> @param dens Density matrix nbas*nbas lmatrixp built from the updated occMO
  !> @param smat Overlap matrix nbas*nbas lmatrixp
  !> @param wconf Contains the Wannier configuration
  !> @param lupri Default print-unit for output
  !> @param luerr Default print-unit for termination
  subroutine projectout_occspace(occMO,mos,dens,smat,wconf,lupri,luerr)
    type(lmatrixp), intent(inout)       :: occMO, smat
    type(lmatrixp), intent(inout)       :: mos,dens
    type(wannier_config), intent(inout) :: wconf
    integer,intent(in)                  :: lupri,luerr
    
    type(lattice_domains_type) :: dom
    type(lmatrixp)             :: projMO
    type(lmatrixp)             :: virtMO 
    integer                    :: nbas, norb, nocc, nvirt
    integer                    :: l, indx_l
    type(matrix),pointer       :: occ_pt, virt_pt, full_pt
    
    nbas = occMO%nrow
    norb = mos%ncol
    nocc = occMO%ncol
    nvirt = norb - nocc

    !Set up the domain d0
    call ltdom_init(dom, occMO%periodicdims, maxval(occMO%cutoffs))
    call ltdom_getdomain(dom, occMO%cutoffs, incl_redundant=.true.)

    !Project out the occupied space
    !(1 - D S) |0 mu>
    !Get the (linearly dependant) coefficients of the basis vectors
    !|0 mu>' = sum_{L1 nu} Q^{-L1}_{nu mu} |L1 mu>
    call lts_init(projMO, wconf%blat%dims, mos%cutoffs, mos%nrow, mos%nrow, .true.)  
    call lts_zero(projMO)
    
    call w_project_occspace(projMO,occMO,smat,wconf,lupri,luerr)
    if (wconf%debug_level >=0 ) call wtest_virtspace(projMO,occMO,smat,wconf,lupri,luerr)
      
    !Extract linearly independant basis for the virtual space
    call lts_init(virtMO, wconf%blat%dims, mos%cutoffs, nbas, nvirt, &
         & .true., .false.)
    call lts_zero(virtMO)
    call remove_lindep(projMO,virtMO,smat,lupri,luerr)
    if (wconf%debug_level >= 0) call wtest_virtspace(virtMO,occMO,smat,wconf,lupri,luerr)
    
    !Lowdin orthogonalisation of the new set of virtual orbitals
    call wannier_orthogonalisation(virtMO,wconf,smat,nbas,lupri,luerr)

    if (wconf%debug_level >= 0) call wtest_virtspace(virtMO,occMO,smat,wconf,lupri,luerr)
    
    !Recompose the full MOs in the ref cell
    loop_l: do l=1,dom%nelms
       indx_l = dom%indcs(l)
       occ_pt => lts_get(occMO, indx_l)
       virt_pt => lts_get(virtMO, indx_l)
       full_pt => lts_get(mos,indx_l)
       call mat_insert_section(occ_pt,1,nbas,1,nocc,full_pt)
       call mat_insert_section(virt_pt,1,nbas,nocc+1,norb,full_pt)
    enddo loop_l

    call lts_free(virtMO)  
    call lts_free(projMO)
    
    !Free domain d0
    call ltdom_free(dom)

  end subroutine projectout_occspace

  !> @brief build a linearly-dependent basis for the virtuals by projecting out the occ
  !> |0 mu>' = sum_{L1 nu} Q^{-L1}_{nu mu} |L1 mu>
  !> @author Elisa Rebolini
  !> @date Sept 2016
  !>
  !> @param projMO
  !> @param occMO
  !> @param smat
  !> @param wconf
  !> @param lupri
  !> @param luerr
  subroutine w_project_occspace(projMO,occMO,smat,wconf,lupri,luerr)

    type(lmatrixp), intent(inout)       :: projMO,occMO,smat
    type(wannier_config), intent(inout) :: wconf
    integer                             :: lupri,luerr

    type(lattice_domains_type) :: dom, dom_d
    type(lmatrixp)             :: overlap
    integer                    :: m, indx_m, l2, indx_l2, l, indx_l
    integer                    :: nbas, nocc
    type(matrix),pointer       :: q_ptm,c_ml,c_l2l,S_L
    type(matrix)               :: S_L2,tmp
    logical                    :: lstat_L2, lstat_L1L, lstat_L2L
    
    nbas = projMO%nrow
    nocc = occMO%ncol
    
    !Set up the domain d0
    call ltdom_init(dom, projMO%periodicdims, maxval(projMO%cutoffs))
    call ltdom_getdomain(dom, projMO%cutoffs, incl_redundant=.true.)

    call ltdom_init(dom_d, projMO%periodicdims, 2*maxval(projMO%cutoffs))
    call ltdom_getdomain(dom_d, 2*projMO%cutoffs, incl_redundant=.true.)

    call lts_init(overlap, wconf%blat%dims, 2*projMO%cutoffs, nbas, nocc, .true.)
    call mat_init(s_l2,nbas,nbas)
    call mat_init(tmp,nbas,nbas)

    loopl: do l = 1, dom_d%nelms
       indx_l = dom_d%indcs(l)
       call lts_mat_init(overlap,indx_l)
       S_L => lts_get(overlap,indx_l)
       call mat_zero(S_L)
       loopl2: do l2 = 1,dom%nelms
          indx_l2 = dom%indcs(l2)
          call lts_copy(smat,indx_l+indx_l2,lstat_l2,S_L2)
          if (lstat_l2) then
             c_l2l => lts_get(occMO,-indx_l2)
             if (associated(c_l2l)) then
                call mat_mul(s_l2,c_l2l,'n','n',1.0_realk,1.0_realk,S_L)
             endif
          endif
       enddo loopl2
    enddo loopl

    do m=1,dom%nelms
       indx_m=dom%indcs(m)
       q_ptm=>lts_get(projMO,-indx_m)
       if (.not.(associated(q_ptm))) then
          call lts_mat_init(projMO,-indx_m)
          q_ptm=>lts_get(projMO,-indx_m)
       endif
       if (indx_m == 0) then
          call mat_identity(q_ptm)
       else
          call mat_zero(q_ptm)
       endif
       do l=1,dom_d%nelms
          indx_l=dom_d%indcs(l)
          c_ml=>lts_get(occMO,indx_l-indx_m)
          if (associated(c_ml)) then
             s_l=>lts_get(overlap,indx_l)
             call mat_mul(c_ml,s_l,'n','t',-1.0_realk,1.0_realk,q_ptm)
          endif
       enddo
    enddo

    call mat_free(tmp)
    call mat_free(s_l2)    
    call lts_free(overlap)
    
    call ltdom_free(dom)
    call ltdom_free(dom_d)

  end subroutine w_project_occspace
  

  !> @brief Remove linear dependency in the virtual orbitals
  !> @author Elisa Rebolini
  !> @date Sept. 2016
  !>
  !> @param[in] projMO Linearly dependent basis set for the virtual space in lattice storage
  !> @param[inout] virtMO Linearly independent basis set
  !> @param[in] smat Overlap matrix of the AO basis functions in lattice storage
  !> @param[in] lupri Default print-unit for output
  !> @param[in] luerr Default print-unit for termination
  subroutine remove_lindep(projMO,virtMO,smat,lupri,luerr)

    type(lmatrixp),intent(in)    :: projMO,smat
    type(lmatrixp),intent(inout) :: virtMO
    integer,intent(in)           :: lupri,luerr

    type(lattice_domains_type) :: dom
    type(matrix),pointer       :: q_pt1, q_pt2, virt_pt
    integer                    :: l1, indx_l1, l2, indx_l2, k, idamax
    character(len = 1)         :: c1
    integer                    :: norb,nvirt,nbas,ncell,ndim,nocc
    integer                    :: i,j,ij,info
    type(matrix)               :: Sproj, S_L1L2, matU,sigma, tmp
    real(realk),pointer        :: s2dim(:,:),vl(:,:),vr(:,:)
    real(realk),pointer        :: wr(:),wi(:),work(:)
    logical                    :: lstat_L1L2
    
    nbas = projMO%nrow
    norb = projMO%ncol
    nvirt = virtMO%ncol
    nocc = norb-nvirt
    !Nb of cells
    ncell =1
    do i = 1, projMO%periodicdims
       ncell = ncell * (1 + 2*projMO%cutoffs(i))
    enddo
    ! Size of the full matrix
    ndim = ncell*norb

    !Set up the domain d0
    call ltdom_init(dom, projMO%periodicdims, maxval(projMO%cutoffs))
    call ltdom_getdomain(dom, projMO%cutoffs, incl_redundant=.true.)

    !Compute the overlap of the projected basis in the ref cell
    call mat_init(Sproj,nbas,nbas)
    call mat_zero(Sproj)

    call mat_init(tmp,nbas,nbas)
    call mat_init(S_L1L2,nbas,nbas)
    loopl1: do l1=1, dom%nelms
       indx_l1 = dom%indcs(l1)
       q_pt1 => lts_get(projMO,-indx_l1)
       if (associated(q_pt1)) then
          loopl2: do l2 = 1,dom%nelms
             indx_l2 = dom%indcs(l2)
             q_pt2 => lts_get(projMO,-indx_l2)
             if (associated(q_pt2)) then
                call lts_copy(smat,indx_l1-indx_l2,lstat_L1L2,S_L1L2)
                if (lstat_l1l2) then
                   call mat_mul(q_pt2,S_L1L2,'T','N',1.0_realk,0.0_realk,tmp)
                   call mat_mul(tmp,q_pt1,'N','N',1.0_realk,1.0_realk,Sproj)
                endif
             endif
          enddo loopl2
       endif
    enddo loopl1
    call mat_free(S_L1L2)
    call mat_free(tmp)
        
    !Transform to 2D array
    call mem_alloc(s2dim,nbas,nbas)
    s2dim(:,:) = 0.0_realk
    do i = 1, nbas
       do j = 1, nbas
          ij = (j-1)*nbas + i
          s2dim(i,j) = Sproj%elms(ij)
       enddo
    enddo
 
    !Diagomalisation
    call mem_alloc(wr,nbas)
    call mem_alloc(wi,nbas)
    call mem_alloc(vl,nbas,nbas)
    call mem_alloc(vr,nbas,nbas)
    call mem_alloc(work,4*nbas)
    vr(:,:) = 0.0_realk
    call dgeev('N','V', nbas, s2dim, nbas, wr, wi, vl, 1, vr, &
         nbas, work, 4*nbas, info)
    if (info /= 0) then
       write (*,*) 'DGEEV failed in remove_lindep with info =',info
       call lsquit('DGEEV in remove_lindep',-1)
    endif
    
    call sort_virt(vr,wr,nbas,lupri,luerr)

    !Extract the virtual space eigenvectors and eigenvalues
    !a.k.a. corresponding to the nvirt biggest eigenvalues
    call mat_init(matU,nbas,nvirt)
    call mat_zero(matU)
    do j = 1, nvirt
       do i = 1,nbas
          ij = (j-1)*nbas + i
          matU%elms(ij) = vr(i,j)/sqrt(wr(j))
       enddo
    enddo

    !Construct the virtual space basis
    loopl: do l1 = 1, dom%nelms
       indx_l1 = dom%indcs(l1)
       q_pt1 => lts_get(projMO,indx_l1)
       call lts_mat_init(virtMO,indx_l1)
       virt_pt => lts_get(virtMO,indx_l1)
       call mat_mul(q_pt1,matU,'N','N',1.0_realk,0.0_realk,virt_pt)
    enddo loopl

    call mat_free(matU)
    call mem_dealloc(wr)
    call mem_dealloc(wi)
    call mem_dealloc(vl)
    call mem_dealloc(vr)
    call mem_dealloc(work)
    call mem_dealloc(s2dim)
    call mat_free(Sproj)

    call ltdom_free(dom)
            
  end subroutine remove_lindep


  !> @brief Shukla's algorithm for the SCF iteration 
  !>
  !> The Fock matrix is kept in AO space
  !> the orthogonalising potential is added only to the OCCUPIED orbitals
  !> The virtual orbitals are never computed
  !>
  !> @author Elisa Rebolini
  !> @date September 2016
  !> @param fockmat Fock matrix AO basis in lattice storage
  !> @param smat Overlap matrix AO basis in latice storage
  !> @prarm orthmat Orthogonalising potential AO basis in lattice storage
  !> @param mos Input/output MO coefficients in lattice storage
  !> @param wconf  Contains the Wannier configuration
  !> @param nocc Number of occupied orbitals
  !> @param lupri Default print-unit for output
  !> @param luerr Default print-unit for termination
  subroutine scf_shuklaAO(fockmat, smat, orthmat, &
            &  mos, timings, wconf, lupri, luerr)

    type(llmatrixp), intent(inout)      :: Orthmat
    type(lmatrixp), intent(inout)       :: Fockmat, Smat
    type(lmatrixp), intent(inout)       :: mos
    type(timings_type), intent(inout)   :: timings
    type(wannier_config), intent(inout) :: wconf
    integer                             :: lupri,luerr

    type(lmatrixp)       :: tmpmos
    integer              :: ao_cutoffs(3)
    integer              :: nbas,ncell,ndim,i,info,j,ij
    type(matrix)         :: fullfockmat,fullSmat,fullOrthmat,fullmos
    real(realk), pointer :: eival(:)
    
    ! AO cutoffs as defined in input
    ao_cutoffs = wconf%ao_cutoff

    !Number of basis functions per cell
    nbas = mos%nrow
    
    ! First transform pot, S and F matrices to full matrices
    ncell =1
    do i = 1, fockmat%periodicdims
       ncell = ncell * (1 + 2*ao_cutoffs(i))
    enddo
    ! Size of the full Fock, overlap and orth. pot. matrices
    ndim = ncell*fockmat%nrow

    call mat_init(fullfockmat,ndim,ndim)
    call mat_zero(fullfockmat)

    call mat_init(fullSmat,ndim,ndim)
    call mat_zero(fullSmat)

    call mat_init(fullOrthmat,ndim,ndim)
    call mat_zero(fullOrthmat)
    
    ! The overlap matrix has smaller cutoffs, but still need to be
    ! expanded in the same space as the Fock matrix
    call timings_start(timings, 'SCFLOOP - P0, S, F to full')
    call lts_tofull(fockmat,fullfockmat,fockmat%cutoffs/2)
    call lts_tofull(Smat,fullSmat,fockmat%cutoffs/2)
    call lts_tofull(Orthmat,fullOrthmat,fockmat%cutoffs/2)
    call timings_stop(timings, 'SCFLOOP - P0, S, F to full')
    
    ! add the potential to the fock matrix
    call mat_daxpy(wconf%orthpot_strength, fullOrthmat, fullFockmat)
    call mat_free(fullOrthmat) ! not needed anymore

    ! Diagonalize the matrices
    call mat_init(fullmos,ndim,ndim)
    call mem_alloc(eival,ndim) 

    !call mat_diag_f(fullFockmat,fullSmat,eival,fullmos)
    !call mat_print(fullmos,1,ndim,1,ndim,6)
    call wannier_dggev(Fullfockmat,fullSmat,eival,fullmos,lupri,luerr)
    !call mat_print(fullmos,1,ndim,1,ndim,6)
    
    call timings_start(timings, 'SCFLOOP - Diag F')
    call mat_diag_f(Fullfockmat,fullSmat,eival,fullmos)
    call timings_stop(timings, 'SCFLOOP - Diag F')

    call wtest_fullsol(FullFockmat,fullSmat,fullmos,eival,ndim,lupri,luerr,1.0e-6_realk)
   
    write(lupri,*) ''
    write(lupri,*) 'Occupied Eigenvalues:'
    do i=1,wconf%nocc
       write (lupri,'(A,I2,X,A,X,F12.8)') '  Epsilon',i,'=',eival(i)
    enddo
    
    write(luerr,*) ''
    write(luerr,*) 'Occupied Eigenvalues:'
    do i=1,wconf%nocc
       write (luerr,'(A,I2,X,A,X,F12.8)') '  Epsilon',i,'=',eival(i)
    enddo
    
    ! Back transform the occupied MO coefficients to lattice-type matrix
    call lts_zero(mos)
    call timings_start(timings, 'SCFLOOP - Occ MO')
    call select_occmos(fullmos,mos,wconf%nocc,luerr)
    if (wconf%debug_level >= 3) then
       write(luerr,*) ''
       write(luerr,*) '*** Occupied Eigenvectors -- Before Lowdin orthogonalisation'
       call lts_print(mos,luerr,1)
    endif
    !call wannier_orthogonalisation(mos,wconf,smat,mos%nrow,lupri,luerr)
    call timings_start(timings, 'SCFLOOP - Occ MO')
    
    if (wconf%debug_level >= 1) then
       write(luerr,*) ''
       write(luerr,*) '*** Occupied Eigenvectors -- After Lowdin orthogonalisation'
       call lts_print(mos,luerr,1)
    endif
    
    call mem_dealloc(eival)
    call mat_free(fullmos)

    call mat_free(fullSmat)
    call mat_free(fullfockmat)
    
  end subroutine scf_shuklaAO


  !> @brief Selct the MO coefficients localized within the ref cell 
  !> @author Elisa Rebolini
  !> @date 11-2015
  !> Select the first N column of the fullCmo matrix and transform them
  !> to the lmatrixp tyoe
  !> @param fullCmo full MO coefficient matrix
  !> @param Cmo MO coefficient localized on the reference cell
  subroutine select_occmos(fullCmo,Cmo,nocc,luerr)
    
    type(matrix), intent(in)      :: fullCmo
    type(lmatrixp), intent(inout) :: Cmo
    integer,intent(in)            :: nocc,luerr
    
    type(matrix) :: subCmo
    type(matrix) :: tmp
    
    call mat_init(subCmo,fullCmo%nrow,nocc)
    call mat_zero(subCmo)
    
    call mat_section(fullCmo,1,fullCmo%nrow,1,nocc,subCmo)
    
    call lts_zero(Cmo)
    call lts_fromfull_mos(subCmo,Cmo,nocc,Cmo%nrow)
    
    call mat_free(subCmo)
  end subroutine select_occmos

end module wannier_shukla
