add_lsdalton_runtest(NAME 3pa LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME alpha LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(
  NAME
    f_dft
  LABELS
    "openrsp;ContinuousIntegration"
  COST
    120
  )
add_lsdalton_runtest(
  NAME
    ff_dft
  LABELS
    "openrsp;ContinuousIntegration"
  COST
    1800
  )
add_lsdalton_runtest(
  NAME
    ffff_diff_orig_ord
  LABELS
    "openrsp;ContinuousIntegration;nondftopenrsp"
  COST
    10
  )
add_lsdalton_runtest(
  NAME
    ffff_d_o_o_h2o2
  LABELS
    "openrsp;nondftopenrsp"
  COST
    150
  )
add_lsdalton_runtest(NAME ffff LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME fff LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME ff LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME f LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(
  NAME
    g_dft
  LABELS
    "openrsp;ContinuousIntegration"
  COST
    300
  )
add_lsdalton_runtest(
  NAME
    gf_dft
  LABELS
    "openrsp;ContinuousIntegration"
  COST
    1800
  )
add_lsdalton_runtest(
  NAME
    gff_dft
  LABELS
    "openrsp;ContinuousIntegration"
  COST
    1800
  )
# NEW
add_lsdalton_runtest(
  NAME
    gfff_h2o2
  LABELS
    "openrsp;nondftopenrsp"
  )

add_lsdalton_runtest(
  NAME
    ggff_h2o2
  LABELS
    "openrsp;nondftopenrsp"
  )
add_lsdalton_runtest(
  NAME
    ggff_static_h2o2
  LABELS
    "openrsp;nondftopenrsp"
  )
add_lsdalton_runtest(
  NAME
    gggf_h2o2
  LABELS
    "openrsp;nondftopenrsp"
  )
add_lsdalton_runtest(
  NAME
    ggg_h2o2
  LABELS
    "openrsp;nondftopenrsp"
  )

# END NEW
add_lsdalton_runtest(NAME gfff LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME gfff_multi_freq_cfg LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME gfff_static LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME gff LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME gff_static LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME gf LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(
  NAME
    gg_b3lyp
  LABELS
    "openrsp;ContinuousIntegration"
  COST
    1800
  )
add_lsdalton_runtest(
  NAME
    gg_blyp
  LABELS
    "openrsp;ContinuousIntegration"
  COST
    1800
  )
add_lsdalton_runtest(
  NAME
    ggff
  LABELS
    "openrsp;ContinuousIntegration;nondftopenrsp"
  COST
    120
  )
add_lsdalton_runtest(NAME ggff_static LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME ggf LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(
  NAME
    ggf_lda
  LABELS
    "openrsp;ContinuousIntegration"
  COST
    550
  )
add_lsdalton_runtest(
  NAME
    gggf
  LABELS
    "openrsp;ContinuousIntegration;nondftopenrsp"
  COST
    140
  )
add_lsdalton_runtest(NAME gggg LABELS "openrsp;nondftopenrsp" COST 180)
add_lsdalton_runtest(NAME ggg_lda LABELS "openrsp;ContinuousIntegration")
add_lsdalton_runtest(NAME ggg LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(
  NAME
    gg_lda
  LABELS
    "openrsp;ContinuousIntegration"
  COST
    450
  )
add_lsdalton_runtest(NAME gg LABELS "openrsp;ContinuousIntegration")
add_lsdalton_runtest(
  NAME
    gg_pbe0
  LABELS
    "openrsp;ContinuousIntegration"
  COST
    1800
  )
add_lsdalton_runtest(
  NAME
    gg_pbe
  LABELS
    "openrsp;ContinuousIntegration"
  COST
    1800
  )
add_lsdalton_runtest(NAME g LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME multi_prop_and_freq_cfg LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME multi_prop LABELS "openrsp;ContinuousIntegration;nondftopenrsp")
add_lsdalton_runtest(NAME tpa LABELS "openrsp;ContinuousIntegration;nondftopenrsp")

# Conditionally available tests
# TODO Uncomment or remove
#if(ENABLE_FRAME)
#   add_lsdalton_runtest(NAME FraME_f    LABELS "openrsp;frame;ContinuousIntegration")
#   add_lsdalton_runtest(NAME FraME_ff   LABELS "openrsp;frame;ContinuousIntegration")
#   add_lsdalton_runtest(NAME FraME_fff  LABELS "openrsp;frame;ContinuousIntegration")
#   add_lsdalton_runtest(NAME FraME_ffff LABELS "openrsp;frame;ContinuousIntegration")
#endif()
