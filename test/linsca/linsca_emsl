#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_emsl.info <<'%EOF%'
   linsca_emsl
   -------------
   Molecule:         Methane with -OH,and -Cl and -F
   Wave Function:    HF
   Test Purpose:     Check the EMSL basis set format
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_emsl.mol <<'%EOF%'
BASIS
cc-pVDZ_emsl Aux=cc-pVTZ_emsl
Methane with -OH,and -Cl and -F
b3lyp/6-31+g(d,p) optimized geo.
Atomtypes=5 Nosymmetry 
Charge=1. Atoms=2
H    0.520821    0.080551    1.451675
H    1.965224   -1.097271    0.053015
Charge=6.    Atoms=1
C    0.430423    0.005605    0.364687
Charge=8.    Atoms=1
O    1.023374   -1.101579   -0.177807
Charge=9.    Atoms=1
F    0.989971    1.157250   -0.160167
Charge=17.    Atoms=1
Cl   -1.303842   -0.036443   -0.048756
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_emsl.dal <<'%EOF%'
**GENERAL
.TESTMPICOPY
**INTEGRALS
.DENSFIT
**WAVE FUNCTIONS
.DFT
LDA
*DENSOPT
.RH
.DIIS
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_emsl.check
cat >> linsca_emsl.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi


CRIT1=`$GREP "Final DFT energy:   * \-667\.1377" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
