#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_mcd_calc.info <<'%EOF%'
   LSresponse_mcd_calc 
   -------------
   Molecule:         HF
   Wave Function:    DFT (B3LYP) / 6-31G*
   Test Purpose:     Test MCD: Aterm, Bterm and Damped MCD. 
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_mcd_calc.mol <<'%EOF%'
BASIS
6-31G*
H3+ molecule with
a 6-31G* basis
Atomtypes=1 Nosymmetry Angstrom
Charge=2.0  Atoms=3
H  0.000000     0.346410     0.000000
H  0.300000    -0.173205     0.000000
H -0.300000    -0.173205     0.000000
%EOF%

#######################################################################
#  DALTON INPUT
#  WARNING DO NOT USE THIS INPUT WITHOUT MODIFICATION
#######################################################################
cat > LSresponse_mcd_calc.dal <<'%EOF%'
**WAVE FUNCTIONS
.DFT
B3LYP
*DENSOPT
.ARH
.START
ATOMS
.CONVDYN
TIGHT
**RESPONSE
*QUASIMCD
.DEGENERATE
.MCDEXCIT
2
.DAMPEDRANGE
0.9797425912d0
0.1097425912d0
10
*END QUASIMCD
*SOLVER
.CONVDYN
TIGHT
.DTHR
1.0d-4
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSresponse_mcd_calc.check
cat >>LSresponse_mcd_calc.check <<'%EOF%'

log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY                                         
CRIT1=`$GREP "This is a DFT calculation of type: B3LYP" $log | wc -l`  
CRIT2=`$GREP "Final DFT energy\: *\-6\.87401729" $log | wc -l`       
TEST[1]=`expr   $CRIT1 + $CRIT2`                        
CTRL[1]=2                                                           
TEST[1]=`expr   $CRIT2`                        
CTRL[1]=1                                                           
ERROR[1]="DFT ENERGY NOT CORRECT -"  

# DAMPED 
CRIT2=`$GREP "9\.79742591[0-9][0-9]E\-01 * 6\.380[0-9][0-9][0-9][0-9][0-9][0-9][0-9]E\+00 * 4\.7474[0-9][0-9][0-9][0-9][0-9][0-9]E\+00" $log | wc -l` 
CRIT9=`$GREP "2\.15028520[0-9][0-9]E\+05 * 1\.25953[0-9][0-9][0-9][0-9][0-9]E\-02 * 9\.3719[0-9][0-9][0-9][0-9][0-9][0-9]E\-03" $log | wc -l` 
TEST[2]=`expr   $CRIT2 + $CRIT9`     
CTRL[2]=2
ERROR[2]="Damped MCD spectra not correct -"     

# A term
CRIT1=`$GREP "london A term \(a\.u\.\) * 0?\.3321" $log | wc -l` 
CRIT2=`$GREP "london A term \(std unit * 4?\.2912" $log | wc -l` 
CRIT3=`$GREP "nonlondon A term \(a\.u\.\) * 0?\.2801" $log | wc -l` 
CRIT4=`$GREP "nonlondon A term \(std unit * 3?\.6201" $log | wc -l` 
TEST[3]=`expr   $CRIT1 + $CRIT2 + $CRIT3 + $CRIT4`     
CTRL[3]=4   
ERROR[3]="Aterm NOT CORRECT -"     

# B term
CRIT1=`$GREP "LAO    BTERM\-result  \(a\.u\.\) * : * \-1\.0762" $log | wc -l` 
CRIT2=`$GREP "LAO    BTERM\-result  \(std units\) * : * \-0?\.0000" $log | wc -l` 
CRIT3=`$GREP "NONLAO BTERM\-result  \(a\.u\.\) * : * \-0?\.0931" $log | wc -l` 
TEST[4]=`expr   $CRIT1 + $CRIT2 + $CRIT3`     
CTRL[4]=12
ERROR[4]="Bterm NOT CORRECT -"     

# Excitation
CRIT1=`$GREP "Excitation Energies\(au\)\: * 0.922[7-8]" $log | wc -l` 
TEST[5]=`expr   $CRIT1`     
CTRL[5]=2 
ERROR[5]="Excitation Energies NOT CORRECT -"     

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[6]=`expr  $CRIT1`
CTRL[6]=1
ERROR[6]="Memory leak -"

PASSED=1
for i in 1 2 3 4 5 6
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
