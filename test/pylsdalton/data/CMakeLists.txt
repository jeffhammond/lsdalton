file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/${PYMOD_INSTALL_FULLDIR}/../data)
file(GLOB _fs
  LIST_DIRECTORIES false
  RELATIVE ${CMAKE_CURRENT_LIST_DIR}
  CONFIGURE_DEPENDS  # Re-glob every time. See also here for why it might fail: https://cmake.org/cmake/help/v3.14/command/file.html#filesystem
  "*.npy"
  )
foreach(_f IN LISTS _fs)
  file(
    CREATE_LINK
      ${CMAKE_CURRENT_LIST_DIR}/${_f}
      ${PROJECT_BINARY_DIR}/${PYMOD_INSTALL_FULLDIR}/../data/${_f}
    SYMBOLIC
    )
endforeach()

