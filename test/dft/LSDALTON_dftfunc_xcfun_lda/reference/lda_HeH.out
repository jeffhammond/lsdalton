     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton v2018.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,                    Norway   (Geometry optimizer)
    Ashleigh Barnes,        Oak Ridge National Laboratory,         USA      (ML-DEC)
    Radovan Bast,           UiT The Arctic University of Norway,   Norway   (CMake, Testing)
    Pablo Baudin,           Aarhus University,                     Denmark  (DEC,CCSD)
    Dmytro Bykov,           Oak Ridge National Laboratory,         USA      (ML-DEC)
    Sonia Coriani,          Technical University of Denmark,       Denmark  (Response)
    Roberto Di Remigio,     UiT The Arctic University of Norway,   Norway   (PCM)
    Karen Dundas,           UiT The Arctic University of Norway,   Norway   (OpenRSP)
    Patrick Ettenhuber,     Aarhus University,                     Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,                     Denmark  (CCSD(T), DEC)
    Luca Frediani,          UiT The Arctic University of Norway,   Norway   (PCM, Supervision)
    Daniel Henrik Friese,   Heinrich-Heine-Universitat Dusseldorf, Germany  (OpenRSP)
    Bin Gao,                UiT The Arctic University of Norway,   Norway   (OpenRSP, QcMatrix)
    Trygve Helgaker,        University of Oslo,                    Norway   (Supervision)
    Stinne Hoest,           Aarhus University,                     Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,                     Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,                    Norway   (ADMM)
    Branislav Jansik,       Aarhus University,                     Denmark  (Trilevel, orbital localization)
    Frank Jensen,           Aarhus University,                     Denmark  (ADMM basis sets)
    Poul Joergensen,        Aarhus University,                     Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,                     Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,                     Denmark  (RSP, INT, DEC, SCF, Input, MPI, 
                                                                             MAT)
    Andreas Krapp,          University of Oslo,                    Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,                     Denmark  (Response, DEC)
    Chandan Kumar,          University of Oslo,                    Norway   (Nuclei-selected NMR shielding)
    Patrick Merlot,         University of Oslo,                    Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,                     Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,                     Denmark  (Supervision)
    Elisa Rebolini,         University of Oslo,                    Norway   (MO-PARI-K)
    Simen Reine,            University of Oslo,                    Norway   (Integrals and approximations, 
                                                                             XC, MPI, OpenMP, geo.opt.)
    Magnus Ringholm,        UiT The Arctic University of Norway,   Norway   (OpenRSP)
    Kenneth Ruud,           UiT The Arctic University of Norway,   Norway   (OpenRSP, Supervision)
    Vladimir Rybkin,        University of Oslo,                    Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,                         Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham               England  (E-coefficients)
    Erik Tellgren,          University of Oslo,                    Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,                 Norway   (Response)
    Lea Thoegersen,         Aarhus University,                     Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,                    Norway   (FMM)
    Lukas Wirz,             University of Oslo,                    Norway   (NR-PARI)
    Marcin Ziolkowski,      Aarhus University,                     Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
 ================================================================
Version information
-------------------
Version       | v2018.0-85-gac2a206-dirty
Commit hash   | ac2a2060e79e3775b140fc9c5ef86c975c4347a7
Commit author | Roberto Di Remigio
Commit date   | Fri Jan 17 14:54:43 2020 +0100
Branch        | update_xcfun

Configuration and build information
-----------------------------------
Who compiled             | roberto
Compilation hostname     | minazo
Operating system         | Linux-4.19.95
Processor                | x86_64
CMake version            | 3.15.1
CMake generator          | Unix Makefiles
Build type               | DEBUG
Configuration time       | 2020-01-20 08:05:12
Fortran compiler         | /nix/store/3fnsihsjlxsg7jrc84rgsdj1q0flvfg9-gfortran-wrapper-8.3.0/bin/gfortran
Fortran compiler version | 8.3.0
C compiler               | /nix/store/5yyx688q9qxhb4ypawq7v80fm3ix27dm-gcc-wrapper-8.3.0/bin/gcc
C compiler version       | 8.3.0
C++ compiler             | /nix/store/5yyx688q9qxhb4ypawq7v80fm3ix27dm-gcc-wrapper-8.3.0/bin/g++
C++ compiler version     | 8.3.0
64-bit integers          | OFF
MPI parallelization      | OFF
OpenMP parallelization   | OFF

Further components
------------------

Runtime information
-------------------
Hostname:
    minazo
Current working dir:
    /tmp/DALTON_scratch_roberto/lda_HeH_9915
  
 The XCfun module is activated
    The Functional chosen is a LDA type functional
    The Functional chosen contains no exact exchange contribution

         Start simulation
     Date and time (Linux)  : Mon Jan 20 08:14:51 2020
     Host name              : minazo                                  
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    6-31G                                   
    HeH, CCSD( T)/cc-pVQZ opt. geometry     
    Test open shell response                
    Atomtypes=2 Charge=0 Nosymmetry Angstrom                                                                                
    Charge=1.0 Atoms=1                                                                                                      
    H     0.0000000000   0.0000000000   0.0000000000                                                                        
    Charge=2.0 Atoms=1                                                                                                      
    He    0.0000000000   0.0000000000   3.9218840000                                                                        
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **GENERAL
    .XCFUN
    **WAVE FUNCTIONS
    .DFT
     LDA
    *DFT INPUT
    .GRID TYPE
     BECKEORIG LMG
    .RADINT
    1.0D-11
    .ANGINT
    31
    *DENSOPT
    .RH
    .DIIS
    *END OF INPUT
 
WARNING:  No bonds - no atom pairs are within normal bonding distances
WARNING:  maybe coordinates were in Bohr, but program were told they were in Angstrom ?
 
                      
    Atoms and basis sets
      Total number of atoms        :      2
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 H      1.000 6-31G                      4        2 [4s|2s]                                      
          2 He     2.000 6-31G                      4        2 [4s|2s]                                      
    ---------------------------------------------------------------------
    total          3                                8        4
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :        4
      Primitive Regular basisfunctions   :        8
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)
    The Functional chosen is a LDA type functional
    The Functional chosen contains no exact exchange contribution
     
    The Exchange-Correlation Grid specifications:
    Radial Quadrature : LMG scheme
                        Radial grid as proposed by R. Lindh, P.A. Malmqvist
                        and L. Gagliardi. Theor. Chem. Acc. (2001) 106, 178
    Space partitioning: Becke partitioning scheme without atomic size correction
                        J. Chem. Phys. (1988) vol 88 page 2547

    We use grid pruning according to Mol. Phys. (1993) vol 78 page 997

     DFT LSint Radial integration threshold:   0.1000D-10
     DFT LSint integration order range     :   [  5:  31]
     Hardness of the partioning function   :   3
     DFT LSint screening thresholds        :   0.10D-08   0.20D-09   0.20D-11
     Threshold for number of electrons     :   0.10D-02
     The Exact Exchange Factor             : 0.0000D+00

    Density subspace min. method    : DIIS                    
    Density optimization            : Diagonalization                    

    Maximum size of Fock/density queue in averaging:    7

    WARNING WARNING WARNING spin check commented out!!! /Stinne


    --------------------------
    <Unrestricted calculation>
    --------------------------
    ALPHA spin occupancy =     1
    BETA  spin occupancy =     2

 
    Matrix type: mtype_unres_dense

    Convergence threshold for gradient        :   0.10E-03
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Since the input basis set is a segmented contracted basis we
    perform the integral evaluation in the more efficient
    standard input basis and then transform to the Grand 
    Canonical basis, which is general contracted.
    You can force the integral evaluation in Grand 
    Canonical basis by using the keyword
    .NOGCINTEGRALTRANSFORM
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in AO basis

    End of configuration!

 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 

    Level 1 atomic calculation on 6-31G Charge   1
    ================================================
  
    Total Number of grid points:        9158

    Max allocated memory, Grid                  622.387 kB

    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1     -0.4394566766    0.00000000000    0.00      0.00000    0.00    0.0000000    9.892E-02 ###
      2     -0.4414820651   -0.00202538857    0.00      0.00000    0.00    0.0000000    1.081E-02 ###
      3     -0.4415066937   -0.00002462855   -1.00      0.00000    0.00    0.0000000    5.397E-05 ###

    Level 1 atomic calculation on 6-31G Charge   2
    ================================================
  
    Total Number of grid points:        8788

    Max allocated memory, Grid                  599.262 kB

    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1     -2.6882299144    0.00000000000    0.00      0.00000    0.00    0.0000000    1.656E+00 ###
      2     -2.8096680262   -0.12143811178    0.00      0.00000    0.00    0.0000000    6.019E-01 ###
      3     -2.8266971697   -0.01702914347   -1.00      0.00000    0.00    0.0000000    3.490E-03 ###
      4     -2.8266977408   -0.00000057108   -1.00      0.00000    0.00    0.0000000    2.200E-05 ###
 
    Matrix type: mtype_unres_dense
 ndim =           4

    First density: Atoms in molecule guess

    Total Number of grid points:       17945

    Max allocated memory, Grid                  952.508 kB

    Iteration 0 energy:       -3.268215064230
 
    Diagonalize the Atoms in Molecule Fock Matrix to obtain Initial Density
 FALLBACK: open shell currently works only with cholesky decomposition
 - Ask Dr. Jansik to fix it for Lowdin!
    Preparing to do cholesky decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1     -3.3020715526    0.00000000000    0.00      0.00000    0.00    0.0000000    1.480E-01 ###
      2     -3.3027523029   -0.00068075038    0.00      0.00000    0.00    0.0000000    9.288E-03 ###
      3     -3.3027549791   -0.00000267617   -1.00      0.00000    0.00    0.0000000    3.219E-05 ###
    SCF converged in      3 iterations
    >>>  CPU Time used in SCF iterations is   0.36 seconds
    >>> wall Time used in SCF iterations is   0.36 seconds

    Total no. of matmuls in SCF optimization:         71

    Number of occupied alpha orbitals:       1
    Number of occupied beta orbitals:        2

    Number of virtual alpha orbitals:       3
    Number of virtual beta orbitals:        2

    Number of occupied alpha orbital energies to be found:       1
    Number of occupied beta orbital energies to be found:        1

    Number of virtual alpha orbital energies to be found:       1
    Number of virtual beta orbital energies to be found:        1


Calculation of alpha occupied orbital energies converged in     1 iterations!

Calculation of beta occupied orbital energies converged in     2 iterations!

Calculation of alpha virtual orbital energies converged in     3 iterations!

Calculation of beta virtual orbital energies converged in     2 iterations!

    E(LUMO), alpha:                         -0.072401 au
   -E(HOMO), alpha:                         -0.556404 au
   -------------------------------------
    "Alpha" HOMO-LUMO Gap (iteratively):     0.484003 au


    E(LUMO), beta:                           0.684901 au
   -E(HOMO), beta:                          -0.259516 au
   -------------------------------------
    "Beta" HOMO-LUMO Gap (iteratively):      0.944417 au


    E(LUMO):                                -0.072401 au
   -E(HOMO):                                -0.259516 au
   -------------------------------------
    "Overall" HOMO-LUMO Gap (iteratively):   0.187115 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.00068075038    0.0000    0.0000    0.0000000
      3   -0.00000267617   -1.0000    0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 AO Gradient norm
    ======================================================================
        1            -3.30207155255763185053      0.148029808678427D+00
        2            -3.30275230293585586594      0.928757944691859D-02
        3            -3.30275497910397541901      0.321858209699564D-04

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final DFT energy:                       -3.302754979104
          Nuclear repulsion:                       0.269858674198
          Electronic energy:                      -3.572613653302

    Total no. of matmuls used:                        79
    Total no. of Fock/KS matrix evaluations:           4
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                        864.442 kB
      Max allocated memory, type(matrix)                   6.096 kB
      Max allocated memory, real(realk)                  639.456 kB
      Max allocated memory, integer                      128.012 kB
      Max allocated memory, logical                        1.092 kB
      Max allocated memory, character                      2.448 kB
      Max allocated memory, AOBATCH                       14.560 kB
      Max allocated memory, ODBATCH                        1.584 kB
      Max allocated memory, LSAOTENSOR                     3.072 kB
      Max allocated memory, SLSAOTENSOR                    3.248 kB
      Max allocated memory, ATOMTYPEITEM                 201.056 kB
      Max allocated memory, ATOMITEM                       1.776 kB
      Max allocated memory, LSMATRIX                       3.312 kB
      Max allocated memory, OverlapT                      41.664 kB
      Max allocated memory, integrand                     64.512 kB
      Max allocated memory, integralitem                  46.080 kB
      Max allocated memory, IntWork                       10.392 kB
      Max allocated memory, Overlap                       74.664 kB
      Max allocated memory, ODitem                         1.152 kB
      Max allocated memory, LStensor                       2.224 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   0.99 seconds
    >>> wall Time used in LSDALTON is   0.99 seconds

    End simulation
     Date and time (Linux)  : Mon Jan 20 08:14:52 2020
     Host name              : minazo                                  
