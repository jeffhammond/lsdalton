#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_PropertyTest1.info <<'%EOF%'
   LSDALTON_PropertyTest1
   -------------
   Molecule:         SO2 and Water/(6-31G/cc-pVDZ)
   Wave Function:    B3LYP
   Test Purpose:     Check DFT energy
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_PropertyTest1.mol <<'%EOF%'
ATOMBASIS
LSint test,
============================
Atomtypes=4 Nosymmetry
Charge=8.0 Atoms=1  Bas=cc-pVDZ
O          -1.244255        0.015216        1.446161 
Charge=8.0 Atoms=2  Bas=6-31G
O           1.225543        0.015216        1.446161 
O          -0.009356        0.015216       -3.215161 
Charge=16.0 Atoms=1 Bas=6-31G
S          -0.009356        0.015216        0.723517 
Charge=1.0  Atoms=2 Bas=cc-pVDZ
H          -0.337034       -0.861284       -3.013896 
H           0.931113       -0.104860       -3.346471 
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_PropertyTest1.dal <<'%EOF%'
**GENERAL
.TESTMPICOPY
.NOGCBASIS
**INTEGRALS
.DEBUGPROP
**WAVE FUNCTIONS
.HF
*DENSOPT
.START
H1DIAG
.MAXIT
1
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_PropertyTest1.check
cat >> LSDALTON_PropertyTest1.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

CRIT1=`$GREP "SYMMETRY OF PROP ANGMOMX \= * 2" $log | wc -l`
CRIT2=`$GREP "SYMMETRY OF PROP ANGMOMY \= * 2" $log | wc -l`
CRIT3=`$GREP "SYMMETRY OF PROP ANGMOMZ \= * 2" $log | wc -l`
CRIT4=`$GREP "Norm of ANGMOMX\= *   2\.345372102" $log | wc -l`
CRIT5=`$GREP "Norm of ANGMOMY\= *   2\.493039246" $log | wc -l`
CRIT6=`$GREP "Norm of ANGMOMZ\= *   1\.238104102" $log | wc -l`
CRIT7=`$GREP "Maximum Element of ANGMOMX\= * 4\.8678110368853" $log | wc -l`
CRIT8=`$GREP "Maximum Element of ANGMOMY\= * 4\.8678110368853" $log | wc -l`
CRIT9=`$GREP "Maximum Element of ANGMOMZ\= * 2\.0000000000000" $log | wc -l`
TEST[1]=`expr   $CRIT1 \+ $CRIT4 \+ $CRIT7`
CTRL[1]=3
ERROR[1]="PROP ANGMOMX NOT CORRECT"
TEST[2]=`expr   $CRIT2 \+ $CRIT5 \+ $CRIT8`
CTRL[2]=3
ERROR[2]="PROP ANGMOMY NOT CORRECT"
TEST[3]=`expr   $CRIT3 \+ $CRIT6 \+ $CRIT9`
CTRL[3]=3
ERROR[3]="PROP ANGMOMZ NOT CORRECT"

CRIT1=`$GREP "SYMMETRY OF PROP ANGLONX \= * 3" $log | wc -l`
CRIT2=`$GREP "SYMMETRY OF PROP ANGLONY \= * 3" $log | wc -l`
CRIT3=`$GREP "SYMMETRY OF PROP ANGLONZ \= * 3" $log | wc -l`
CRIT4=`$GREP "Norm of ANGLONX\= * 1\.046901774" $log | wc -l`
CRIT5=`$GREP "Norm of ANGLONY\= * 1\.069380834" $log | wc -l`
CRIT6=`$GREP "Norm of ANGLONZ\= * 1\.043645133" $log | wc -l`
CRIT7=`$GREP "Maximum Element of ANGLONX\= * 1\.73205081" $log | wc -l`
CRIT8=`$GREP "Maximum Element of ANGLONY\= * 1\.73205081" $log | wc -l`
CRIT9=`$GREP "Maximum Element of ANGLONZ\= * 2\.00000000" $log | wc -l`
TEST[4]=`expr   $CRIT1 \+ $CRIT4 \+ $CRIT7`
CTRL[4]=3
ERROR[4]="PROP ANGLONX NOT CORRECT"
TEST[5]=`expr   $CRIT2 \+ $CRIT5 \+ $CRIT8`
CTRL[5]=3
ERROR[5]="PROP ANGLONY NOT CORRECT"
TEST[6]=`expr   $CRIT3 \+ $CRIT6 \+ $CRIT9`
CTRL[6]=3
ERROR[6]="PROP ANGLONZ NOT CORRECT"

CRIT1=`$GREP "SYMMETRY OF PROP 1ELPOT \= * 1" $log | wc -l`
CRIT2=`$GREP "PROPTEST6 1ELPOT\= * 0?\.00000000000000" $log | wc -l`
TEST[7]=`expr   $CRIT1 \+ $CRIT2 `
CTRL[7]=2
ERROR[7]="PROP 1ELPOT NOT CORRECT"

CRIT1=`$GREP "SYMMETRY OF PROP LONMOMX \= * 3" $log | wc -l`
CRIT2=`$GREP "SYMMETRY OF PROP LONMOMY \= * 3" $log | wc -l`
CRIT3=`$GREP "SYMMETRY OF PROP LONMOMZ \= * 3" $log | wc -l`
CRIT4=`$GREP "Norm of LONMOMX\= * 14\.108805955" $log | wc -l`
CRIT5=`$GREP "Norm of LONMOMY\= * 24\.581084247" $log | wc -l`
CRIT6=`$GREP "Norm of LONMOMZ\= * 9\.439550944" $log | wc -l`
CRIT7=`$GREP "Maximum Element of LONMOMX\= * 17\.80283543" $log | wc -l`
CRIT8=`$GREP "Maximum Element of LONMOMY\= * 22\.54439186" $log | wc -l`
CRIT9=`$GREP "Maximum Element of LONMOMZ\= * 11\.99155747" $log | wc -l`
TEST[8]=`expr   $CRIT1 \+ $CRIT4 \+ $CRIT7`
CTRL[8]=3
ERROR[8]="PROP LONMOMX NOT CORRECT"
TEST[9]=`expr   $CRIT2 \+ $CRIT5 \+ $CRIT8`
CTRL[9]=3
ERROR[9]="PROP LONMOMY NOT CORRECT"
TEST[10]=`expr   $CRIT3 \+ $CRIT6 \+ $CRIT9`
CTRL[10]=3
ERROR[10]="PROP LONMOMZ NOT CORRECT"

CRIT1=`$GREP "SYMMETRY OF PROP MAGMOMX \= * 2" $log | wc -l`
CRIT2=`$GREP "SYMMETRY OF PROP MAGMOMY \= * 2" $log | wc -l`
CRIT3=`$GREP "SYMMETRY OF PROP MAGMOMZ \= * 2" $log | wc -l`
CRIT4=`$GREP "Norm of MAGMOMX\= * 14\.149825141" $log | wc -l`
CRIT5=`$GREP "Norm of MAGMOMY\= * 24\.607629743" $log | wc -l`
CRIT6=`$GREP "Norm of MAGMOMZ\= * 9\.470525361" $log | wc -l`
CRIT7=`$GREP "Maximum Element of MAGMOMX\= * 17\.80283543" $log | wc -l`
CRIT8=`$GREP "Maximum Element of MAGMOMY\= * 22\.54439186" $log | wc -l`
CRIT9=`$GREP "Maximum Element of MAGMOMZ\= * 11\.77388519" $log | wc -l`
TEST[11]=`expr   $CRIT1 \+ $CRIT4 \+ $CRIT7`
CTRL[11]=3
ERROR[11]="PROP MAGMOMX NOT CORRECT"
TEST[12]=`expr   $CRIT2 \+ $CRIT5 \+ $CRIT8`
CTRL[12]=3
ERROR[12]="PROP MAGMOMY NOT CORRECT"
TEST[13]=`expr   $CRIT3 \+ $CRIT6 \+ $CRIT9`
CTRL[13]=3
ERROR[13]="PROP MAGMOMZ NOT CORRECT"

CRIT1=`$GREP "SYMMETRY OF PROP PSO 001  \= * 2" $log | wc -l`
CRIT2=`$GREP "SYMMETRY OF PROP PSO 002  \= * 2" $log | wc -l`
CRIT3=`$GREP "SYMMETRY OF PROP PSO 003  \= * 2" $log | wc -l`
CRIT4=`$GREP "SYMMETRY OF PROP PSO 004  \= * 2" $log | wc -l`
CRIT5=`$GREP "SYMMETRY OF PROP PSO 005  \= * 2" $log | wc -l`
CRIT6=`$GREP "SYMMETRY OF PROP PSO 006  \= * 2" $log | wc -l`
CRIT7=`$GREP "SYMMETRY OF PROP PSO 007  \= * 2" $log | wc -l`
CRIT8=`$GREP "SYMMETRY OF PROP PSO 011  \= * 2" $log | wc -l`
CRIT9=`$GREP "SYMMETRY OF PROP PSO 013  \= * 2" $log | wc -l`
CRIT10=`$GREP "SYMMETRY OF PROP PSO 015  \= * 2" $log | wc -l`
CRIT11=`$GREP "SYMMETRY OF PROP PSO 017  \= * 2" $log | wc -l`
CRIT12=`$GREP "SYMMETRY OF PROP PSO 018  \= * 2" $log | wc -l`
TEST[14]=`expr   $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 \+ $CRIT9 \+ $CRIT10 \+ $CRIT11 \+ $CRIT12`
CTRL[14]=12
ERROR[14]="PROP PSO NOT CORRECT SYMMETRY"

CRIT1=`$GREP "Norm of PSO 001 \= * 1\.079327287" $log | wc -l`
CRIT2=`$GREP "Norm of PSO 003 \= * 1\.117283042" $log | wc -l`
CRIT3=`$GREP "Norm of PSO 005 \= * 1\.662074303" $log | wc -l`
CRIT4=`$GREP "Norm of PSO 007 \= * 1\.692791167" $log | wc -l`
CRIT5=`$GREP "Norm of PSO 008 \= * 1\.686667996" $log | wc -l`
CRIT6=`$GREP "Norm of PSO 009 \= * 1\.667682303" $log | wc -l`
CRIT7=`$GREP "Norm of PSO 010 \= * 15\.356776565" $log | wc -l`
CRIT8=`$GREP "Norm of PSO 012 \= * 15\.368341057" $log | wc -l`
CRIT9=`$GREP "Norm of PSO 015 \= * 0?\.544771475" $log | wc -l`
CRIT10=`$GREP "Norm of PSO 017 \= * 0?\.542625493" $log | wc -l`
TEST[15]=`expr   $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 \+ $CRIT9 \+ $CRIT10`
CTRL[15]=10
ERROR[15]="PROP PSO NOT CORRECT"

CRIT1=`$GREP "SYMMETRY OF PROP 001 NSTX \= * 2" $log | wc -l`
CRIT2=`$GREP "SYMMETRY OF PROP 001 NSTY \= * 2" $log | wc -l`
CRIT3=`$GREP "SYMMETRY OF PROP 001 NSTZ \= * 2" $log | wc -l`
CRIT4=`$GREP "SYMMETRY OF PROP 002 NSTX \= * 2" $log | wc -l`
CRIT5=`$GREP "SYMMETRY OF PROP 005 NSTY \= * 2" $log | wc -l`
CRIT6=`$GREP "SYMMETRY OF PROP 007 NSTX \= * 2" $log | wc -l`
CRIT7=`$GREP "SYMMETRY OF PROP 007 NSTY \= * 2" $log | wc -l`
CRIT8=`$GREP "SYMMETRY OF PROP 007 NSTZ \= * 2" $log | wc -l`
TEST[16]=`expr   $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8`
CTRL[16]=8
ERROR[16]="PROP NST NOT CORRECT SYMMETRY"

CRIT1=`$GREP "Norm of 001 NSTX\= * 0?\.442331853" $log | wc -l`
CRIT2=`$GREP "Norm of 001 NSTZ\= * 0?\.190223476" $log | wc -l`
CRIT3=`$GREP "Norm of 002 NSTY\= * 0?\.527449711" $log | wc -l`
CRIT4=`$GREP "Norm of 003 NSTX\= * 0?\.116036150" $log | wc -l`
CRIT5=`$GREP "Norm of 003 NSTY\= * 0?\.279385041" $log | wc -l`
CRIT6=`$GREP "Norm of 003 NSTZ\= * 0?\.462768448" $log | wc -l`
CRIT7=`$GREP "Norm of 004 NSTX\= * 0?\.435639927" $log | wc -l`
CRIT8=`$GREP "Norm of 004 NSTZ\= * 0?\.211204347" $log | wc -l`
CRIT9=`$GREP "Norm of 005 NSTZ\= * 0?\.060999563?" $log | wc -l`
CRIT10=`$GREP "Norm of 006 NSTY\= * 0?\.322580435" $log | wc -l`
TEST[17]=`expr   $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 \+ $CRIT9 \+ $CRIT10`
CTRL[17]=10
ERROR[17]="PROP NST NOT CORRECT"

CRIT1=`$GREP "SYMMETRY OF PROP DIPVELX \= * 2" $log | wc -l`
CRIT2=`$GREP "SYMMETRY OF PROP DIPVELY \= * 2" $log | wc -l`
CRIT3=`$GREP "SYMMETRY OF PROP DIPVELZ \= * 2" $log | wc -l`
CRIT4=`$GREP "Norm of DIPVELX\= *   1\.180986142" $log | wc -l`
CRIT5=`$GREP "Norm of DIPVELY\= *   1\.139525210" $log | wc -l`
CRIT6=`$GREP "Norm of DIPVELZ\= *   1\.146684162" $log | wc -l`
CRIT7=`$GREP "Maximum Element of DIPVELX\= * 3\.20408983" $log | wc -l`
CRIT8=`$GREP "Maximum Element of DIPVELY\= * 3\.20408983" $log | wc -l`
CRIT9=`$GREP "Maximum Element of DIPVELZ\= * 3\.20408983" $log | wc -l`
TEST[18]=`expr   $CRIT1 \+ $CRIT4 \+ $CRIT7`
CTRL[18]=3
ERROR[18]="PROP DIPVELX NOT CORRECT"
TEST[19]=`expr   $CRIT2 \+ $CRIT5 \+ $CRIT8`
CTRL[19]=3
ERROR[19]="PROP DIPVELY NOT CORRECT"
TEST[20]=`expr   $CRIT3 \+ $CRIT6 \+ $CRIT9`
CTRL[20]=3
ERROR[20]="PROP DIPVELZ NOT CORRECT"

CRIT1=`$GREP "SYMMETRY OF PROP XXROTSTR \= * 2" $log | wc -l`
CRIT2=`$GREP "SYMMETRY OF PROP XYROTSTR \= * 2" $log | wc -l`
CRIT3=`$GREP "SYMMETRY OF PROP XZROTSTR \= * 2" $log | wc -l`
CRIT4=`$GREP "SYMMETRY OF PROP YYROTSTR \= * 2" $log | wc -l`
CRIT5=`$GREP "SYMMETRY OF PROP YZROTSTR \= * 2" $log | wc -l`
CRIT6=`$GREP "SYMMETRY OF PROP ZZROTSTR \= * 2" $log | wc -l`
TEST[21]=`expr   $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6`
CTRL[21]=6
ERROR[21]="PROP ROTSTR NOT CORRECT SYMMETRY"

CRIT1=`$GREP "Norm of XXROTSTR\= * 1\.742596174" $log | wc -l`
CRIT2=`$GREP "Norm of XYROTSTR\= * 0?\.894575428" $log | wc -l`
CRIT3=`$GREP "Norm of XZROTSTR\= * 2\.360023473" $log | wc -l`
CRIT4=`$GREP "Norm of YYROTSTR\= * 0?\.849209134" $log | wc -l`
CRIT5=`$GREP "Norm of YZROTSTR\= * 2\.149219052" $log | wc -l`
CRIT6=`$GREP "Norm of ZZROTSTR\= * 4\.172736325" $log | wc -l`
TEST[22]=`expr   $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6`
CTRL[22]=6
ERROR[22]="PROP ROTSTR NOT CORRECT"

CRIT1=`$GREP "SYMMETRY OF PROP S1MAGX \= * 2" $log | wc -l`
CRIT2=`$GREP "SYMMETRY OF PROP S1MAGY \= * 2" $log | wc -l`
CRIT3=`$GREP "SYMMETRY OF PROP S1MAGZ \= * 2" $log | wc -l`
CRIT4=`$GREP "Norm of S1MAGX\= * 0?\.953239765" $log | wc -l`
CRIT5=`$GREP "Norm of S1MAGY\= * 1\.406627822" $log | wc -l`
CRIT6=`$GREP "Norm of S1MAGZ\= * 0?\.558930575" $log | wc -l`
CRIT7=`$GREP "Maximum Element of S1MAGX\= * 1\.23217729" $log | wc -l`
CRIT8=`$GREP "Maximum Element of S1MAGY\= * 1\.71052176" $log | wc -l`
CRIT9=`$GREP "Maximum Element of S1MAGZ\= * 0?\.67033531" $log | wc -l`
TEST[23]=`expr   $CRIT1 \+ $CRIT4 \+ $CRIT7`
CTRL[23]=3
ERROR[23]="PROP S1MAGX NOT CORRECT"
TEST[24]=`expr   $CRIT2 \+ $CRIT5 \+ $CRIT8`
CTRL[24]=3
ERROR[24]="PROP S1MAGY NOT CORRECT"
TEST[25]=`expr   $CRIT3 \+ $CRIT6 \+ $CRIT9`
CTRL[25]=3
ERROR[25]="PROP S1MAGZ NOT CORRECT"

CRIT1=`$GREP "SYMMETRY OF PROP S1MAGRX \= * 3" $log | wc -l`
CRIT2=`$GREP "SYMMETRY OF PROP S1MAGRY \= * 3" $log | wc -l`
CRIT3=`$GREP "SYMMETRY OF PROP S1MAGRZ \= * 3" $log | wc -l`
CRIT4=`$GREP "Norm of S1MAGRX\= * 1\.191104272" $log | wc -l`
CRIT5=`$GREP "Norm of S1MAGRY\= * 1\.323998529" $log | wc -l`
CRIT6=`$GREP "Norm of S1MAGRZ\= * 0?\.457643298" $log | wc -l`
CRIT7=`$GREP "Maximum Element of S1MAGRX\= * 1\.86053578" $log | wc -l`
CRIT8=`$GREP "Maximum Element of S1MAGRY\= * 1\.87621087" $log | wc -l`
CRIT9=`$GREP "Maximum Element of S1MAGRZ\= * 0?\.61546917" $log | wc -l`
TEST[26]=`expr   $CRIT1 \+ $CRIT4 \+ $CRIT7`
CTRL[26]=3
ERROR[26]="PROP S1MAGRX NOT CORRECT"
TEST[27]=`expr   $CRIT2 \+ $CRIT5 \+ $CRIT8`
CTRL[27]=3
ERROR[27]="PROP S1MAGRY NOT CORRECT"
TEST[28]=`expr   $CRIT3 \+ $CRIT6 \+ $CRIT9`
CTRL[28]=3
ERROR[28]="PROP S1MAGRZ NOT CORRECT"

CRIT1=`$GREP "SYMMETRY OF PROP S1MAGLX \= * 3" $log | wc -l`
CRIT2=`$GREP "SYMMETRY OF PROP S1MAGLY \= * 3" $log | wc -l`
CRIT3=`$GREP "SYMMETRY OF PROP S1MAGLZ \= * 3" $log | wc -l`
CRIT4=`$GREP "Norm of S1MAGLX\= * 1\.191104272" $log | wc -l`
CRIT5=`$GREP "Norm of S1MAGLY\= * 1\.323998529" $log | wc -l`
CRIT6=`$GREP "Norm of S1MAGLZ\= * 0?\.457643298" $log | wc -l`
CRIT7=`$GREP "Maximum Element of S1MAGLX\= * 1\.86053578" $log | wc -l`
CRIT8=`$GREP "Maximum Element of S1MAGLY\= * 1\.87621087" $log | wc -l`
CRIT9=`$GREP "Maximum Element of S1MAGLZ\= * 0?\.61546917" $log | wc -l`
TEST[29]=`expr   $CRIT1 \+ $CRIT4 \+ $CRIT7`
CTRL[29]=3
ERROR[29]="PROP S1MAGLX NOT CORRECT"
TEST[30]=`expr   $CRIT2 \+ $CRIT5 \+ $CRIT8`
CTRL[30]=3
ERROR[30]="PROP S1MAGLY NOT CORRECT"
TEST[31]=`expr   $CRIT3 \+ $CRIT6 \+ $CRIT9`
CTRL[31]=3
ERROR[31]="PROP S1MAGLZ NOT CORRECT"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[32]=`expr  $CRIT1`
CTRL[32]=1
ERROR[32]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[33]=`expr  $CRIT1`
CTRL[33]=0
ERROR[33]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
