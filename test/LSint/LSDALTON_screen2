#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_screen2.info <<'%EOF%'
   LSDALTON_screen2
   -------------
   Molecule:         2 HCN molecules (cc-pVDZ,6-31G)
   Wave Function:    HF
   Test Purpose:     check screening 
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_screen2.mol <<'%EOF%'
ATOMBASIS
2 HCN molecules placed 10 atomic units apart
STRUCTURE IS NOT OPTIMIZED.
Atomtypes=6 Nosymmetry Angstrom
Charge=1. Atoms=1 Basis=cc-pVDZ
H   0.000   0.000  -1.06992
Charge=1. Atoms=1 Basis=6-31G
H   0.000  10.000  -1.06992
Charge=7. Atoms=1 Basis=cc-pVDZ
N   0.000   0.000   1.15301
Charge=7. Atoms=1 Basis=6-31G
N   0.000  10.000   1.15301
Charge=6. Atoms=1 Basis=cc-pVDZ
C   0.000   0.000   0.000
Charge=6. Atoms=1 Basis=6-31G
C   0.000  10.000   0.000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_screen2.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRALS
.DEBUGSCREEN
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.START
H1DIAG
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_screen2.check
cat >> LSDALTON_screen2.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY test
CRIT1=`$GREP "Final HF energy:    * \-185.710929" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="HF energy not correct"

# ENERGY test
CRIT1=`$GREP "di_screen_test SUCCESSFUL" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="screening not correct"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=0
ERROR[4]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
