     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | pablo
     Host                     | pablo-AU
     System                   | Linux-4.6.5-040605-generic
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/bin/gfortran
     Fortran compiler version | GNU Fortran (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     C compiler               | /usr/bin/gcc
     C compiler version       | gcc (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     C++ compiler             | /usr/bin/g++
     C++ compiler version     | g++ (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     BLAS                     | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                              | libmkl_gf_lp64.so;/opt/intel/composer_xe_2015.2.16
                              | 4/mkl/lib/intel64/libmkl_sequential.so;/opt/intel/
                              | composer_xe_2015.2.164/mkl/lib/intel64/libmkl_core
                              | .so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/l
                              | ib/x86_64-linux-gnu/libm.so
     LAPACK                   | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                              | libmkl_lapack95_lp64.a;/opt/intel/composer_xe_2015
                              | .2.164/mkl/lib/intel64/libmkl_gf_lp64.so
     Static linking           | OFF
     Last Git revision        | 98a37eb189034b6a19fd892c944ff6d68d788eed
     Git branch               | pablo/cpt
     Configuration time       | 2017-03-23 10:52:36.296585
  

         Start simulation
     Date and time (Linux)  : Thu Mar 23 10:58:42 2017
     Host name              : pablo-AU                                
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    cc-pVDZ Aux=cc-pVDZ-RI                  
    %Equilibrium water geometry used in MEST
                                            
    Atomtypes=2 Charge=0                                                                                                    
    Charge=8.0 Atoms=1                                                                                                      
    O        1.84345        0.00000        0.00000                                                                          
    Charge=1.0 Atoms=2                                                                                                      
    H        0.00000        0.00000        0.00000                                                                          
    H        2.49100        1.72597        0.00000                                                                          
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .ARH
    .START
    ATOMS
    .CONVTHR
    1.0d-8
    **GENERAL
    .NOGCBASIS
    **CC
    .RIMP3
    .CANONICAL
    .MEMORY
    2
    *END OF INPUT
 
 
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
                      
    Atoms and basis sets
      Total number of atoms        :      3
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 O      8.000 cc-pVDZ                   26       14 [9s4p1d|3s2p1d]                              
          2 H      1.000 cc-pVDZ                    7        5 [4s1p|2s1p]                                  
          3 H      1.000 cc-pVDZ                    7        5 [4s1p|2s1p]                                  
    ---------------------------------------------------------------------
    total         10                               40       24
    ---------------------------------------------------------------------
                      
                      
    Atoms and basis sets
      Total number of atoms        :      3
      THE  AUXILIARY is on R =   2
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 O      8.000 cc-pVDZ-RI                56       56 [7s5p4d2f|7s5p4d2f]                          
          2 H      1.000 cc-pVDZ-RI                14       14 [3s2p1d|3s2p1d]                              
          3 H      1.000 cc-pVDZ-RI                14       14 [3s2p1d|3s2p1d]                              
    ---------------------------------------------------------------------
    total         10                               84       84
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       24
      Auxiliary basisfunctions           :       84
      Primitive Regular basisfunctions   :       40
      Primitive Auxiliary basisfunctions :       84
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    You have requested Augmented Roothaan-Hall optimization
    => explicit averaging is turned off!

    Expand trust radius if ratio is larger than:            0.75
    Contract trust radius if ratio is smaller than:         0.25
    On expansion, trust radius is expanded by a factor      1.20
    On contraction, trust radius is contracted by a factor  0.70

    Maximum size of subspace in ARH linear equations:       2

    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          

    Maximum size of Fock/density queue in averaging:   10

    Convergence threshold for gradient        :   0.10E-07
    We perform the calculation in the standard input basis
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis

    End of configuration!


    First density: Atoms in molecule guess

    Iteration 0 energy:      -75.883424515208
 
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  1.00000000E-08
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1    -75.9900374222    0.00000000000    0.00      0.00000    0.00    2.895E-01 ###
      2    -76.0158956075   -0.02585818524    0.00      0.00000   -0.00    1.524E-01 ###
      3    -76.0237725965   -0.00787698905    0.00      0.00000   -0.00    2.202E-02 ###
      4    -76.0240309178   -0.00025832130    0.00      0.00000   -0.00    3.220E-03 ###
      5    -76.0240384558   -0.00000753799    0.00      0.00000   -0.00    5.058E-04 ###
      6    -76.0240386864   -0.00000023061    0.00      0.00000   -0.00    6.717E-05 ###
      7    -76.0240386897   -0.00000000334    0.00      0.00000   -0.00    5.390E-06 ###
      8    -76.0240386898   -0.00000000002    0.00      0.00000   -0.00    9.423E-07 ###
      9    -76.0240386898   -0.00000000000    0.00      0.00000   -0.00    2.338E-07 ###
     10    -76.0240386898   -0.00000000000    0.00      0.00000   -0.00    2.841E-08 ###
     11    -76.0240386898    0.00000000000    0.00      0.00000   -0.00    4.225E-09 ###
    SCF converged in     11 iterations
    >>>  CPU Time used in SCF iterations is   1.61 seconds
    >>> wall Time used in SCF iterations is   1.61 seconds

    Total no. of matmuls in SCF optimization:       1023

    Number of occupied orbitals:       5
    Number of virtual orbitals:       19

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     3 iterations!

    Calculation of virtual orbital energies converged in     9 iterations!

     E(LUMO):                         0.183200 au
    -E(HOMO):                        -0.550962 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.734162 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.02585818524    0.0000   -0.0000    0.0000000
      3   -0.00787698905    0.0000   -0.0000    0.0000000
      4   -0.00025832130    0.0000   -0.0000    0.0000000
      5   -0.00000753799    0.0000   -0.0000    0.0000000
      6   -0.00000023061    0.0000   -0.0000    0.0000000
      7   -0.00000000334    0.0000   -0.0000    0.0000000
      8   -0.00000000002    0.0000   -0.0000    0.0000000
      9   -0.00000000000    0.0000   -0.0000    0.0000000
     10   -0.00000000000    0.0000   -0.0000    0.0000000
     11    0.00000000000    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1           -75.99003742221904644794      0.289452711425909D+00
        2           -76.01589560745978246814      0.152384223524953D+00
        3           -76.02377259650953078562      0.220178690417647D-01
        4           -76.02403091780763588758      0.321983425905862D-02
        5           -76.02403845579533481214      0.505790006529727D-03
        6           -76.02403868640229234188      0.671709226922951D-04
        7           -76.02403868974491274457      0.539002366438103D-05
        8           -76.02403868976774958810      0.942274076367305D-06
        9           -76.02403868976901435417      0.233797853264215D-06
       10           -76.02403868976908540844      0.284135452836731D-07
       11           -76.02403868976908540844      0.422549364795667D-08

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                       -76.024038689769
          Nuclear repulsion:                       9.009363910090
          Electronic energy:                     -85.033402599859



    -- Full molecular info --

    FULL: Overall charge of molecule    :      0

    FULL: Number of electrons           :     10
    FULL: Number of atoms               :      3
    FULL: Number of basis func.         :     24
    FULL: Number of aux. basis func.    :     84
    FULL: Number of core orbitals       :      1
    FULL: Number of valence orbitals    :      4
    FULL: Number of occ. orbitals       :      5
    FULL: Number of occ. alpha orbitals : ******
    FULL: Number of occ. beta  orbitals :  32767
    FULL: Number of virt. orbitals      :     19
    FULL: Local memory use type full    :  0.11E-04
    FULL: Distribute matrices           : F
    FULL: Using frozen-core approx.     : F

 Allocate space for molecule%Co on Master use_bg= F
 Allocate space for molecule%Cv on Master use_bg= F
    Memory set in input to be:    2.000     GB


    =============================================================================
         -- Full molecular Coupled-Cluster calculation -- 
    =============================================================================

    Using canonical orbitals as requested in input!



 ================================================ 
              Full molecular driver               
 ================================================ 

 RIMP2 CORRELATION ENERGY =  -0.20467061609329990     
    >>>  CPU Time used in FULL RIMP2 is   0.06 seconds
    >>> wall Time used in FULL RIMP2 is   0.06 seconds
  
 2nd-order doubles: loop info:
    Max. Vir batch dimension   =           19
    Tot. number of Vir batches =            1
    Max. Occ batch dimension   =            5
    Tot. number of Occ batches =            1
    Tot. Memory required       =    2.7869200000000000E-003  GB
    Job type                   =            1
 RIMP3: Second-order correlation energy (RIMP2)      =   -0.2046706161    
 RIMP3: Third-order energy correction (RIMP3-RIMP2)  =   -0.6801965146E-02
 RIMP3: Total third-order correlation energy (RIMP3) =   -0.2114725812    




    ******************************************************************************
    *                             CC ENERGY SUMMARY                              *
    ******************************************************************************

     E: Hartree-Fock energy                            :      -76.0240386898
     E: Correlation energy                             :       -0.2114725812
     E: Total RIMP3 energy                             :      -76.2355112710



    CC Memory summary
    -----------------
     Allocated memory for array4   :   0.000     GB
     Memory in use for array4      :   0.000     GB
     Max memory in use for array4  :   0.000     GB
    ------------------


    ------------------------------------------------------
    Total CPU  time used in CC           :        0.236000     s
    Total Wall time used in CC           :        0.232000     s
    ------------------------------------------------------


    Hostname       : pablo-AU                                          
    Job finished   : Date: 23/03/2017   Time: 10:58:44



    =============================================================================
                              -- end of CC program --
    =============================================================================



    Total no. of matmuls used:                      1051
    Total no. of Fock/KS matrix evaluations:          12
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                         16.311 MB
      Max allocated memory, type(matrix)                 207.360 kB
      Max allocated memory, real(realk)                   16.227 MB
      Max allocated memory, integer                       83.881 kB
      Max allocated memory, logical                        0.780 kB
      Max allocated memory, character                      2.080 kB
      Max allocated memory, AOBATCH                       77.824 kB
      Max allocated memory, BATCHTOORB                     0.112 kB
      Max allocated memory, ODBATCH                        5.104 kB
      Max allocated memory, LSAOTENSOR                     5.472 kB
      Max allocated memory, SLSAOTENSOR                    6.624 kB
      Max allocated memory, ATOMTYPEITEM                 153.056 kB
      Max allocated memory, ATOMITEM                       1.584 kB
      Max allocated memory, LSMATRIX                      11.536 kB
      Max allocated memory, OverlapT                      93.152 kB
      Max allocated memory, linkshell                      1.260 kB
      Max allocated memory, integrand                    580.608 kB
      Max allocated memory, integralitem                   1.037 MB
      Max allocated memory, IntWork                      125.568 kB
      Max allocated memory, Overlap                       14.272 MB
      Max allocated memory, ODitem                         3.712 kB
      Max allocated memory, LStensor                      65.630 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   2.01 seconds
    >>> wall Time used in LSDALTON is   2.01 seconds

    End simulation
     Date and time (Linux)  : Thu Mar 23 10:58:44 2017
     Host name              : pablo-AU                                
