#!/usr/bin/env python

import os
import sys
import shutil

sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
from runtest_lsdalton import Filter, TestRun

test = TestRun(__file__, sys.argv)

# Input files:
DAL1 = "fullccsd_crashcalc"
DAL2 = "fullccsd_restart"
MOL  = "HF"

# Define filter for restarted calculation
# =======================================
f = Filter()
f.add(string = 'Nuclear repulsion',
      rel_tolerance = 1.0e-8)

f.add(string = 'Correlation energy',
      rel_tolerance = 1.0e-8)

f.add(string = 'E: Hartree-Fock energy',
      rel_tolerance = 1.0e-8)

f.add(string = 'Allocated memory (TOTAL)')

f.add(string = 'Memory in use for array4',
      rel_tolerance = 1.0e-9)

f2 = Filter()

# check that restart really happened
f2.add(string = 'Re-starting CC iterations from amplitudes of iteration')
f2.add(string = 'Correlation energy',
      rel_tolerance = 1.0e-8)
f2.add(string = 'E: Hartree-Fock energy',
      rel_tolerance = 1.0e-8)
f2.add(string = 'Allocated memory (TOTAL)')
f2.add(string = 'Memory in use for array4',
      rel_tolerance = 1.0e-9)


# Build list of files to save for restart
# =======================================
#HF_restart  = ['cmo_orbitals.u','lcm_orbitals.u','dens.restart','fock.restart','overlap.restart']
HF_restart  = ['lcm_orbitals.u','dens.restart','fock.restart']
CC_restart = ['cc_nfo.restart','ta11.tns','ta12.tns','ta21.tns','ta22.tns']

get_restart = ['-get','"'] + HF_restart + CC_restart + ['"']
put_restart = ['-put','"'] + HF_restart + CC_restart + ['"']

arg_restart = " ".join(get_restart)


# Run first calculation with CRASHCALC keyword
# ============================================
test.run([DAL1+'.dal'], [MOL+'.mol'], {'out': f}, args = arg_restart,
        accepted_errors = ['Crashed Calculation due to .CRASHCALC keyword'])

name = "_".join([DAL1,MOL]) 

# Remove prefix from restart files
# ================================
for filename in HF_restart:
    tmp = ".".join([name,filename])
    shutil.move(tmp,filename)

for filename in CC_restart:
    tmp = ".".join([name,filename])
    shutil.move(tmp,filename)


arg_restart = " ".join(put_restart)

# Run second calculation with RESTART keyword
# ===========================================
test.run([DAL2+'.dal'], [MOL+'.mol'], {'out': f2}, args = arg_restart)


# Delete files
# ============
for filename in HF_restart:
    os.remove(filename)

for filename in CC_restart:
    os.remove(filename)

sys.exit(test.return_code)

