  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | bykov
     Host                     | st-d04262
     System                   | Darwin-13.4.0
     CMake generator          | Unix Makefiles
     Processor                | i386
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/local/bin/gfortran
     Fortran compiler version | GNU Fortran (GCC) 4.9.0 20140309 (experimental)
     C compiler               | /usr/bin/gcc
     C compiler version       | Apple LLVM version 6.0 (clang-600.0.56) (based on 
                              | LLVM 3.5svn)
     C++ compiler             | /usr/bin/g++
     C++ compiler version     | Apple LLVM version 6.0 (clang-600.0.56) (based on 
                              | LLVM 3.5svn)
     BLAS                     | /usr/lib/libblas.dylib
     LAPACK                   | /usr/lib/liblapack.dylib
     Static linking           | OFF
     Last Git revision        | d4b8fd755686dea1a2fb9a28e4eb7bc88ddfaa72
     Git branch               | bykov/unrestmp2
     Configuration time       | 2016-06-13 22:16:50.228105
  

         Start simulation
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    6-31G                                   
                                            
                                            
    Atomtypes=2 Generators=0 Charge=1                                                                                       
    Charge=6.0 Atoms=2                                                                                                      
    C   0.66006866310997      0.00000000001283     -0.00000000000029                                                        
    C   -0.66006866311006      0.00000000001289      0.00000000000033                                                       
    Charge=1.0 Atoms=4                                                                                                      
    H   1.22990138668480     -0.92185326325999      0.00000000000006                                                        
    H   1.22990138677291      0.92185326324721      0.00000000000008                                                        
    H   -1.22990138668484     -0.92185326325998     -0.00000000000009                                                       
    H   -1.22990138677279      0.92185326324705     -0.00000000000010                                                       
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .UNREST
    .ARH
    .START
    ATOMS
    .CONVDYN
    TIGHT
    **CC
    .MP2
    .CANONICAL
    .MEMORY
    2.0
    *END OF INPUT
 

WARNING: Number of short HX and YX bond lengths:    4    1
WARNING: If not intentional, maybe your coordinates were in Angstrom,
WARNING: but "Angstrom" was not specified in .mol file
 
                      
    Atoms and basis sets
      Total number of atoms        :      6
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 C      6.000 6-31G                     22        9 [10s4p|3s2p]                                 
          2 C      6.000 6-31G                     22        9 [10s4p|3s2p]                                 
          3 H      1.000 6-31G                      4        2 [4s|2s]                                      
          4 H      1.000 6-31G                      4        2 [4s|2s]                                      
          5 H      1.000 6-31G                      4        2 [4s|2s]                                      
          6 H      1.000 6-31G                      4        2 [4s|2s]                                      
    ---------------------------------------------------------------------
    total         16                               60       26
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    1.0000
      Regular basisfunctions             :       26
      Primitive Regular basisfunctions   :       60
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    You have requested Augmented Roothaan-Hall optimization
    => explicit averaging is turned off!

    Expand trust radius if ratio is larger than:            0.75
    Contract trust radius if ratio is smaller than:         0.25
    On expansion, trust radius is expanded by a factor      1.20
    On contraction, trust radius is contracted by a factor  0.70

    Maximum size of subspace in ARH linear equations:       2

    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          

    Maximum size of Fock/density queue in averaging:   10

    WARNING WARNING WARNING spin check commented out!!! /Stinne


    --------------------------
    <Unrestricted calculation>
    --------------------------
    ALPHA spin occupancy =     7
    BETA  spin occupancy =     8

 
    Matrix type: mtype_unres_dense
 
    Due to the tightend SCF convergence threshold we also tighten the integral Threshold
    with a factor:      0.100000

    Dynamic convergence threshold for gradient:   0.39E-05
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Due to the presence of the keyword (default for correlation)
    .NOGCINTEGRALTRANSFORM
    We transform the input basis to the Grand Canonical
    basis and perform integral evaluation using this basis
     
    The Overall Screening threshold is set to              :  1.0000E-09
    The Screening threshold used for Coulomb               :  1.0000E-11
    The Screening threshold used for Exchange              :  1.0000E-09
    The Screening threshold used for One-electron operators:  1.0000E-16
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis
    We activate the Atomic Fragment Optimization Correction (AFOC)

    End of configuration!

 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 

    Level 1 atomic calculation on 6-31G Charge   6
    ================================================
  
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1    -36.1261883331    0.00000000000    0.00      0.00000    0.00    1.811E+00 ###
      2    -37.2417446448   -1.11555631170    0.00      0.00000    0.00    5.280E-01 ###
      3    -37.3143365252   -0.07259188037   -1.00      0.00000    0.00    8.028E-02 ###
      4    -37.3153570603   -0.00102053510   -1.00      0.00000    0.00    1.000E-02 ###
      5    -37.3153753736   -0.00001831338   -1.00      0.00000    0.00    5.076E-04 ###

    Level 1 atomic calculation on 6-31G Charge   1
    ================================================
  
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1     -0.3410549973    0.00000000000    0.00      0.00000    0.00    1.982E-01 ###
      2     -0.3488197504   -0.00776475307    0.00      0.00000    0.00    1.270E-02 ###
      3     -0.3488518065   -0.00003205613   -1.00      0.00000    0.00    3.937E-05 ###
 
    Matrix type: mtype_unres_dense

    First density: Atoms in molecule guess

    Iteration 0 energy:      -79.873479337012
 
 FALLBACK: open shell currently works only with cholesky decomposition
 - Ask Dr. Jansik to fix it for Lowdin!
    Preparing to do cholesky decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  3.87298335E-06
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1    -73.3123684899    0.00000000000    0.00      0.00000    0.00    5.989E-01 ###
      2    -73.3751033922   -0.06273490228    0.00      0.00000   -0.00    8.251E-02 ###
      3    -73.3768204965   -0.00171710433    0.00      0.00000   -0.00    1.958E-02 ###
      4    -73.3769138277   -0.00009333120    0.00      0.00000   -0.00    2.674E-03 ###
      5    -73.3769158073   -0.00000197958    0.00      0.00000   -0.00    1.729E-04 ###
      6    -73.3769158152   -0.00000000793    0.00      0.00000   -0.00    2.199E-05 ###
      7    -73.3769158154   -0.00000000013    0.00      0.00000   -0.00    5.715E-06 ###
      8    -73.3769158154   -0.00000000001    0.00      0.00000   -0.00    1.433E-06 ###
    SCF converged in      8 iterations
    >>>  CPU Time used in SCF iterations is   1.46 seconds
    >>> wall Time used in SCF iterations is   1.45 seconds

    Total no. of matmuls in SCF optimization:        933

    Number of occupied alpha orbitals:       7
    Number of occupied beta orbitals:        8

    Number of virtual alpha orbitals:      19
    Number of virtual beta orbitals:       18

    Number of occupied alpha orbital energies to be found:       1
    Number of occupied beta orbital energies to be found:        1

    Number of virtual alpha orbital energies to be found:       1
    Number of virtual beta orbital energies to be found:        1


Calculation of alpha occupied orbital energies converged in     6 iterations!

Calculation of beta occupied orbital energies converged in     7 iterations!

Calculation of alpha virtual orbital energies converged in    17 iterations!

Calculation of beta virtual orbital energies converged in     8 iterations!

    E(LUMO), alpha:                         -0.362334 au
   -E(HOMO), alpha:                         -1.086576 au
   -------------------------------------
    "Alpha" HOMO-LUMO Gap (iteratively):     0.724242 au


    E(LUMO), beta:                           0.028691 au
   -E(HOMO), beta:                          -0.954489 au
   -------------------------------------
    "Beta" HOMO-LUMO Gap (iteratively):      0.983181 au


    E(LUMO):                                -0.362334 au
   -E(HOMO):                                -0.954489 au
   -------------------------------------
    "Overall" HOMO-LUMO Gap (iteratively):   0.592156 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.06273490228    0.0000   -0.0000    0.0000000
      3   -0.00171710433    0.0000   -0.0000    0.0000000
      4   -0.00009333120    0.0000   -0.0000    0.0000000
      5   -0.00000197958    0.0000   -0.0000    0.0000000
      6   -0.00000000793    0.0000   -0.0000    0.0000000
      7   -0.00000000013    0.0000   -0.0000    0.0000000
      8   -0.00000000001    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1           -73.31236848992683974302      0.598933098889294D+00
        2           -73.37510339220789035153      0.825074402349286D-01
        3           -73.37682049653498950192      0.195784170455354D-01
        4           -73.37691382773213888413      0.267413446060560D-02
        5           -73.37691580731352303246      0.172875401842907D-03
        6           -73.37691581524173045636      0.219918719207100D-04
        7           -73.37691581537292506709      0.571502693907161D-05
        8           -73.37691581538241791804      0.143279350804783D-05

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                       -73.376915815382
          Nuclear repulsion:                      63.376897404727
          Electronic energy:                    -136.753813220109



    -- Full molecular info --

    FULL: Overall charge of molecule    :      1

    FULL: Number of electrons           :     15
    FULL: Number of atoms               :      6
    FULL: Number of basis func.         :     26
    FULL: Number of aux. basis func.    :      0
    FULL: Number of core orbitals       :      2
    FULL: Number of valence orbitals    :      5
    FULL: Number of occ. orbitals       :      7
    FULL: Number of occ. alpha orbitals :      7
    FULL: Number of occ. beta  orbitals :      8
    FULL: Number of virt. orbitals      :     19
    FULL: Local memory use type full    :  0.13E-04
    FULL: Distribute matrices           : F

 Allocate space for molecule%Co on Master use_bg= F
 Allocate space for molecule%Cv on Master use_bg= F
    Memory set in input to be:    2.000     GB


    =============================================================================
         -- Full molecular Coupled-Cluster calculation -- 
    =============================================================================

    Using canonical orbitals as requested in input!



 ================================================ 
              Full molecular driver               
 ================================================ 





    ******************************************************************************
    *                             CC ENERGY SUMMARY                              *
    ******************************************************************************

     E: Hartree-Fock energy                            :      -73.3769158154
     E: Correlation energy                             :       -0.1029236089
     E: Total MP2 energy                               :      -73.4798394243



    CC Memory summary
    -----------------
     Allocated memory for array4   :   0.000     GB
     Memory in use for array4      :   0.000     GB
     Max memory in use for array4  :   0.000     GB
    ------------------


    ------------------------------------------------------
    Total CPU  time used in CC           :        0.430011     s
    Total Wall time used in CC           :        0.419000     s
    ------------------------------------------------------


    Hostname       : st-d04262                                         
    Job finished   : Date: 13/06/2016   Time: 22:25:36



    =============================================================================
                              -- end of CC program --
    =============================================================================



    Total no. of matmuls used:                       949
    Total no. of Fock/KS matrix evaluations:           9
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                          8.557 MB
      Max allocated memory, type(matrix)                 432.640 kB
      Max allocated memory, real(realk)                    7.643 MB
      Max allocated memory, integer                      103.508 kB
      Max allocated memory, logical                        1.092 kB
      Max allocated memory, character                      2.640 kB
      Max allocated memory, AOBATCH                       26.624 kB
      Max allocated memory, BATCHTOORB                     0.112 kB
      Max allocated memory, ODBATCH                        6.336 kB
      Max allocated memory, LSAOTENSOR                    21.888 kB
      Max allocated memory, SLSAOTENSOR                   40.848 kB
      Max allocated memory, ATOMTYPEITEM                 153.056 kB
      Max allocated memory, ATOMITEM                       3.584 kB
      Max allocated memory, LSMATRIX                       4.032 kB
      Max allocated memory, OverlapT                     118.144 kB
      Max allocated memory, linkshell                      1.632 kB
      Max allocated memory, integrand                    896.000 kB
      Max allocated memory, integralitem                   2.048 MB
      Max allocated memory, IntWork                      161.608 kB
      Max allocated memory, Overlap                        4.308 MB
      Max allocated memory, ODitem                         4.608 kB
      Max allocated memory, LStensor                      38.972 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   2.39 seconds
    >>> wall Time used in LSDALTON is   2.37 seconds

    End simulation
