     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | magnus
     Host                     | foeroyar
     System                   | Linux-4.15.9-300.fc27.x86_64
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/bin/gfortran
     Fortran compiler version | GNU Fortran (GCC) 7.3.1 20180303 (Red Hat 7.3.1-5)
     C compiler               | /usr/bin/gcc
     C compiler version       | gcc (GCC) 7.3.1 20180303 (Red Hat 7.3.1-5)
     C++ compiler             | /usr/bin/g++
     C++ compiler version     | g++ (GCC) 7.3.1 20180303 (Red Hat 7.3.1-5)
     BLAS                     | /home/magnus/Programs/intel/compilers_and_librarie
                              | s_2018.1.163/linux/mkl/lib/intel64/libmkl_gf_lp64.
                              | so;/home/magnus/Programs/intel/compilers_and_libra
                              | ries_2018.1.163/linux/mkl/lib/intel64/libmkl_seque
                              | ntial.so;/home/magnus/Programs/intel/compilers_and
                              | _libraries_2018.1.163/linux/mkl/lib/intel64/libmkl
                              | _core.so;/usr/lib64/libpthread.so;/usr/lib64/libm.
                              | so
     LAPACK                   | /home/magnus/Programs/intel/compilers_and_librarie
                              | s_2018.1.163/linux/mkl/lib/intel64/libmkl_lapack95
                              | _lp64.a;/home/magnus/Programs/intel/compilers_and_
                              | libraries_2018.1.163/linux/mkl/lib/intel64/libmkl_
                              | gf_lp64.so
     Static linking           | OFF
     Last Git revision        | c302fb14fe9246b903b0aa544ea6c9d865e8bf89
     Git branch               | frame-openrsp
     Configuration time       | 2018-03-21 23:02:27.661788
  

         Start simulation
     Date and time (Linux)  : Wed Mar 21 23:03:05 2018
     Host name              : foeroyar                                
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    STO-3G                                  
    Acrolein                                
    ------------------------                
    AtomTypes=3 NoSymmetry Angstrom                                                                                         
    Charge=6.0 Atoms=3                                                                                                      
    C             -0.145335   -0.546770    0.000607                                                                         
    C              1.274009   -0.912471   -0.000167                                                                         
    C              1.630116   -2.207690   -0.000132                                                                         
    Charge=8.0 Atoms=1                                                                                                      
    O             -0.560104    0.608977    0.000534                                                                         
    Charge=1.0 Atoms=4                                                                                                      
    H             -0.871904   -1.386459    0.001253                                                                         
    H              2.004448   -0.101417   -0.000710                                                                         
    H              0.879028   -3.000685    0.000484                                                                         
    H              2.675323   -2.516779   -0.000673                                                                         
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **GENERAL
    .NOGCBASIS
    **FraME
    **INTEGRALS
    .NO SCREEN
    **WAVE FUNCTIONS
    .HF
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :      8
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 C      6.000 STO-3G                    15        5 [6s3p|2s1p]                                  
          2 C      6.000 STO-3G                    15        5 [6s3p|2s1p]                                  
          3 C      6.000 STO-3G                    15        5 [6s3p|2s1p]                                  
          4 O      8.000 STO-3G                    15        5 [6s3p|2s1p]                                  
          5 H      1.000 STO-3G                     3        1 [3s|1s]                                      
          6 H      1.000 STO-3G                     3        1 [3s|1s]                                      
          7 H      1.000 STO-3G                     3        1 [3s|1s]                                      
          8 H      1.000 STO-3G                     3        1 [3s|1s]                                      
    ---------------------------------------------------------------------
    total         30                               72       24
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       24
      Primitive Regular basisfunctions   :       72
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    You have requested Augmented Roothaan-Hall optimization
    => explicit averaging is turned off!

    Expand trust radius if ratio is larger than:            0.75
    Contract trust radius if ratio is smaller than:         0.25
    On expansion, trust radius is expanded by a factor      1.20
    On contraction, trust radius is contracted by a factor  0.70

    Maximum size of subspace in ARH linear equations:       2

    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          

    Maximum size of Fock/density queue in averaging:   10

    Convergence threshold for gradient        :   0.10E-03
    We perform the calculation in the standard input basis
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis

    End of configuration!


    First density: Atoms in molecule guess

    Iteration 0 energy:     -195.317866148368
 
    Diagonalize the Atoms in Molecule Fock Matrix to obtain Initial Density
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1   -188.1528759874    0.00000000000    0.00      0.00000    0.00    4.884E-01 ###
      2   -188.2162945097   -0.06341852226    0.00      0.00000   -0.00    4.272E-01 ###
      3   -188.3147473525   -0.09845284274    0.00      0.00000   -0.00    4.560E-02 ###
      4   -188.3172320405   -0.00248468804    0.00      0.00000   -0.00    1.036E-02 ###
      5   -188.3173865858   -0.00015454534    0.00      0.00000   -0.00    5.145E-03 ###
      6   -188.3174053207   -0.00001873486    0.00      0.00000   -0.00    1.386E-03 ###
      7   -188.3174084305   -0.00000310976    0.00      0.00000   -0.00    4.267E-04 ###
      8   -188.3174086699   -0.00000023945    0.00      0.00000   -0.00    8.689E-05 ###
    SCF converged in      8 iterations
    >>>  CPU Time used in SCF iterations is   4.04 seconds
    >>> wall Time used in SCF iterations is   4.06 seconds

    Total no. of matmuls in SCF optimization:        617

    Number of occupied orbitals:      15
    Number of virtual orbitals:        9

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in    15 iterations!

    Calculation of virtual orbital energies converged in     9 iterations!

     E(LUMO):                         0.204228 au
    -E(HOMO):                        -0.325572 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.529800 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.06341852226    0.0000   -0.0000    0.0000000
      3   -0.09845284274    0.0000   -0.0000    0.0000000
      4   -0.00248468804    0.0000   -0.0000    0.0000000
      5   -0.00015454534    0.0000   -0.0000    0.0000000
      6   -0.00001873486    0.0000   -0.0000    0.0000000
      7   -0.00000310976    0.0000   -0.0000    0.0000000
      8   -0.00000023945    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1          -188.15287598744657771022      0.488395126346912D+00
        2          -188.21629450970885955030      0.427185384668276D+00
        3          -188.31474735245308238518      0.456028739158234D-01
        4          -188.31723204049598052734      0.103593734930042D-01
        5          -188.31738658583330447982      0.514519105926471D-02
        6          -188.31740532069449045594      0.138596312704518D-02
        7          -188.31740843045901101505      0.426727336368526D-03
        8          -188.31740866991071925440      0.868876797267845D-04

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                      -188.317408669911
          Nuclear repulsion:                     102.664846375494
          Electronic energy:                    -290.982255045405

    Total no. of matmuls used:                       633
    Total no. of Fock/KS matrix evaluations:           9
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):    216576 byte Should be zero, otherwise a leakage is present
      Allocated memory (type(matrix)):         216576 byte  - Should be zero - otherwise a leakage is present
 
      Max allocated memory, TOTAL                          1.287 MB
      Max allocated memory, type(matrix)                 373.248 kB
      Max allocated memory, real(realk)                    1.184 MB
      Max allocated memory, integer                       54.274 kB
      Max allocated memory, logical                        0.960 kB
      Max allocated memory, character                      3.904 kB
      Max allocated memory, AOBATCH                       44.200 kB
      Max allocated memory, ODBATCH                       13.728 kB
      Max allocated memory, LSAOTENSOR                    38.912 kB
      Max allocated memory, ATOMTYPEITEM                 114.792 kB
      Max allocated memory, ATOMITEM                       6.048 kB
      Max allocated memory, LSMATRIX                       4.368 kB
      Max allocated memory, OverlapT                     213.120 kB
      Max allocated memory, linkshell                      3.600 kB
      Max allocated memory, integrand                     64.512 kB
      Max allocated memory, integralitem                   1.037 MB
      Max allocated memory, IntWork                       27.448 kB
      Max allocated memory, Overlap                      388.452 kB
      Max allocated memory, ODitem                         9.984 kB
      Max allocated memory, LStensor                      33.538 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   4.85 seconds
    >>> wall Time used in LSDALTON is   4.88 seconds

    End simulation
     Date and time (Linux)  : Wed Mar 21 23:03:10 2018
     Host name              : foeroyar                                
