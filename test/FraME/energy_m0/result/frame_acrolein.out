  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | magnus
     Host                     | foeroyar
     System                   | Linux-4.13.16-302.fc27.x86_64
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /home/magnus/Programs/intel/compilers_and_librarie
                              | s_2018.0.128/linux/bin/intel64/ifort
     Fortran compiler version | ifort (IFORT) 18.0.0 20170811
     C compiler               | /home/magnus/Programs/intel/compilers_and_librarie
                              | s_2018.0.128/linux/bin/intel64/icc
     C compiler version       | icc (ICC) 18.0.0 20170811
     C++ compiler             | /home/magnus/Programs/intel/compilers_and_librarie
                              | s_2018.0.128/linux/bin/intel64/icpc
     C++ compiler version     | icpc (ICC) 18.0.0 20170811
     Static linking           | OFF
     Last Git revision        | ad65f25935da77eb829a498032f4d28de6c9aaa1
     Git branch               | frame-develop
     Configuration time       | 2017-12-05 13:05:59.677033
  

         Start simulation
     Date and time (Linux)  : Tue Dec  5 15:25:42 2017
     Host name              : foeroyar                                
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    6-31G                                   
    Generated by PEAS 0.1.0                 
    Some informative stuff...               
    AtomTypes=3 Charge=0.0 Angstrom                                                                                         
    Charge=8.0 Atoms=1                                                                                                      
    O    1.542000    0.710000   -0.255000                                                                                   
    Charge=6.0 Atoms=3                                                                                                      
    C    0.674000   -0.126000   -0.067000                                                                                   
    C   -0.749000    0.181000    0.071000                                                                                   
    C   -1.633000   -0.798000    0.275000                                                                                   
    Charge=1.0 Atoms=4                                                                                                      
    H    0.937000   -1.200000    0.010000                                                                                   
    H   -1.044000    1.225000    0.000000                                                                                   
    H   -1.313000   -1.832000    0.342000                                                                                   
    H   -2.692000   -0.601000    0.381000                                                                                   
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **FraME
    **WAVE FUNCTIONS
    .HF
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :      8
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 O      8.000 6-31G                     22        9 [10s4p|3s2p]                                 
          2 C      6.000 6-31G                     22        9 [10s4p|3s2p]                                 
          3 C      6.000 6-31G                     22        9 [10s4p|3s2p]                                 
          4 C      6.000 6-31G                     22        9 [10s4p|3s2p]                                 
          5 H      1.000 6-31G                      4        2 [4s|2s]                                      
          6 H      1.000 6-31G                      4        2 [4s|2s]                                      
          7 H      1.000 6-31G                      4        2 [4s|2s]                                      
          8 H      1.000 6-31G                      4        2 [4s|2s]                                      
    ---------------------------------------------------------------------
    total         30                              104       44
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       44
      Primitive Regular basisfunctions   :      104
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    You have requested Augmented Roothaan-Hall optimization
    => explicit averaging is turned off!
 
    Expand trust radius if ratio is larger than:            0.75
    Contract trust radius if ratio is smaller than:         0.25
    On expansion, trust radius is expanded by a factor      1.20
    On contraction, trust radius is contracted by a factor  0.70
 
    Maximum size of subspace in ARH linear equations:       2
 
    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          
 
    Maximum size of Fock/density queue in averaging:   10
 
    Convergence threshold for gradient        :   0.10E-03
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Since the input basis set is a segmented contracted basis we
    perform the integral evaluation in the more efficient
    standard input basis and then transform to the Grand 
    Canonical basis, which is general contracted.
    You can force the integral evaluation in Grand 
    Canonical basis by using the keyword
    .NOGCINTEGRALTRANSFORM
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis
 
    End of configuration!
 
 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 
 
    Level 1 atomic calculation on 6-31G Charge   8
    ================================================
  
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1    -71.4946859143    0.00000000000    0.00      0.00000    0.00    4.531E+00 ###
      2    -73.8341599123   -2.33947399805    0.00      0.00000    0.00    2.663E+00 ###
      3    -74.2580432558   -0.42388334348   -1.00      0.00000    0.00    1.607E-01 ###
      4    -74.2598387062   -0.00179545041   -1.00      0.00000    0.00    3.161E-02 ###
      5    -74.2598920070   -0.00005330082   -1.00      0.00000    0.00    2.600E-03 ###
      6    -74.2598924085   -0.00000040150   -1.00      0.00000    0.00    1.479E-06 ###
 
    Level 1 atomic calculation on 6-31G Charge   6
    ================================================
  
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1    -36.1261883331    0.00000000000    0.00      0.00000    0.00    1.811E+00 ###
      2    -37.2417446448   -1.11555631170    0.00      0.00000    0.00    5.280E-01 ###
      3    -37.3143365252   -0.07259188037   -1.00      0.00000    0.00    8.028E-02 ###
      4    -37.3153570603   -0.00102053510   -1.00      0.00000    0.00    1.000E-02 ###
      5    -37.3153753736   -0.00001831338   -1.00      0.00000    0.00    5.076E-04 ###
 
    Level 1 atomic calculation on 6-31G Charge   1
    ================================================
  
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1     -0.3410549973    0.00000000000    0.00      0.00000    0.00    1.982E-01 ###
      2     -0.3488197504   -0.00776475307    0.00      0.00000    0.00    1.270E-02 ###
      3     -0.3488518065   -0.00003205613   -1.00      0.00000    0.00    3.937E-05 ###
 
    Matrix type: mtype_dense
 
    First density: Atoms in molecule guess
 
    Iteration 0 energy:     -192.155824629963
 
    Diagonalize the Atoms in Molecule Fock Matrix to obtain Initial Density
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1   -190.5831714620    0.00000000000    0.00      0.00000    0.00    4.877E-01 ###
      2   -190.6404298583   -0.05725839627    0.00      0.00000    0.00    4.141E-01 ###
      3   -190.7040990084   -0.06366915014    0.00      0.00000    0.00    6.926E-02 ###
      4   -190.7068712945   -0.00277228610    0.00      0.00000    0.00    1.323E-02 ###
      5   -190.7069963136   -0.00012501913    0.00      0.00000    0.00    5.192E-03 ###
      6   -190.7070174250   -0.00002111132    0.00      0.00000    0.00    1.569E-03 ###
      7   -190.7070202177   -0.00000279278    0.00      0.00000    0.00    8.338E-04 ###
      8   -190.7070207053   -0.00000048751    0.00      0.00000    0.00    1.451E-04 ###
      9   -190.7070207387   -0.00000003341    0.00      0.00000    0.00    2.953E-05 ###
    SCF converged in      9 iterations
    >>>  CPU Time used in SCF iterations is   3.58 seconds
    >>> wall Time used in SCF iterations is   3.59 seconds
 
    Total no. of matmuls in SCF optimization:        872
 
    Number of occupied orbitals:      15
    Number of virtual orbitals:       29
 
    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1
 
 
    Calculation of HOMO-LUMO gap
    ============================
 
    Calculation of occupied orbital energies converged in    15 iterations!
 
    Calculation of virtual orbital energies converged in    11 iterations!
 
     E(LUMO):                         0.074994 au
    -E(HOMO):                        -0.400019 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.475013 au
 
 
    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.05725839627    0.0000    0.0000    0.0000000
      3   -0.06366915014    0.0000    0.0000    0.0000000
      4   -0.00277228610    0.0000    0.0000    0.0000000
      5   -0.00012501913    0.0000    0.0000    0.0000000
      6   -0.00002111132    0.0000    0.0000    0.0000000
      7   -0.00000279278    0.0000    0.0000    0.0000000
      8   -0.00000048751    0.0000    0.0000    0.0000000
      9   -0.00000003341    0.0000    0.0000    0.0000000
 
    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1          -190.58317146200462843808      0.487657819749959D+00
        2          -190.64042985827185816561      0.414144328455472D+00
        3          -190.70409900841249850600      0.692575128991510D-01
        4          -190.70687129450783459106      0.132284184157315D-01
        5          -190.70699631364033166392      0.519181659598641D-02
        6          -190.70701742496143538119      0.156946352825469D-02
        7          -190.70702021774025070044      0.833816749874389D-03
        8          -190.70702070525473459384      0.145104887711400D-03
        9          -190.70702073866411296876      0.295288187546895D-04
 
          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<
 
 
          Final HF energy:                      -190.707020738664
          Nuclear repulsion:                     103.152862428166
          Electronic energy:                    -293.859883166830
 
    Total no. of matmuls used:                       888
    Total no. of Fock/KS matrix evaluations:          10
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):    325248 byte Should be zero, otherwise a leakage is present
      Allocated memory (type(matrix)):         325248 byte  - Should be zero - otherwise a leakage is present
 
      Max allocated memory, TOTAL                          6.472 MB
      Max allocated memory, type(matrix)                 960.256 kB
      Max allocated memory, real(realk)                    2.505 MB
      Max allocated memory, integer                      217.511 kB
      Max allocated memory, logical                        1.852 kB
      Max allocated memory, character                     18.352 kB
      Max allocated memory, AOBATCH                      578.560 kB
      Max allocated memory, ODBATCH                      110.264 kB
      Max allocated memory, LSAOTENSOR                    54.272 kB
      Max allocated memory, SLSAOTENSOR                  570.880 kB
      Max allocated memory, ATOMTYPEITEM                 337.584 kB
      Max allocated memory, ATOMITEM                     306.144 kB
      Max allocated memory, LSMATRIX                      17.360 kB
      Max allocated memory, OverlapT                       3.283 MB
      Max allocated memory, linkshell                      9.520 kB
      Max allocated memory, integrand                    290.304 kB
      Max allocated memory, integralitem                   1.659 MB
      Max allocated memory, IntWork                       55.096 kB
      Max allocated memory, Overlap                        1.552 MB
      Max allocated memory, ODitem                        80.192 kB
      Max allocated memory, LStensor                     114.019 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
 
    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   4.19 seconds
    >>> wall Time used in LSDALTON is   4.20 seconds

    End simulation
     Date and time (Linux)  : Tue Dec  5 15:25:46 2017
     Host name              : foeroyar                                
