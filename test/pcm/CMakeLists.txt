add_lsdalton_runtest(NAME pcm_energy LABELS "runtest;pcm;ContinuousIntegration")

# Conditionally available tests
if(ENABLE_RSP)
  add_lsdalton_runtest(NAME pcm_linear_response LABELS "runtest;pcm;lsresponse;ContinuousIntegration")
  add_lsdalton_runtest(NAME pcm_quadratic_response LABELS "runtest;pcm;lsresponse")
  add_lsdalton_runtest(NAME pcm_cubic_response LABELS "runtest;pcm;lsresponse;ContinuousIntegration")
endif()
