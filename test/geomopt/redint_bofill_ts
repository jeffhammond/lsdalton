#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > redint_bofill_ts.info <<'%EOF%'
   redint_bofill_ts
   -------------
   Molecule:         CH3-CH3 = CH2-CH2 + H2
   Wave Function:    HF / STO-3G
   Test Purpose:     Test 1st order TS optimizer in redundant internal
   coordinates with model Hessian updated with Bofill's formula
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > redint_bofill_ts.mol <<'%EOF%'
BASIS
STO-3G
Second order saddle point optimization in Cartesian coordinates

Atomtypes=2
Charge=6.0 Atoms=2
C     0.0000000000        0.0000000000        1.4597876282   
C     0.0000000000        0.0000000000       -1.4597876282   
Charge=1.0 Atoms=6
H     1.6808445513        0.9704360937        2.1445545504   
H    -1.6808445513        0.9704360937        2.1445545504   
H     0.0000000000       -1.9408721894        2.1445545504   
H    -1.9608445513       -0.4204360937       -2.1445545504   
H     1.2008445513       -1.4004360937       -2.1445545504   
H     0.4600000000        1.8308721894       -2.1445545504   
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > redint_bofill_ts.dal <<'%EOF%'
**OPTIMI
.REDINT
.SADDLE
.BAKER
.PRINT 
12
**WAVE FUNCTION
.HF
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > redint_bofill_ts.check
cat >> redint_bofill_ts.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final HF energy: * \-78\.3016053" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Number of energies
CRIT3=`$GREP "Final * HF energy" $log | wc -l` 
TEST[3]=`expr   $CRIT3`
CTRL[3]=9
ERROR[3]="Wrong number of geometry steps"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
