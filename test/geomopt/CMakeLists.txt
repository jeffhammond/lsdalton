add_lsdalton_runtest_v1(NAME geoopt_constrain             )
add_lsdalton_runtest_v1(NAME geoopt_purify                )
add_lsdalton_runtest_v1(NAME geoopt_rapid LABELS "ContinuousIntegration")
add_lsdalton_runtest_v1(NAME redint_numgrad)
add_lsdalton_test(NAME cartes_bfgs_min        )
add_lsdalton_test(NAME cartes_psb_min         )
add_lsdalton_test(NAME cartes_trilevel_newconv)
add_lsdalton_test(NAME redint_bfgs            )
add_lsdalton_test(NAME redint_bofill_ts       )

# Conditionally available tests
if(ENABLE_RSP)
  add_lsdalton_runtest_v1(NAME Excited_state_opt LABELS "lsresponse")
endif()

