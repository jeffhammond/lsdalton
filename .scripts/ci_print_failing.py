#!/usr/bin/env python
import re
import sys

badtests = []
testfail = re.compile(r'^\s*(?P<num>\d+) - (?P<name>\w+(?:-\w+)*) \(Failed\)\s*$')

with open('full_ctest_output.dat', 'r') as outfile:
    ctestout = outfile.readlines()

ctest_exit_status = int(ctestout[0])
if (ctest_exit_status == 0):
    sys.stdout.write('\n  <<<  All test cases have passed!  >>>\n\n')
else:
    sys.stdout.write('\n  <<<  Failing outputs follow.  >>>\n\n')
    sys.stdout.write(''.join(ctestout))

# <<<  return ctest error code  >>>
sys.exit(ctest_exit_status)
