: # *-*-perl-*-*
  eval 'exec perl -S $0 ${1+"$@"}'
    if 0;
#
# -----------
#   com2mol
# -----------
#
# Script to convert Gaussian .com files to Dalton .mol files
# Requires Perl 5
#
# modified by Simen Reine 2011
# modified by Vebj�rn Bakken 2004
# written by Alf Christian Hennum 2001
#
# Declare the subroutines
sub trim($);
sub ltrim($);
sub rtrim($);

# Perl trim function to remove whitespace from the start and end of the string
sub trim($)
{
        my $string = shift;
        $string =~ s/^\s+//;
        $string =~ s/\s+$//;
        return $string;
}
# Left trim function to remove leading whitespace
sub ltrim($)
{
        my $string = shift;
        $string =~ s/^\s+//;
        return $string;
}
# Right trim function to remove trailing whitespace
sub rtrim($)
{
        my $string = shift;
        $string =~ s/\s+$//;
        return $string;
}

# Atoms are defined in the range H to Kr
%atoms = ( 'H'  => { charge =>  1, number => 0, a => [] },
           'He' => { charge =>  2, number => 0, a => [] },
           'Li' => { charge =>  3, number => 0, a => [] },
           'Be' => { charge =>  4, number => 0, a => [] },
           'B'  => { charge =>  5, number => 0, a => [] },
           'C'  => { charge =>  6, number => 0, a => [] },
           'N'  => { charge =>  7, number => 0, a => [] },
           'O'  => { charge =>  8, number => 0, a => [] },
           'F'  => { charge =>  9, number => 0, a => [] },
           'Ne' => { charge => 10, number => 0, a => [] },
           'Na' => { charge => 11, number => 0, a => [] },
           'Mg' => { charge => 12, number => 0, a => [] },
           'Al' => { charge => 13, number => 0, a => [] },
           'Si' => { charge => 14, number => 0, a => [] },
           'P'  => { charge => 15, number => 0, a => [] },
           'S'  => { charge => 16, number => 0, a => [] },
           'Cl' => { charge => 17, number => 0, a => [] },
           'Ar' => { charge => 18, number => 0, a => [] },
           'K'  => { charge => 19, number => 0, a => [] },
           'Ca' => { charge => 20, number => 0, a => [] },
           'Sc' => { charge => 21, number => 0, a => [] },
           'Ti' => { charge => 22, number => 0, a => [] },
           'V'  => { charge => 23, number => 0, a => [] },
           'Cr' => { charge => 24, number => 0, a => [] },
           'Mn' => { charge => 25, number => 0, a => [] },
           'Fe' => { charge => 26, number => 0, a => [] },
           'Co' => { charge => 27, number => 0, a => [] },
           'Ni' => { charge => 28, number => 0, a => [] },
           'Cu' => { charge => 29, number => 0, a => [] },
           'Zn' => { charge => 30, number => 0, a => [] },
           'Ga' => { charge => 31, number => 0, a => [] },
           'Ge' => { charge => 32, number => 0, a => [] },
           'As' => { charge => 33, number => 0, a => [] },
           'Se' => { charge => 34, number => 0, a => [] },
           'Br' => { charge => 35, number => 0, a => [] },
           'Kr' => { charge => 36, number => 0, a => [] }
	  ); 
$atomtype = 0;

#
# Handle command-line parameters
#
if (!@ARGV || $ARGV[0] eq "-h" || $ARGV[2] ne "") {
    help_message();
    exit;
}
$COMFILE = $ARGV[0];
if ($ARGV[1] eq "") {
    $_ = $ARGV[0];
    s/\.com/\.mol/;
    $MOLFILE = $_;
}
else
{
    $MOLFILE = "$ARGV[1]";
}

#
# Open files
#
open (COM,"$COMFILE") || die "\nERROR: Couldn't open .com-file $COMFILE\n\n";
read_comfile();
if  ($atomtype == 0 ) {
    print "\nERROR: Couldnt find any atoms in $COMFILE\n\n";
    exit;
}
open (MOL,">$MOLFILE") || die "\nERROR: Couldn't open .mol-file $MOLFILE\n\n";
#$molhandle = \*MOL;
write_molfile();

#
# Subroutines
#

sub help_message {
    print "\nUsage: com2mol .com-file [.mol-file]\n\n";
    print "The .com-file must be specified, if no mol-file is given,\n"; 
    print "the name is derived from the .com-file\n\n";
}

sub read_comfile {
    while (<COM>) {
	@line = split;
	foreach $label (keys %atoms) {
	    if ($line[0] =~ /$label/ && $line[0] !~ /Title/ ) {	
		if (check_line($_)) {
		    if (new_atom($label)) {$atomtype++;};
		    $atoms{$label}{number}++;
		    push @{$atoms{$label}{a}},ltrim($_);
		}
	    }
	}
    }
}

sub new_atom {
    my $label = $_[0];
    if ($atoms{$label}{number} == 0) {return(1);}
    else {return(0)};
}

sub check_line {
    my @parts = split($_[0]);    
    if ($#line != 3) { 
	return(0);
    }
    else {
# some valid number :  /^(-\d+\.?\d*|\d+\.?\d*|\.?\d+)/  
	if ($line[0] !~/[a-zA-Z]+/ || $line[1] !~ /^(-\d+\.?\d*|\d+\.?\d*|\.?\d+)/ || 
	    $line[2] !~  /^(-\d+\.?\d*|\d+\.?\d*|\.?\d+)/ || $line[3] !~ /^(-\d+\.?\d*|\d+\.?\d*|\.?\d+)/ ){ 
	    return(0);
	}
	else {
	    return(1);
	}
    }
}

sub write_molfile {  
    print  MOL "BASIS\nbasissett\nKommentar1\nKommentar2\n"; 
    printf MOL "Atomtypes=%d Angstrom\n",$atomtype;
    foreach $a (keys %atoms) {
	if($atoms{$a}{number}> 0){
	    printf MOL "Charge=%d\. Atoms=%d\n",$atoms{$a}{charge} ,$atoms{$a}{number}; 
	    print  MOL @{$atoms{$a}{a}};
	}
    }
}



# subs : 




sub read_dalout 
{ 
  my $found = 0; 
  while (<INF>) {
    if (/Final geometry/  || $found ) { 
      if (/Final geometry/) {
	$found=1;
	next}; 
      if (/Iter/) {return;}; 
      @line=split;      
      foreach $lab (keys %atoms) {
	if ($line[0]=~/$lab/)
	{
	  if (newatom($lab)) {$atomtype++;};       
	  checkline ($_); 
	  $atoms{$lab}{number}++;
	  push @{$atoms{$lab}{a}}, $_;
	};
      };
    };  
  };  
};


sub make_gaussian_input 
{
  print $outhandle "\ # rhf/3-21g \n\n";
  print $outhandle "Title Card Required\n\n";
  print $outhandle " 0  1\n";
  foreach $a (keys %atoms) {
    if($atoms{$a}{number} > 0){
      print  $outhandle @{$atoms{$a}{a}};
    };
  }; 
};

 
