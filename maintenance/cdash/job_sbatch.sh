#!/bin/bash

#The branch to be tested
branch=$1
#The build type (see list below)
build=$2
#The type of build to appear on https://testboard.org/cdash/index.php?project=LSDalton
type=$3
#The number of MPI ranks to run with
nmpi=$4
#The number of OMP threads to run with
nomp=$5
#The modules to be loaded (on stallo), in addition to the default git and cmake modules
modules=$6
#The lsdalton submodules to activate/deactivate, e.g. -DENABLE_SCALAPACK=ON
submodules=$7
#The directory used for the builds, creating a subdirectory to check out and buld the code for each different branch tested
base_dir=$8
#NOTUR project number
account=$9
#Specifying if job is to be run
run_job=${10}

substring=${submodules//-D/}
modstring=${modules//\//-}

#Name to be used for commit script, testboard.org and for the job script
name="stallo"_${branch}_${build}_${nmpi}_${nomp}_${modstring// /_}_${substring// /_// /_}
#Name of the commit file, submitted to the queue by "sbatch ${commit}"
commit=${name}.commit
#The directory the script was called from
call_dir=`pwd`
#Project name, and the name of the directory used for checking out the code
program="lsdalton"
#Main code repository
repository="git@gitlab.com:dalton/lsdalton.git"
#Place to store the submit scripts generate by this scipt
cd ${base_dir}/run_scripts

setup="./setup "
if [ "$build" == "coverage" ]; then
  setup="${setup} --coverage"
elif [ "$build" == "debug" ]; then
  setup="${setup} --type=debug"
elif [ "$build" == "check" ]; then
  setup="${setup} --type=debug --check"
elif [ "$build" == "release" ]; then
  setup="${setup} --type=release"
elif [ "$build" == "release64" ]; then
  setup="${setup} --type=release --int64"
elif [ "$build" == "scalapack" ]; then
  setup="${setup} --type=release --scalapack"
else
  echo "Error in type specification, must be one of:"
  echo "coverage:   --coverage                       "
  echo "debug:      --type=debug                     "
  echo "check:      --type=debug --check             "
  echo "release:    --type=release                   "
  echo "release64:  --type=release --int64           "
  echo "scalapack:  --type=release --scalapack       "
  exit 1
fi

if [ $nmpi \> "1" ]; then
  setup="${setup} --mpi"
fi

if [ $nomp \> "1" ]; then
  setup="${setup} --omp"
fi

setup="${setup} ${submodules}"

setup="${setup} -DBUILDNAME='${name}' ${name}"

###############################################################################################################
#
# The submit script generation
#
###############################################################################################################
echo "#!/bin/bash                                                                                  "  > $commit
echo "                                                                                             " >> $commit
echo "###############################################################################              " >> $commit
echo "# Automatically generated script to test LSDALTON by job_sbatch.sh                           " >> $commit
echo "# located under maintenance/cdash of the LSDALTON repository                                 " >> $commit
echo "# https://gitlab.com/dalton/lsdalton                                                         " >> $commit
echo "#                                                                                            " >> $commit
echo "# Input parameters provided to the script:                                                   " >> $commit
echo "#                                                                                            " >> $commit
echo "# Build type:   ${build}                                                                     " >> $commit
echo "# Branch:       ${branch}                                                                    " >> $commit
echo "# #mpi nodes:   ${nmpi}                                                                      " >> $commit
echo "# #omp threads: ${nomp}                                                                      " >> $commit
echo "# Modules:      ${modules}                                                                   " >> $commit
echo "# Submodules:   ${submodules}                                                                " >> $commit
echo "#                                                                                            " >> $commit
echo "# Internal parameters set by the script:                                                     " >> $commit
echo "#                                                                                            " >> $commit
echo "# Program:         ${program}                                                                " >> $commit
echo "# Code repository: ${program}                                                                " >> $commit
echo "# NOTUR account:   ${account}                                                                " >> $commit
echo "# Base directory:  ${base_dir}                                                               " >> $commit
echo "# Build directory: ${base_dir}/${program}/${name}                                            " >> $commit
echo "# Setup script:    ${setup}                                                                  " >> $commit
echo "# Call directory:  ${call_dir}                                                               " >> $commit
echo "###############################################################################              " >> $commit
echo "#                                                                                            " >> $commit
echo "#SBATCH --account=${account}                                                                 " >> $commit
echo "#SBATCH --job-name=${name}                                                                   " >> $commit
echo "#SBATCH --nodes=${nmpi}                                                                      " >> $commit
echo "#SBATCH --ntasks-per-node=${nomp}                                                            " >> $commit
echo "#SBATCH --time=0-24:00:00                                                                    " >> $commit
echo "#SBATCH --mem-per-cpu=2GB                                                                    " >> $commit
echo "#                                                                                            " >> $commit
echo "                                                                                             " >> $commit
echo "# Load modules                                                                               " >> $commit
echo "module purge                                                                                 " >> $commit
echo "module load cmake/3.5.1                                                                      " >> $commit
echo "module load git/2.8.1                                                                        " >> $commit

# Load provided modules
for module in ${modules}; do
  echo "module load ${module}                                                                        " >> $commit
done
echo "                                                                                             " >> $commit
echo "# List active modules                                                                        " >> $commit
echo "echo 'Modules loaded:'                                                                       " >> $commit
echo "module list                                                                                  " >> $commit
echo "                                                                                             " >> $commit
echo "# Set environmental variables                                                                " >> $commit
echo "export MATH_ROOT=${MKLROOT}                                                                  " >> $commit
echo "export OMP_NUM_THREADS=${nomp}                                                               " >> $commit
echo "export DALTON_NUM_MPI_PROCS=${nmpi}                                                          " >> $commit
echo "                                                                                             " >> $commit
echo "# Enter the directory where the code will be build from                                      " >> $commit
echo "cd ${base_dir}                                                                               " >> $commit
echo "                                                                                             " >> $commit
echo "# (Create) and enter directory for given branch                                              " >> $commit
echo "if [ -d "${branch}" ]; then                                                                   " >> $commit
echo "  cd ${branch}                                                                               " >> $commit
echo "else                                                                                         " >> $commit
echo "  mkdir ${branch}                                                                            " >> $commit
echo "  cd ${branch}                                                                               " >> $commit
echo "fi                                                                                           " >> $commit
echo "                                                                                             " >> $commit
echo "# Clone or pull repository                                                                   " >> $commit
echo "if [ -d "${program}" ]; then                                                                  " >> $commit
echo "  cd ${program}                                                                              " >> $commit
echo "  git pull                                                                                   " >> $commit
echo "  git submodule update --init --recursive                                                    " >> $commit
echo "else                                                                                         " >> $commit
echo "  git clone --recursive ${repository} ${program}                                             " >> $commit
echo "  cd ${program}                                                                              " >> $commit
echo "  git checkout -b ${branch} origin/${branch}                                                 " >> $commit
echo "fi                                                                                           " >> $commit
echo "                                                                                             " >> $commit
echo "# Remove build directory if it exists                                                        " >> $commit
echo "if [ -d "${name}" ]; then                                                                     " >> $commit
echo "  rm -rf ${name}                                                                             " >> $commit
echo "fi                                                                                           " >> $commit
echo "                                                                                             " >> $commit
echo "# Run setup command (whichs build the cmake build directory)                                 " >> $commit
echo "echo 'Running setup command'                                                                 " >> $commit
echo "${setup}                                                                                     " >> $commit
echo "                                                                                             " >> $commit
echo "# Enter the build directory                                                                  " >> $commit
echo "cd ${name}                                                                                   " >> $commit
echo "                                                                                             " >> $commit
echo "# Store the module list in file MODULES                                                      " >> $commit
echo "module list 2> MODULES                                                                       " >> $commit
echo "                                                                                             " >> $commit
echo "# Build and run                                                                              " >> $commit
echo "echo 'Build and test'                                                                        " >> $commit
echo "make $type                                                                                   " >> $commit
echo "                                                                                             " >> $commit
echo "                                                                                             " >> $commit

if [ "$run_job" == "run" ]; then
  #Submit job to queue
  sbatch $commit
fi

#Return to the directory where the script was activated
cd $call_dir



