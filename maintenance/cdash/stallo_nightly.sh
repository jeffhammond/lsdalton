#!/bin/bash
# Script to activate several different builds (Nightly, Experimental or others)
# Originally written 03-04-2017 by Simen Reine
# 
# Example use:
#
# sh stallo_nightly.sh Experimental /home/simensr/stallo-nightly nn4654k generate
#
# generates the slurm run scripts under /home/simensr/stallo-nightly/run_scripts, whereas
#
# sh stallo_nightly.sh Experimental /home/simensr/stallo-nightly nn4654k run
#
# generates the slurm run scripts under /home/simensr/stallo-nightly/run_scripts AND submit the jobs to queue
#
# The command:
#
# sh stallo_nightly.sh Nightly `pwd` nn4654k generate
#
# will generate all the run scipts used on stallo under the directory run_scripts
#
TYPE="$1"
BASE="$2"
ACCOUNT="$3"
RUN="$4"
sh job_sbatch.sh master release   $TYPE 4 2 "intel/13.0 openmpi/1.8.5 mkl/11.1"                 "" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master release   $TYPE 4 2 "intel/14.0 openmpi/1.8.4 mkl/11.1"                 "-DTENSORS_ENABLE_BRUTE_FORCE_FLUSH=ON" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master release   $TYPE 4 2 "intel/13.4 openmpi/1.6.5 mkl/11.1"                 "" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master release   $TYPE 1 1 "intel/14.0 mkl/11.1"                               "" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master release   $TYPE 4 2 "intel/14.0"                                        "" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master release   $TYPE 4 2 "gcc/4.9.1 openmpi/1.8.4 mkl/11.1"                  "" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master release   $TYPE 1 1 "gcc/4.9.1 mkl/11.1"                                "" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master debug     $TYPE 1 1 "intel/14.0 mkl/11.1"                               "" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master debug     $TYPE 4 2 "intel/14.0"                                        "" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master debug     $TYPE 4 2 "gcc/4.9.1 openmpi/1.8.4 mkl/11.1"                  "" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master debug     $TYPE 1 1 "gcc/4.9.1 mkl/11.1"                                "" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master debug     $TYPE 4 2 "pgi/pgi64/2014 mkl/11.1"                           "" $BASE $ACCOUNT $RUN
sh job_sbatch.sh master debug     $TYPE 1 1 "pgi/pgi64/2014 mkl/11.1"                           "" $BASE $ACCOUNT $RUN
#sh job_sbatch.sh master scalapack $TYPE 4 2 "intel/14.0 openmpi/1.8.4 mkl/11.1 scalapack/2.0.2" "-DTENSORS_ENABLE_BRUTE_FORCE_FLUSH=ON" $BASE $ACCOUNT $RUN
#sh job_sbatch.sh master scalapack $TYPE 4 2 "intel/14.0 mkl/11.1 impi/4.1.3"                     "" $BASE $ACCOUNT $RUN
#sh job_sbatch.sh master scalapack $TYPE 4 2 "intel/14.0 mkl/11.1 scalapack/2.0.2 impi/4.1.3"    "" $BASE $ACCOUNT $RUN
