bname=$(basename "$0")
bname=${bname/.bash/}"_$CI_BUILD_REF_NAME"
##########
echo " Running GitlabCI-i64/herc-gcc48-b64-modc-mpich.bash"
source /opt/mpich-3.1.4-gnu-4.8/source.bash
export DALTON_NUM_MPI_PROCS=3
export OMP_NUM_THREADS=2
export DALTON_TMPDIR=$(pwd)/tmp
export LSDALTON_DEVELOPER=1
#
wrk=$1
lib=lsdalton_$bname
git submodule update --force --init --recursive
#
if [ ! -d $bname ]
then
   ./setup --fc=mpif90 --cc=mpicc --cxx=mpic++ --mpi --omp --int64 --type=debug --check -DENABLE_DEC=ON -DENABLE_TENSORS=ON -DENABLE_RSP=OFF -DENABLE_XCFUN=OFF -DENABLE_PCMSOLVER=OFF -DBUILDNAME="$bname" $bname
fi
if [ ! -d $DALTON_TMPDIR ]
then
   mkdir $DALTON_TMPDIR
fi
#
cd $bname
#
ctest -D Nightly --track GitLabCI -L ContinuousIntegration
exit $?
