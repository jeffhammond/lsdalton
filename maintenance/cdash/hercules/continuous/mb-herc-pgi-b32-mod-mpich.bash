bname=$(basename "$0")
bname=${bname/.bash/}
##########
echo "running continuous/mb-herc-pgi-b32-mod-mpich.bash"
export LM_LICENSE_FILE=/opt/pgi/license.dat
source /opt/pgi/linux86-64/15.7/pgi.sh
source /opt/pgi/linux86-64/15.7/mpi.sh
export LD_LIBRARY_PATH=/opt/pgi/15.7/share_objects/lib64:$LD_LIBRARY_PATH
export DALTON_NUM_MPI_PROCS=6
export OMP_NUM_THREADS=2
#
wrk=$1
lib=lsdalton_$bname
export DALTON_TMPDIR=$wrk/$lib/$bname/tmp
#
if [ ! -d $wrk/$lib ]
then
   cd $wrk
   git clone --recursive git@gitlab.com:dalton/lsdalton.git $lib
fi
#
cd $wrk/$lib
#
if [ ! -d $wrk/$lib/$bname ]
then
   ./setup --fc=mpif90 --cc=mpicc --cxx=mpic++ --mpi --omp --int64 --type=debug -DENABLE_DEC=ON -DENABLE_TENSORS=ON -DENABLE_RSP=OFF -DENABLE_XCFUN=OFF -DENABLE_GEN1INT=OFF -DBUILDNAME="$bname" $bname
fi
if [ ! -d $DALTON_TMPDIR ]
then
   mkdir $DALTON_TMPDIR
fi
#
cd $bname
#
ctest -D Continuous -L dec
